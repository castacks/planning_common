/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * se3_tangent_bundle.cpp
 *
 *  Created on: Feb 1, 2015
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/states/se3_tangent_bundle.h"
#include "planning_common/utils/state_utils.h"

namespace ca {
namespace planning_common {

namespace su = state_utils;

SE3TangentBundle::EigenForm SE3TangentBundle::StateType::GetEigenForm() const{
  SE3TangentBundle::EigenForm data;
  data.time = GetTime().position;
  data.position = Eigen::Vector3d(GetPose().getX(), GetPose().getY(), GetPose().getZ());
  data.velocity = Eigen::Vector3d(GetTangentPose().GetXDot(), GetTangentPose().GetYDot(),GetTangentPose().GetZDot());
  data.orientation = su::ConvertSO3toEuler(&GetPose().rotation());
  data.rot_rates = Eigen::Vector3d(GetTangentPose().GetPhiDot(), GetTangentPose().GetThetaDot(), GetTangentPose().GetPsiDot());
  return data;
}

void SE3TangentBundle::StateType::SetEigenForm(const SE3TangentBundle::EigenForm &data) {
  GetTime().position = data.time;
  GetPose().setXYZ(data.position.x(), data.position.y(), data.position.z());
  su::ConvertEulerToSO3(data.orientation.x(), data.orientation.y(), data.orientation.z(), &GetPose().rotation());
  GetTangentPose().SetXYZDot(data.velocity.x(), data.velocity.y(), data.velocity.z());
  GetTangentPose().SetPhiThetaPsiDot(data.rot_rates.x(), data.rot_rates.y(), data.rot_rates.z());
}

Eigen::Vector3d SE3TangentBundle::StateType::GetVelocity() const {
  return Eigen::Vector3d(GetTangentPose().GetXDot(), GetTangentPose().GetYDot(),GetTangentPose().GetZDot());
}


}
}

