/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * path_utils.cpp
 *
 *  Created on: Jun 23, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/utils/path_utils.h"
#include "planning_common/utils/state_utils.h"
#include "planning_common/utils/workspace_utils.h"
#include "ca_nav_msgs/WorkspaceTrajectory.h"
#include "math_utils/math_utils.h"
#include "angles/angles.h"


namespace wu = ca::planning_common::workspace_utils;
namespace su = ca::planning_common::state_utils;
namespace ob = ompl::base;
namespace vm = ca::math_utils::vector_math;
namespace nu = ca::math_utils::numeric_operations;

namespace ca {
namespace planning_common {
namespace path_utils {

//bool StateDistanceFromEnd(const PathWaypoint &path, double distance, ompl::base::State *sample);

double GetWorkspaceTranslationLength (const PathWaypoint &path) {
  double length = 0;
  for (std::size_t i = 0 ; i < path.NumberOfWaypoints() - 1; i++)
    length += (wu::GetTranslationVectorXd(path.getSpaceInformation(), path.GetWaypoint(i)) -
        wu::GetTranslationVectorXd(path.getSpaceInformation(), path.GetWaypoint(i+1))).norm();
  return length;
}

PathWaypoint ConvertPathToSE3TangentBundlePath(const ompl::geometric::PathGeometric& original_path) {
  return ConvertPathToSE3TangentBundlePath( boost::bind( static_cast< const ompl::base::State* (ompl::geometric::PathGeometric::*)( unsigned int ) const > (&ompl::geometric::PathGeometric::getState), &original_path, _1),
                                            boost::bind(&ompl::geometric::PathGeometric::getStateCount, &original_path),
                                            original_path.getSpaceInformation());
}

PathWaypoint ConvertPathToSE3TangentBundlePath(const PathWaypoint& original_path) {
  return ConvertPathToSE3TangentBundlePath( boost::bind( static_cast< const ompl::base::State* (PathWaypoint::*)( unsigned int ) const > (&PathWaypoint::GetWaypoint), &original_path, _1),
                                            boost::bind(&PathWaypoint::NumberOfWaypoints, &original_path),
                                            original_path.getSpaceInformation());
}


PathWaypoint ConvertPathToSE3TangentBundlePath(boost::function< const ompl::base::State* (unsigned int)> GetStateFn,
                                               boost::function< std::size_t (void)> GetNumState,
                                               const ompl::base::SpaceInformationPtr& si_original) {

  // Why are we defining si here and not passing it?
  // Ans - Pragmatic reasons. We already know the space is se3 tangent bundle. That just leaves the bounds as a
  // variable. Bounds are useful for planning purposes - there is no need for it to exist afterwards (old paradigm didnt need it)
  // Then it makes sense to just bury the creation here.
  ompl::base::SpaceInformationPtr si = su::GetStandardSE3TangentBundle();

  PathWaypoint output_path(si);
  for (std::size_t i = 0 ; i < GetNumState(); i++) {
    ob::ScopedState<SE3TangentBundle> output(si);
    const ompl::base::State* input_state = GetStateFn(i);
    su::ConvertAnyToSE3TangentBundle(input_state, si_original, output.get());
    output_path.GetWaypoints().push_back(si->cloneState(output.get()));
  }
  return output_path;
}

PathWaypoint ConvertWorkspaceTrajectoryMsgToPathWaypoint(const ca_nav_msgs::WorkspaceTrajectory &msg) {
  ompl::base::SpaceInformationPtr si = su::GetStandardSE3TangentBundle();
  PathWaypoint path(si);
  ca_nav_msgs::WorkspaceTrajectory workspace_traj = msg;
  for (ca_nav_msgs::WorkspaceTrajectory::_waypoint_type::const_iterator it = workspace_traj.waypoint.begin(); it != workspace_traj.waypoint.end(); it++) {
    ob::ScopedState<SE3TangentBundle> output(si);
    su::ConvertOdomToSE3TangentBundle(*it, output.get());
    path.AppendWaypoint(output.get());
  }
  return path;
}

ca_nav_msgs::WorkspaceTrajectory ConvertPathWaypointToWorkspaceTrajectoryMsg(const PathWaypoint &path) {
  ca_nav_msgs::WorkspaceTrajectory workspace_traj;
  for (PathWaypoint::waypoints::const_iterator it = path.CBegin(); it != path.CEnd(); it++) {
    const SE3TangentBundle::StateType *state = (*it)->as<SE3TangentBundle::StateType>();
    nav_msgs::Odometry odom;
    su::ConvertSE3TangentBundleToOdom(state, odom);
    workspace_traj.waypoint.push_back(odom);
  }
  return workspace_traj;
}

bool IsIndexValid(const PathWaypoint &path, double index) {
  if (path.Size() == 0 || index < 0 || index > path.Size() - 1)
    return false;
  else
    return true;
}

void StateAtIndex(const PathWaypoint &path, double index, ompl::base::State* state) {
  if (path.Size() == 0) {
    OMPL_ERROR("Path utils: StateAtIndex: Path size 0!");
    return;
  }

  if (!IsIndexValid(path, index)) {
    OMPL_ERROR("Path utils: StateAtIndex: Invalid index!");
    return;
  }

  if (path.Size() == 1) {
    path.getSpaceInformation()->copyState(state, path.Front());
    return;
  }

  unsigned int l_index = (unsigned int) std::max(0.0, std::min((double)path.Size()-2.0, std::floor(index)));
  double frac = index - l_index;
  path.getSpaceInformation()->getStateSpace()->InterpolateWithoutMotion(path.GetWaypoint(l_index), path.GetWaypoint(l_index + 1), frac, state);
  return;
}

double ProjectPosition(const PathWaypoint &path, const Eigen::Vector3d &position, double start_index) {
  if (path.Size() == 0) {
    OMPL_ERROR("Path utils: ProjectPosition: Path size 0!");
    return 0;
  }

  if (!IsIndexValid(path, start_index)) {
    OMPL_ERROR("Path utils: ProjectPosition: Invalid index!");
    return 0;
  }

  if (path.Size() ==1)
    return 0;

  unsigned int l_index = (unsigned int) std::max(0.0, std::min((double)path.Size()-2.0, std::floor(start_index)));

  for (unsigned int idx = l_index; idx < path.Size() - 1; idx++) {
    Eigen::Vector3d v1 = wu::GetTranslationVector3d(path.getSpaceInformation(), path.GetWaypoint(idx));
    Eigen::Vector3d v2 = wu::GetTranslationVector3d(path.getSpaceInformation(), path.GetWaypoint(idx+1));
    double frac = vm::FractionSidePlane(v1, v2, position);
    if (frac <= 0)
      return std::max(start_index, (double)idx);
    else if (frac <= 1)
      return std::max(start_index, (double)(idx+frac));
  }
  return path.Size()-1;
}

void SegmentFromIndex(const PathWaypoint &original, double index, PathWaypoint &modified) {
  ob::State* interp = original.getSpaceInformation()->allocState();
  StateAtIndex(original, index, interp);
  modified.AppendWaypoint(interp);
  original.getSpaceInformation()->freeState(interp);
  for (unsigned int idx = (unsigned int) std::round(index+1); idx < original.Size(); idx++) {
    modified.AppendWaypoint(original.GetWaypoint(idx));
  }
}

void SegmentToIndex(const PathWaypoint &original, double index, PathWaypoint &modified) {
  PathWaypoint original_copy(original);
  std::reverse(original_copy.GetWaypoints().begin(), original_copy.GetWaypoints().end());

  SegmentFromIndex(original_copy, (double)original_copy.Size() - 1.0 - index, modified);
  std::reverse(modified.GetWaypoints().begin(), modified.GetWaypoints().end());
}

void SegmentFromToIndex(const PathWaypoint &original, double from_index, double to_index, PathWaypoint &modified) {
  if (from_index > to_index) {
    OMPL_ERROR("Path utils: SegmentFromToIndex: from index more than to index!");
    return;
  }

  if (nu::AreDoubleSame(from_index, to_index)) {
    ob::State* interp = original.getSpaceInformation()->allocState();
    StateAtIndex(original, from_index, interp);
    modified.AppendWaypoint(interp);
    original.getSpaceInformation()->freeState(interp);
    return;
  }

  ob::State* interp = original.getSpaceInformation()->allocState();
  StateAtIndex(original, from_index, interp);
  modified.AppendWaypoint(interp);
  for (unsigned int idx = (int) std::round(from_index+1); idx <= (int) std::round(to_index - 1); idx++) {
    modified.AppendWaypoint(original.GetWaypoint(idx));
  }
  StateAtIndex(original, to_index, interp);
  modified.AppendWaypoint(interp);
  original.getSpaceInformation()->freeState(interp);
}



void StateAtTime(const PathWaypoint &path, double time, ob::State* state) {
  if (path.Size() == 0)
    return;

  if (wu::GetTime(path.getSpaceInformation(), path.Front()) > time) {
    path.getSpaceInformation()->copyState(state, path.Front());
    return;
  }

  if (wu::GetTime(path.getSpaceInformation(), path.Back()) < time) {
    path.getSpaceInformation()->copyState(state, path.Back());
    return;
  }

  for (std::size_t i = 1; i < path.Size(); i++) {
    if (wu::GetTime(path.getSpaceInformation(), path.GetWaypoint(i)) > time) {
      double frac = (time - wu::GetTime(path.getSpaceInformation(), path.GetWaypoint(i-1)) ) / std::max(std::numeric_limits<double>::epsilon(), wu::GetTime(path.getSpaceInformation(), path.GetWaypoint(i)) - wu::GetTime(path.getSpaceInformation(), path.GetWaypoint(i-1)));
      path.getSpaceInformation()->getStateSpace()->InterpolateWithoutMotion(path.GetWaypoint(i-1), path.GetWaypoint(i), frac, state);
      return;
    }
  }
}

PathWaypoint SegmentToTime(const PathWaypoint &original, double time) {
  PathWaypoint segmented(original.getSpaceInformation());
  const ob::State* backup = NULL;

  for (PathWaypoint::waypoints::const_iterator it = original.CBegin(); it != original.CEnd(); it++) {
    if (wu::GetTime(original.getSpaceInformation(), *it) < time) {
      segmented.AppendWaypoint(*it);
      backup = *it;
    } else {
      if (backup) {
        double frac = (time - wu::GetTime(original.getSpaceInformation(), backup) ) / std::max(1e-9, wu::GetTime(original.getSpaceInformation(), *it) - wu::GetTime(original.getSpaceInformation(), backup));
        ob::State* interp = original.getSpaceInformation()->allocState();
        original.getSpaceInformation()->getStateSpace()->InterpolateWithoutMotion(backup, *it, frac, interp);
        segmented.AppendWaypoint(interp);
        original.getSpaceInformation()->freeState(interp);
        break;
      }
    }
  }
  return segmented;
}

PathWaypoint SegmentFromTime(const PathWaypoint &original, double time) {
  PathWaypoint original_copy(original);
  std::reverse(original_copy.GetWaypoints().begin(), original_copy.GetWaypoints().end());

  PathWaypoint segmented(original_copy.getSpaceInformation());
  const ob::State* backup = NULL;

  for (PathWaypoint::waypoints::const_iterator it = original_copy.Begin(); it != original_copy.End(); it++) {
    if (wu::GetTime(original_copy.getSpaceInformation(), *it) > time) {
      segmented.AppendWaypoint(*it);
      backup = *it;
    } else {
      if (backup) {
        double frac = (wu::GetTime(original_copy.getSpaceInformation(), backup) - time) / std::max(1e-9, wu::GetTime(original_copy.getSpaceInformation(), backup) - wu::GetTime(original_copy.getSpaceInformation(), *it));
        ob::State* interp = original_copy.getSpaceInformation()->allocState();
        original_copy.getSpaceInformation()->getStateSpace()->InterpolateWithoutMotion(backup, *it, frac, interp);
        segmented.AppendWaypoint(interp);
        original_copy.getSpaceInformation()->freeState(interp);
        break;
      }
    }
  }
  std::reverse(segmented.GetWaypoints().begin(), segmented.GetWaypoints().end());
  return segmented;
}


bool StitchPathWaypointAtTime(const PathWaypoint &original, const PathWaypoint &branch, PathWaypoint &merged, double threshold) {
  if (original.Size() > 0 && branch.Size() > 0
      && su::AreSpaceInformationSame(original.getSpaceInformation(), branch.getSpaceInformation())
  && su::AreSpaceInformationSame(original.getSpaceInformation(), merged.getSpaceInformation())) {
    PathWaypoint segmented = SegmentToTime(original, wu::GetTime(branch.getSpaceInformation(), branch.GetWaypoint(0)));
    if (segmented.Size() == 0)
      return false;

    if (original.getSpaceInformation()->getStateSpace()->distance(segmented.GetWaypoint(segmented.Size()-1), branch.GetWaypoint(0)) > threshold)
      return false;

    for (std::size_t i = 0; i < segmented.Size()-1; i++)
      merged.AppendWaypoint(segmented.GetWaypoint(i));

    for (std::size_t i = 0; i < branch.Size(); i++)
      merged.AppendWaypoint(branch.GetWaypoint(i));

    return true;
  } else {
    return false;
  }
}


void RecomputeTime(double start_time, PathWaypoint &path) {
  if (path.Size() > 0) {
    wu::GetTimeState(path.getSpaceInformation(), path.GetWaypoints().front())->position = start_time;
    double running_time = start_time;
    for (size_t i = 0; i < path.GetWaypoints().size()-1; i++) {
      double vel = 0.5*(wu::GetTangentTranslationVectorXd(path.getSpaceInformation(), path.GetWaypoints()[i]).norm()
          + wu::GetTangentTranslationVectorXd(path.getSpaceInformation(), path.GetWaypoints()[i+1]).norm());
      double distance = path.getSpaceInformation()->distance(path.GetWaypoints()[i], path.GetWaypoints()[i+1]);
      running_time += distance/vel;
      wu::GetTimeState(path.getSpaceInformation(), path.GetWaypoints()[i+1])->position = running_time;
    }
  }
}


bool StateDistanceFromEndFromRef2D(const PathWaypoint &path, double distance, const Eigen::Vector3d &ref, ob::State *sample) {
  if (path.Size() <= 1)
    return false;
  double iter_distance = 0;
  for (size_t i = path.Size()-1; i-- > 0; ) {
    SE3TangentBundle::EigenForm state_cur = path.GetWaypoint(i)->as<SE3TangentBundle::StateType>()->GetEigenForm();
    iter_distance = vm::Length2D(state_cur.position - ref);
    if (iter_distance > distance) {
      SE3TangentBundle::EigenForm state_next = path.GetWaypoint(i+1)->as<SE3TangentBundle::StateType>()->GetEigenForm();
      double d1 = vm::Length2D(state_next.position - ref);
      if (d1 > distance)
        return false;
      double d2 = iter_distance;
      double p = vm::Length2D(state_next.position - state_cur.position);
      double q = (d2*d2 - d1*d1 - p*p)/(2*p);
      double r = p - (-q + sqrt(distance*distance + q*q - d1*d1));
      double frac = r / p;
      path.getSpaceInformation()->getStateSpace()->interpolate(path.GetWaypoint(i), path.GetWaypoint(i+1), frac, sample);
      return true;
    }
  }
  return false;
}

void SplitPathAtHorizon(const PathWaypoint &original, PathWaypoint &first, PathWaypoint &second, double horizon) {
  if (original.Size() > 1) {
    double accu_length = 0;
    for (std::size_t i = 0 ; i < original.NumberOfWaypoints(); i++) {
      if (i == 0)
        accu_length = 0;
      else
        accu_length += (wu::GetTranslationVectorXd(original.getSpaceInformation(), original.GetWaypoint(i)) -
          wu::GetTranslationVectorXd(original.getSpaceInformation(), original.GetWaypoint(i-1))).norm();

      if (accu_length <= horizon)
        first.AppendWaypoint(original.GetWaypoint(i));
      else
        second.AppendWaypoint(original.GetWaypoint(i));
    }
  } else {
    first.CopyFrom(original);
  }
}

bool WarpSpeedProfile(const PathWaypoint &original_path, PathWaypoint &new_path) {
  double l1 = original_path.length();
  double l2 = new_path.length();
  if (l1 <= l2 && original_path.Size() > 1 && new_path.Size() > 1) {
    double ratio = l1/l2;
    double accum_dist_2 = 0, accum_dist_1 = original_path.getSpaceInformation()->distance(original_path.GetWaypoint(0), original_path.GetWaypoint(1));
    std::size_t count_1 = 1;
    for (std::size_t i = 0; i < new_path.Size(); i++) {
      if (i > 0)
        accum_dist_2 += new_path.getSpaceInformation()->distance(new_path.GetWaypoint(i-1), new_path.GetWaypoint(i));
      double query_dist = ratio*accum_dist_2;
      while (query_dist > accum_dist_1) {
        if (count_1 >= (original_path.Size()-1))
          break;
        count_1++;
        accum_dist_1 += original_path.getSpaceInformation()->distance(original_path.GetWaypoint(count_1-1), original_path.GetWaypoint(count_1));
      }
      double alpha = (accum_dist_1 - query_dist)/original_path.getSpaceInformation()->distance(original_path.GetWaypoint(count_1-1), original_path.GetWaypoint(count_1));
      double velocity = alpha*original_path.GetWaypoint(count_1-1)->as<SE3TangentBundle::StateType>()->GetEigenForm().velocity.norm() +
          (1-alpha)*original_path.GetWaypoint(count_1)->as<SE3TangentBundle::StateType>()->GetEigenForm().velocity.norm();
      new_path.GetWaypoint(i)->as<SE3TangentBundle::StateType>()->GetTangentPose().SetXDot(velocity);
    }
    return true;
  } else {
    return false;
  }
}

bool SpeedProfileCopyBackwards(const PathWaypoint &original_path, PathWaypoint &new_path) {
  double l1 = original_path.length();
  double l2 = new_path.length();

  if (original_path.Size() > 1 && new_path.Size() > 1) {
    double ratio = 1;
    double accum_dist_2 = 0, accum_dist_1 = original_path.getSpaceInformation()->distance(original_path.GetWaypoint(original_path.Size()-2), original_path.GetWaypoint(original_path.Size()-1));
    std::size_t count_1 = original_path.Size()-2;
    for (int i = new_path.Size()-1; i >= 0 ; i--) {
      if (i > 0)
        accum_dist_2 += new_path.getSpaceInformation()->distance(new_path.GetWaypoint(i-1), new_path.GetWaypoint(i));
      double query_dist = ratio*accum_dist_2;
      while (query_dist > accum_dist_1) {
        if (count_1 <= 1)
          break;
        count_1--;
        accum_dist_1 += original_path.getSpaceInformation()->distance(original_path.GetWaypoint(count_1-1), original_path.GetWaypoint(count_1));
      }
      double alpha = (accum_dist_1 - query_dist)/original_path.getSpaceInformation()->distance(original_path.GetWaypoint(count_1-1), original_path.GetWaypoint(count_1));
      double velocity = alpha*original_path.GetWaypoint(count_1-1)->as<SE3TangentBundle::StateType>()->GetEigenForm().velocity.norm() +
          (1-alpha)*original_path.GetWaypoint(count_1)->as<SE3TangentBundle::StateType>()->GetEigenForm().velocity.norm();
      new_path.GetWaypoint(i)->as<SE3TangentBundle::StateType>()->GetTangentPose().SetXDot(velocity);
    }
    return true;
  } else {
    return false;
  }
}

double ForwardPropagateIndexByDistance(const PathWaypoint &path, double index, double propagate_distance) {
  if (index >= path.Size() - 1)
    return path.Size() - 1;
  unsigned int start_idx = static_cast<unsigned int>(std::ceil(index));
  double accum_dist = 0;
  for (unsigned int idx = start_idx; idx < path.Size() - 1; idx++) {
    double seg_length = path.getSpaceInformation()->distance(path.GetWaypoint(idx), path.GetWaypoint(idx + 1));
    if (accum_dist + seg_length > propagate_distance) {
      double alpha = (propagate_distance - accum_dist) / seg_length;
      return (idx + alpha);
    }
    accum_dist += seg_length;
  }
  return path.Size() - 1;
}

void ShiftPathToStart(const ompl::base::State* start_state, PathWaypoint &path) {
  SE3TangentBundle::EigenForm start_eig = start_state->as<SE3TangentBundle::StateType>()->GetEigenForm();
  if (path.Size() > 0) {
    SE3TangentBundle::EigenForm act_start_eig = path.Front()->as<SE3TangentBundle::StateType>()->GetEigenForm();
    SE3TangentBundle::EigenForm offset;
    offset.time = start_eig.time - act_start_eig.time;
    offset.position = start_eig.position - act_start_eig.position;
    offset.orientation = Eigen::Vector3d(0,0,start_eig.orientation.z() - act_start_eig.orientation.z());
    for (auto it : path.GetWaypoints()) {
      SE3TangentBundle::EigenForm eig = it->as<SE3TangentBundle::StateType>()->GetEigenForm();
      eig.time += offset.time;
      eig.position += offset.position;
      eig.orientation += offset.orientation;
      eig.orientation[2] = angles::normalize_angle(eig.orientation.z());
      it->as<SE3TangentBundle::StateType>()->SetEigenForm(eig);
    }
  }
}

void MakeHeadingTangent(PathWaypoint &path) {
  if (path.Size() > 1) {
    for (size_t i = 0; i < path.GetWaypoints().size() - 1; i++) {
      SE3TangentBundle::EigenForm eig1 = path.GetWaypoints()[i]->as<SE3TangentBundle::StateType>()->GetEigenForm();
      SE3TangentBundle::EigenForm eig2 = path.GetWaypoints()[i+1]->as<SE3TangentBundle::StateType>()->GetEigenForm();
      eig1.orientation[2] = atan2(eig2.position.y() - eig1.position.y(),
                                  eig2.position.x() - eig1.position.x());
      path.GetWaypoints()[i]->as<SE3TangentBundle::StateType>()->SetEigenForm(eig1);
      if (i == path.GetWaypoints().size() - 2) {
       eig2.orientation[2] = eig1.orientation[2];
       path.GetWaypoints()[i+1]->as<SE3TangentBundle::StateType>()->SetEigenForm(eig2);
      }
    }
  }
}

void PrintGlideSlope(const PathWaypoint &path) {
  for (size_t i = 0; i < path.NumberOfWaypoints() - 1; i++) {
    SE3TangentBundle::EigenForm pt1 = path.GetWaypoint(i)->as<SE3TangentBundle::StateType>()->GetEigenForm();
    SE3TangentBundle::EigenForm pt2 = path.GetWaypoint(i+1)->as<SE3TangentBundle::StateType>()->GetEigenForm();
    double glide = (pt2.position.z() - pt1.position.z())/vm::Length2D(pt2.position - pt1.position);
    std::cout<<glide<<"\n";
  }
}

void PrintXY(const PathWaypoint &path) {
  for (size_t i = 0; i < path.NumberOfWaypoints() - 1; i++) {
    SE3TangentBundle::EigenForm pt1 = path.GetWaypoint(i)->as<SE3TangentBundle::StateType>()->GetEigenForm();
    SE3TangentBundle::EigenForm pt2 = path.GetWaypoint(i+1)->as<SE3TangentBundle::StateType>()->GetEigenForm();
    double xy = vm::Length2D(pt2.position - pt1.position);
    std::cout<<xy<<"\n";
  }
}

}  // namespace path_utils
}  // namespace planning_common
}  // namespace ca
