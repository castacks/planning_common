/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * workspace_utils.cpp
 *
 *  Created on: Jun 25, 2014
 *      Author: Sanjiban Choudhury
 */

#include <tf/tf.h>
#include <tf_conversions/tf_eigen.h>

#include "planning_common/utils/workspace_utils.h"
#include "planning_common/utils/state_utils.h"
#include "ompl/base/StateSpaceTypes.h"
#include "tf_utils/tf_utils.h"


namespace ob = ompl::base;
namespace su = ca::planning_common::state_utils;

namespace ca {
namespace planning_common {
namespace workspace_utils {

bool HasTranslation(const ob::SpaceInformationPtr& si) {
  return si->workspace_information().workspace_map().count(ompl_base::WorkspaceTags::TRANSLATION) == 1;
}

bool HasTangentTranslation(const ob::SpaceInformationPtr& si) {
  return si->workspace_information().workspace_map().count(ompl_base::WorkspaceTags::TANGENT_TRANSLATION) == 1;
}

bool HasRotation(const ompl::base::SpaceInformationPtr& si) {
  return si->workspace_information().workspace_map().count(ompl_base::WorkspaceTags::ROTATION) == 1;
}

bool HasTangentRotation(const ompl::base::SpaceInformationPtr& si) {
  return si->workspace_information().workspace_map().count(ompl_base::WorkspaceTags::TANGENT_ROTATION) == 1;
}

bool HasTime(const ompl::base::SpaceInformationPtr& si) {
  return si->workspace_information().workspace_map().count(ompl_base::WorkspaceTags::TIME) == 1;
}

bool HasSO3Rotation(const ompl::base::SpaceInformationPtr& si) {
  if (si->workspace_information().workspace_map().count(ompl_base::WorkspaceTags::ROTATION) != 1)
    return false;
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::ROTATION);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  if (loc.space->getType() == ob::STATE_SPACE_SO3)
    return true;
  else
    return false;
}

const ob::RealVectorStateSpace::StateType* GetTranslationState(const ob::SpaceInformationPtr& si, const ob::State *state, const ob::StateSpace* &space) {
  BOOST_ASSERT_MSG(HasTranslation(si), "Workspace has no translation component. Please check tags");
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::TRANSLATION);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  space = loc.space;
  return si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::RealVectorStateSpace::StateType>();
}

ob::RealVectorStateSpace::StateType* GetTranslationState(const ob::SpaceInformationPtr& si, ob::State *state) {
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::TRANSLATION);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  return si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::RealVectorStateSpace::StateType>();
}

Eigen::VectorXd GetTranslationVectorXd(const ob::SpaceInformationPtr& si, const ob::State *state) {
  const ob::StateSpace *space = NULL;
  const ob::RealVectorStateSpace::StateType* translation = GetTranslationState(si, state, space);
  Eigen::VectorXd vec(space->getDimension());
  for (unsigned int i = 0; i < space->getDimension(); i++)
    vec[i] = translation->values[i];
  return vec;
}

Eigen::Vector2d GetTranslationVector2d(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state) {
  const ob::StateSpace *space = NULL;
  const ob::RealVectorStateSpace::StateType* translation = GetTranslationState(si, state, space);
  BOOST_ASSERT_MSG(space->getDimension() == 2, "Translation component is not 2d");
  return Eigen::Vector2d(translation->values[0], translation->values[1]);
}

Eigen::Vector3d GetTranslationVector3d(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state) {
  const ob::StateSpace *space = NULL;
  const ob::RealVectorStateSpace::StateType* translation = GetTranslationState(si, state, space);
  BOOST_ASSERT_MSG(space->getDimension() == 3, "Translation component is not 3d");
  return Eigen::Vector3d(translation->values[0], translation->values[1], translation->values[2]);
}

unsigned int GetTranslationDimension(const ompl::base::SpaceInformationPtr& si) {
  BOOST_ASSERT_MSG(HasTranslation(si), "Workspace has no translation component. Please check tags");
  return si->getStateSpace()->getSubstateLocationsByName().at(si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::TRANSLATION)).space->getDimension();
}


const ob::RealVectorStateSpace::StateType* GetTangentTranslationState(const ob::SpaceInformationPtr& si, const ob::State *state, const ob::StateSpace* &space) {
  BOOST_ASSERT_MSG(HasTangentTranslation(si), "Workspace has no TangentTranslation component. Please check tags");
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::TANGENT_TRANSLATION);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  space = loc.space;
  return si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::RealVectorStateSpace::StateType>();
}

ob::RealVectorStateSpace::StateType* GetTangentTranslationState(const ob::SpaceInformationPtr& si, ob::State *state) {
  BOOST_ASSERT_MSG(HasTangentTranslation(si), "Workspace has no TangentTranslation component. Please check tags");
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::TANGENT_TRANSLATION);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  return si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::RealVectorStateSpace::StateType>();
}


Eigen::VectorXd GetTangentTranslationVectorXd(const ob::SpaceInformationPtr& si, const ob::State *state) {
  const ob::StateSpace *space = NULL;
  const ob::RealVectorStateSpace::StateType* TangentTranslation = GetTangentTranslationState(si, state, space);
  Eigen::VectorXd vec(space->getDimension());
  for (unsigned int i = 0; i < space->getDimension(); i++)
    vec[i] = TangentTranslation->values[i];
  return vec;
}

Eigen::Vector2d GetTangentTranslationVector2d(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state) {
  const ob::StateSpace *space = NULL;
  const ob::RealVectorStateSpace::StateType* tangent_translation = GetTangentTranslationState(si, state, space);
  BOOST_ASSERT_MSG(space->getDimension() == 2, "TangentTranslation component is not 2d");
  return Eigen::Vector2d(tangent_translation->values[0], tangent_translation->values[1]);
}

Eigen::Vector3d GetTangentTranslationVector3d(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state) {
  const ob::StateSpace *space = NULL;
  const ob::RealVectorStateSpace::StateType* tangent_translation = GetTangentTranslationState(si, state, space);
  BOOST_ASSERT_MSG(space->getDimension() == 3, "TangentTranslation component is not 3d");
  return Eigen::Vector3d(tangent_translation->values[0], tangent_translation->values[1], tangent_translation->values[2]);
}


Eigen::Vector3d GetTangentTranslationVector3dWorld(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state) {\
  Eigen::Vector3d vel = GetTangentTranslationVector3d(si, state);
  tf::Transform transform;
  Eigen::Vector3d rotation = GetRotationVector3d(si, state);
  transform.setRotation(tf::createQuaternionFromRPY(rotation[0], rotation[1], rotation[2]));
  tf_utils::transformVector3d(transform, vel);
  return vel;
}


ob::RealVectorStateSpace::StateType* GetTangentTranslationState(const ob::SpaceInformationPtr& si, ob::State *state, const ompl::base::StateSpace* &space) {
  BOOST_ASSERT_MSG(HasTangentTranslation(si), "Workspace has no TangentTranslation component. Please check tags");
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::TANGENT_TRANSLATION);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  space = loc.space;
  return si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::RealVectorStateSpace::StateType>();
}


const ob::SO2StateSpace::StateType* GetRotationStateSO2(const ob::SpaceInformationPtr& si, const ob::State *state) {
  BOOST_ASSERT_MSG(HasRotation(si), "Workspace has no rotation component. Please check tags");
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::ROTATION);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  BOOST_ASSERT_MSG(loc.space->getType() == ob::STATE_SPACE_SO2, "Rotation component not SO2");
  return si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::SO2StateSpace::StateType>();
}

double GetRotationYaw(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state) {
  BOOST_ASSERT_MSG(HasRotation(si), "Workspace has no rotation component. Please check tags");
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::ROTATION);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  if (loc.space->getType() == ob::STATE_SPACE_SO2)
    return si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::SO2StateSpace::StateType>()->value;
  else
    return GetRotationVector3d(si, state).z();
}

const ob::SO3StateSpace::StateType* GetRotationStateSO3(const ob::SpaceInformationPtr& si, const ob::State *state) {
  BOOST_ASSERT_MSG(HasRotation(si), "Workspace has no rotation component. Please check tags");
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::ROTATION);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  BOOST_ASSERT_MSG(loc.space->getType() == ob::STATE_SPACE_SO3, "Rotation component not SO3");
  return si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::SO3StateSpace::StateType>();
}

ob::SO3StateSpace::StateType* GetRotationStateSO3(const ob::SpaceInformationPtr& si, ob::State *state) {
  BOOST_ASSERT_MSG(HasRotation(si), "Workspace has no rotation component. Please check tags");
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::ROTATION);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  BOOST_ASSERT_MSG(loc.space->getType() == ob::STATE_SPACE_SO3, "Rotation component not SO3");
  return si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::SO3StateSpace::StateType>();
}


Eigen::Vector3d GetRotationVector3d(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state) {
  BOOST_ASSERT_MSG(HasRotation(si), "Workspace has no rotation component. Please check tags");
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::ROTATION);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  if (loc.space->getType() == ob::STATE_SPACE_SO2)
    return Eigen::Vector3d(0, 0, si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::SO2StateSpace::StateType>()->value);
  else
    return su::ConvertSO3toEuler ( GetRotationStateSO3(si, state)  );
}

void SetRotationVector3d(const ompl::base::SpaceInformationPtr& si, const Eigen::Vector3d &rot_vec, ompl::base::State *state) {
  su::ConvertEulerToSO3(rot_vec[0], rot_vec[1], rot_vec[2], GetRotationStateSO3(si, state));
}

const ob::RealVectorStateSpace::StateType* GetTangentRotationState(const ob::SpaceInformationPtr& si, const ob::State *state, const ob::StateSpace* &space) {
  BOOST_ASSERT_MSG(HasTangentRotation(si), "Workspace has no TangentRotation component. Please check tags");
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::TANGENT_ROTATION);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  space = loc.space;
  return si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::RealVectorStateSpace::StateType>();
}

ob::RealVectorStateSpace::StateType* GetTangentRotationState(const ob::SpaceInformationPtr& si, ob::State *state) {
  BOOST_ASSERT_MSG(HasTangentRotation(si), "Workspace has no TangentRotation component. Please check tags");
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::TANGENT_ROTATION);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  return si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::RealVectorStateSpace::StateType>();
}

ob::RealVectorStateSpace::StateType* GetTangentRotationState(const ob::SpaceInformationPtr& si, ob::State *state, const ob::StateSpace* &space) {
  BOOST_ASSERT_MSG(HasTangentRotation(si), "Workspace has no TangentRotation component. Please check tags");
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::TANGENT_ROTATION);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  space = loc.space;
  return si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::RealVectorStateSpace::StateType>();
}


double GetTangentRotationYaw(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state) {
  const ob::StateSpace *space = NULL;
  const ob::RealVectorStateSpace::StateType* tangent_rotation = GetTangentRotationState(si, state, space);
  if (space->getDimension() == 1)
    return tangent_rotation->values[0];
  else
    return tangent_rotation->values[2];
}

Eigen::Vector3d GetTangentRotationVector3d(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state) {
  const ob::StateSpace *space = NULL;
  const ob::RealVectorStateSpace::StateType* tangent_rotation = GetTangentRotationState(si, state, space);
  //BOOST_ASSERT_MSG(space->getDimension() == 3, "Tangent rotation component is not 3d");
  if (space->getDimension() == 3)
    return Eigen::Vector3d(tangent_rotation->values[0], tangent_rotation->values[1], tangent_rotation->values[2]);
  else
    return Eigen::Vector3d(0, 0, tangent_rotation->values[0]);
}


double GetTime(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state) {
  BOOST_ASSERT_MSG(HasTime(si), "Workspace has no Time component. Please check tags");
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::TIME);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  return si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::TimeStateSpace::StateType>()->position;
}

ompl::base::TimeStateSpace::StateType* GetTimeState(const ompl::base::SpaceInformationPtr& si, ompl::base::State *state) {
  BOOST_ASSERT_MSG(HasTime(si), "Workspace has no Time component. Please check tags");
  const std::string name = si->workspace_information().workspace_map().at(ompl_base::WorkspaceTags::TIME);
  ob::StateSpace::SubstateLocation loc = si->getStateSpace()->getSubstateLocationsByName().at(name);
  return si->getStateSpace()->getSubstateAtLocation(state, loc)->as<ob::TimeStateSpace::StateType>();
}

}
}
}

