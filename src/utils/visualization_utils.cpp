/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * visualization_utils.cpp
 *
 *  Created on: Jun 26, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/utils/visualization_utils.h"
#include "ompl/base/ScopedState.h"
#include "ompl/base/StateSpace.h"
#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "ompl/base/PlannerDataGraph.h"
#include "planning_common/states/se3_tangent_bundle.h"

namespace wu = ca::planning_common::workspace_utils;

namespace ca {
namespace planning_common {
namespace visualization_utils {

visualization_msgs::Marker GetMarker(boost::function< const ompl::base::State* (unsigned int)> GetStateFn,
                                     boost::function< std::size_t (void)> GetNumState,
                                     const ompl::base::SpaceInformationPtr& si,
                                     double scale, double r, double g, double b, double a) {

  visualization_msgs::Marker m;
  m.header.frame_id = "world";
  m.header.stamp = ros::Time::now();
  m.ns = "path";
  m.id = 0;
  m.action = visualization_msgs::Marker::ADD;
  m.scale.x = scale;
  m.type = visualization_msgs::Marker::LINE_STRIP; //correct marker type
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;

  // Fill up msg and return it
  geometry_msgs::Point ps;
  for (std::size_t i = 0 ; i < GetNumState(); i++) {
    Eigen::VectorXd vec = wu::GetTranslationVectorXd(si, GetStateFn(i));
    ps.x = vec.x();
    ps.y = vec.y();
    ps.z = vec.rows() == 3 ? vec.z() : 0;
    m.points.push_back(ps);
  }
  return m;

}

visualization_msgs::Marker GetMarkerLineList(const PathWaypoint& path, double scale, double r, double g, double b, double a) {

  visualization_msgs::Marker m;
  m.header.frame_id = "world";
  m.header.stamp = ros::Time::now();
  m.ns = "path";
  m.id = 0;
  m.action = visualization_msgs::Marker::ADD;
  m.scale.x = scale;
  m.type = visualization_msgs::Marker::LINE_LIST; //correct marker type
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;

  // Fill up msg and return it
  geometry_msgs::Point ps;
  for (std::size_t i = 0 ; i < path.NumberOfWaypoints(); i++) {
    Eigen::VectorXd vec = wu::GetTranslationVectorXd(path.getSpaceInformation(), path.GetWaypoint(i));
    ps.x = vec.x();
    ps.y = vec.y();
    ps.z = vec.rows() == 3 ? vec.z() : 0;
    m.points.push_back(ps);
    if (i != 0 && i+1 < path.NumberOfWaypoints())
      m.points.push_back(ps);
  }
  return m;

}

visualization_msgs::Marker GetMarker(const ompl::geometric::PathGeometric& path, double scale, double r, double g, double b, double a) {
  return GetMarker( boost::bind( static_cast< const ompl::base::State* (ompl::geometric::PathGeometric::*)( unsigned int ) const > (&ompl::geometric::PathGeometric::getState), &path, _1),
                    boost::bind(&ompl::geometric::PathGeometric::getStateCount, &path),
                    path.getSpaceInformation(),
                    scale, r, g, b, a);
}

visualization_msgs::Marker GetMarker(const PathWaypoint& path, double scale, double r, double g, double b, double a) {
  return GetMarker( boost::bind( static_cast< const ompl::base::State* (PathWaypoint::*)( unsigned int ) const > (&PathWaypoint::GetWaypoint), &path, _1),
                    boost::bind(&PathWaypoint::NumberOfWaypoints, &path),
                    path.getSpaceInformation(),
                    scale, r, g, b, a);
}




visualization_msgs::Marker GetMarker(const ca_nav_msgs::WorkspaceTrajectory &traj, double scale, double r, double g, double b, double a) {
  visualization_msgs::Marker m;
  m.header.frame_id = "world";
  m.header.stamp = ros::Time::now();
  m.ns = "path";
  m.id = 0;
  m.action = visualization_msgs::Marker::ADD;
  m.scale.x = scale;
  m.type = visualization_msgs::Marker::LINE_STRIP; //correct marker type
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;

  // Fill up msg and return it
  geometry_msgs::Point ps;
  for (std::size_t i = 0 ; i < traj.waypoint.size(); i++) {
    ps.x = traj.waypoint[i].pose.pose.position.x;
    ps.y = traj.waypoint[i].pose.pose.position.y;
    ps.z = traj.waypoint[i].pose.pose.position.z;
    m.points.push_back(ps);
  }
  return m;
}

visualization_msgs::Marker GetMarker(const std::vector<ca::planning_common::PathWaypoint >& path_set, double scale, double r, double g, double b, double a) {
  visualization_msgs::Marker m;
  m.header.frame_id = "world";
  m.header.stamp = ros::Time::now();
  m.ns = "path";
  m.id = 0;
  m.action = visualization_msgs::Marker::ADD;
  m.scale.x = scale;
  m.type = visualization_msgs::Marker::LINE_LIST; //correct marker type
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;
  if (path_set.size() == 0)
    return m;
  m = GetMarkerLineList(path_set.front(), scale, r, g, b, a);
  for (unsigned int i = 1; i < path_set.size(); i++) {
    visualization_msgs::Marker temp_m = ca::planning_common::visualization_utils::GetMarkerLineList(path_set[i], scale, r, g, b, a);
    m.points.insert(m.points.end(), temp_m.points.begin(), temp_m.points.end());
  }
  return m;
}


visualization_msgs::MarkerArray GetMarkerArrayGradient(const Eigen::MatrixXd &gradient, const PathWaypoint& path, double gradient_scale, double scale, double r, double g, double b, double a) {
  visualization_msgs::MarkerArray ma;
  ma.markers.push_back(GetMarker(path, scale, r, g, b, a));

  visualization_msgs::Marker m;
  m.header.frame_id = "world";
  m.ns = "gradient";
  m.id = 0;
  m.header.stamp = ros::Time::now();
  m.action = visualization_msgs::Marker::ADD;
  m.scale.x = scale;
  m.scale.y = 1.5*scale;
  m.type = visualization_msgs::Marker::LINE_STRIP; //correct marker type
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;

  BOOST_ASSERT_MSG(gradient.rows() == path.NumberOfWaypoints(), "Gradient size must match the waypoints");
  BOOST_ASSERT_MSG(gradient.cols() >= wu::GetTranslationDimension(path.getSpaceInformation()), "Gradient dimension must be more than workspace");
  // Fill up msg and return it
  geometry_msgs::Point ps;
  for (std::size_t i = 0 ; i < path.NumberOfWaypoints(); i++) {
    Eigen::VectorXd vec = wu::GetTranslationVectorXd(path.getSpaceInformation(), path.GetWaypoint(i));
    visualization_msgs::Marker marker_gradient(m);
    ps.x = vec.x();
    ps.y = vec.y();
    ps.z = vec.rows() == 3 ? vec.z() : 0;
    marker_gradient.points.push_back(ps);

    ps.x = vec.x() + gradient_scale*gradient(i, 0);
    ps.y = vec.y() + gradient_scale*gradient(i, 1);
    ps.z = vec.rows() == 3 ? vec.z() + gradient_scale*gradient(i, 2): 0;
    marker_gradient.points.push_back(ps);

    ma.markers.push_back(marker_gradient);
    m.id ++;
  }

  return ma;
}

visualization_msgs::Marker GetGraph(const ompl::base::PlannerData &data, unsigned int resolution, double scale, double r, double g,  double b, double a) {
  namespace ob = ompl::base;
  visualization_msgs::Marker m;
  m.action = visualization_msgs::Marker::ADD;
  m.scale.x = scale;
  m.type = visualization_msgs::Marker::LINE_LIST; //correct marker type
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;

  m.header.frame_id = "world";
  m.ns = "graph";
  m.id = 0;
  m.header.stamp = ros::Time::now();
  m.scale.x = scale;
  m.scale.y = 1.5*scale;
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;

  // Get graph
  ob::PlannerData::Graph::Type graph = data.toBoostGraph();
  ob::PlannerData::Graph::EIterator ei, ei_end;
  boost::property_map<ob::PlannerData::Graph::Type, vertex_type_t>::type vertices = get(vertex_type_t(), graph);

  // Fill up msg and return
  geometry_msgs::Point ps;
  for (boost::tie(ei, ei_end) = boost::edges(graph); ei != ei_end; ++ei) {
    // Get path corresponding to edge
    std::vector<ob::State*> block;
    unsigned int ans = data.getSpaceInformation()->getMotionStates(vertices[boost::source(*ei, graph)]->getState(), vertices[boost::target(*ei, graph)]->getState(), block, resolution, true, true);
    BOOST_ASSERT_MSG((int)ans == ans && block.size() == ans, "Internal error in path interpolation. Incorrect number of intermediate states. Please contact the developers.");
    // Enter path in marker
    for (std::vector<ob::State*>::iterator it = block.begin() ; it != block.end(); ++it) {
      Eigen::VectorXd vec = wu::GetTranslationVectorXd(data.getSpaceInformation(), *it);
      ps.x = vec.x();
      ps.y = vec.y();
      ps.z = vec.rows() == 3 ? vec.z() : 0;
      m.points.push_back(ps);
      if (it != block.begin() && boost::next(it) != block.end())
        m.points.push_back(ps);
      data.getSpaceInformation()->freeState(*it);
    }
  }
  return m;
}

visualization_msgs::Marker GetMarker(const nav_msgs::Odometry &odom, double scale, double r, double g, double b, double a) {
  visualization_msgs::Marker m;
  m.action = visualization_msgs::Marker::ADD;
  m.scale.x = scale;
  m.scale.y = m.scale.x / 10;
  m.scale.z = m.scale.y;
  m.type = visualization_msgs::Marker::ARROW; //correct marker type
  m.color.r = r; m.color.g = g; m.color.b = b; m.color.a = a;
  m.header.frame_id = "world";

  m.pose = odom.pose.pose;
  return m;
}

visualization_msgs::Marker VisualizeSpeedGrid(const PathWaypoint &path, double grid_res_ms, double ms2m, double num_levels, double scale) {
  if (path.NumberOfWaypoints() == 0)
    return visualization_msgs::Marker();

  visualization_msgs::Marker m;
  m.header.frame_id = "world";
  m.header.stamp = ros::Time::now();
  m.ns = "speed_grid";
  m.id = 0;
  m.action = visualization_msgs::Marker::ADD;
  m.scale.x = scale;
  m.type = visualization_msgs::Marker::LINE_LIST; //correct marker type
  //m.color.r = 0.5; m.color.g = 0.5; m.color.b = 0.5; m.color.a = 1.0;
  std_msgs::ColorRGBA gray, orange;
  gray.r = 0.5; gray.g = 0.5; gray.b = 0.5; gray.a = 1.0;
  //cyan.r = 67/255.0; cyan.g = 232/255.0; cyan.b = 166/255.0; cyan.a = 1.0;
  orange.r = 235/255.0; orange.g = 123/255.0; orange.b = 12/255.0; orange.a = 1.0;


  // Fill up msg and return it
  geometry_msgs::Point ps;
  for (std::size_t i = 0 ; i < path.NumberOfWaypoints() - 1; i++) {
    Eigen::Vector3d vec = path.GetWaypoint(i)->as<SE3TangentBundle::StateType>()->GetEigenForm().position;
    Eigen::Vector3d vec2 = path.GetWaypoint(i+1)->as<SE3TangentBundle::StateType>()->GetEigenForm().position;

    ps.x = vec.x();
    ps.y = vec.y();
    ps.z = vec.z();
    m.points.push_back(ps);
    m.colors.push_back(gray);

    ps.z = vec.z() - num_levels * grid_res_ms*ms2m;
    m.points.push_back(ps);
    m.colors.push_back(gray);

    for (int i = 0; i <= num_levels; i++) {
      ps.x = vec.x();
      ps.y = vec.y();
      ps.z = vec.z() - i * grid_res_ms*ms2m;
      m.points.push_back(ps);
      m.colors.push_back(gray);

      ps.x = vec2.x();
      ps.y = vec2.y();
      ps.z = vec2.z() - i * grid_res_ms*ms2m;
      m.points.push_back(ps);
      m.colors.push_back(gray);
    }

    ps.x = vec.x();
    ps.y = vec.y();
    ps.z = vec.z() - path.GetWaypoint(i)->as<SE3TangentBundle::StateType>()->GetEigenForm().velocity.norm() * ms2m;
    m.points.push_back(ps);
    m.colors.push_back(orange);

    ps.x = vec2.x();
    ps.y = vec2.y();
    ps.z = vec2.z() - path.GetWaypoint(i+1)->as<SE3TangentBundle::StateType>()->GetEigenForm().velocity.norm() * ms2m;
    m.points.push_back(ps);
    m.colors.push_back(orange);
  }
  return m;
}


}
}
}
