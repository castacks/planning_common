/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * state_utils.cpp
 *
 *  Created on: Jun 25, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/utils/state_utils.h"
#include "planning_common/utils/workspace_utils.h"
#include "planning_common/state_validity/bounds_state_validity_checker.h"
#include <tf/transform_datatypes.h>
#include <geom_cast/geom_cast.hpp>

namespace ob = ompl::base;

namespace ca {
namespace planning_common {
namespace state_utils {

namespace wu = workspace_utils;

ob::ScopedState<ob::SO3StateSpace> ConvertEulerToSO3 (double roll, double pitch, double yaw) {
  tf::Quaternion quat = tf::createQuaternionFromRPY(roll, pitch, yaw);
  ob::ScopedState<ob::SO3StateSpace> so3_state(ob::StateSpacePtr(new ob::SO3StateSpace()));
  so3_state->setAxisAngle(quat.getAxis().x(), quat.getAxis().y(), quat.getAxis().z(), quat.getAngle());
  return so3_state;
}

void ConvertEulerToSO3 (double roll, double pitch, double yaw, ompl::base::SO3StateSpace::StateType *state) {
  tf::Quaternion quat = tf::createQuaternionFromRPY(roll, pitch, yaw);
  state->setAxisAngle(quat.getAxis().x(), quat.getAxis().y(), quat.getAxis().z(), quat.getAngle());
}

Eigen::Vector3d ConvertSO3toEuler (const ompl::base::SO3StateSpace::StateType *state) {
  tf::Quaternion quat;
  Eigen::Vector3d euler;
  quat.setX(state->x);
  quat.setY(state->y);
  quat.setZ(state->z);
  quat.setW(state->w);
  tf::Matrix3x3(quat).getRPY(euler[0], euler[1], euler[2]);
  return euler;
}

void ConvertAnyToSE3TangentBundle (const ompl::base::State* input_state, const ompl::base::SpaceInformationPtr &si, SE3TangentBundle::StateType *se3tb_state) {
  if (wu::HasTime(si))
    se3tb_state->GetTime().position = wu::GetTime(si, input_state);
  else
    se3tb_state->GetTime().position = 0;

  if (wu::HasTranslation(si)) {
    Eigen::VectorXd vec = wu::GetTranslationVectorXd(si, input_state);
    vec.rows() == 3 ? se3tb_state->GetPose().setXYZ(vec[0], vec[1], vec[2]) : se3tb_state->GetPose().setXYZ(vec[0], vec[1], 0);
  } else {
    se3tb_state->GetPose().setXYZ(0, 0, 0);
  }

  if (wu::HasTangentTranslation(si)) {
    Eigen::VectorXd vec = wu::GetTangentTranslationVectorXd(si, input_state);
    vec.rows() == 3 ? se3tb_state->GetTangentPose().SetXYZDot(vec[0], vec[1], vec[2]) : se3tb_state->GetTangentPose().SetXYZDot(vec[0], vec[1], 0);
  } else {
    se3tb_state->GetTangentPose().SetXYZDot(0, 0, 0);
  }

  if (wu::HasRotation(si)) {
    Eigen::VectorXd vec3d = wu::GetRotationVector3d(si, input_state);
    ConvertEulerToSO3 (vec3d[0], vec3d[1], vec3d[2], &se3tb_state->GetPose().rotation());
  } else {
    ConvertEulerToSO3 (0, 0, 0, &se3tb_state->GetPose().rotation());
  }

  if (wu::HasTangentRotation(si)) {
    Eigen::VectorXd vec3d = wu::GetTangentRotationVector3d(si, input_state);
    se3tb_state->GetTangentPose().SetPhiThetaPsiDot(vec3d[0], vec3d[1], vec3d[2]);
  } else {
    se3tb_state->GetTangentPose().SetPhiThetaPsiDot(0, 0, 0);
  }
}

void ConvertOdomToSE3TangentBundle(const nav_msgs::Odometry &odom, SE3TangentBundle::StateType *se3tb_state) {
  se3tb_state->GetTime().position = odom.header.stamp.toSec();
  se3tb_state->GetPose().setXYZ(odom.pose.pose.position.x, odom.pose.pose.position.y, odom.pose.pose.position.z);
  se3tb_state->GetPose().rotation().x = odom.pose.pose.orientation.x;
  se3tb_state->GetPose().rotation().y = odom.pose.pose.orientation.y;
  se3tb_state->GetPose().rotation().z = odom.pose.pose.orientation.z;
  se3tb_state->GetPose().rotation().w = odom.pose.pose.orientation.w;
  se3tb_state->GetTangentPose().SetXYZDot(odom.twist.twist.linear.x, odom.twist.twist.linear.y, odom.twist.twist.linear.z);
  se3tb_state->GetTangentPose().SetPhiThetaPsiDot(odom.twist.twist.angular.x, odom.twist.twist.angular.y, odom.twist.twist.angular.z);
}

void ConvertSE3TangentBundleToOdom(const SE3TangentBundle::StateType *se3tb_state, nav_msgs::Odometry &odom) {
  odom.header.stamp = ros::Time(se3tb_state->GetTime().position);
  odom.pose.pose.position.x = se3tb_state->GetPose().getX();
  odom.pose.pose.position.y = se3tb_state->GetPose().getY();
  odom.pose.pose.position.z = se3tb_state->GetPose().getZ();
  odom.pose.pose.orientation.x = se3tb_state->GetPose().rotation().x;
  odom.pose.pose.orientation.y = se3tb_state->GetPose().rotation().y;
  odom.pose.pose.orientation.z = se3tb_state->GetPose().rotation().z;
  odom.pose.pose.orientation.w = se3tb_state->GetPose().rotation().w;
  odom.twist.twist.linear.x = se3tb_state->GetTangentPose().GetXDot();
  odom.twist.twist.linear.y = se3tb_state->GetTangentPose().GetYDot();
  odom.twist.twist.linear.z = se3tb_state->GetTangentPose().GetZDot();
  odom.twist.twist.angular.x = se3tb_state->GetTangentPose().GetPhiDot();
  odom.twist.twist.angular.y = se3tb_state->GetTangentPose().GetThetaDot();
  odom.twist.twist.angular.z = se3tb_state->GetTangentPose().GetPsiDot();
}

bool AreSpaceInformationSame(const ob::SpaceInformationPtr &si1, const ob::SpaceInformationPtr &si2) {
  return si1->getStateSpace()->getType() == si1->getStateSpace()->getType();
}


ompl::base::SpaceInformationPtr GetStandardSE3TangentBundle() {
  ob::StateSpacePtr space(new planning_common::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(-0.5*std::numeric_limits<double>::max() + std::numeric_limits<double>::epsilon());//std::numeric_limits<double>::min());
  bounds.setHigh(0.5*std::numeric_limits<double>::max());
  space->as<planning_common::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ompl::base::SpaceInformationPtr si(new ompl::base::SpaceInformation(space));
  si->setStateValidityChecker(ob::StateValidityCheckerPtr(new BoundsStateValidityChecker(si)));
  si->setStateValidityChecker(ob::StateValidityCheckerPtr(new ob::AllValidStateValidityChecker(si)));
  si->setup();
  return si;
}

tf::Transform GetTFfromSE3(const ompl::base::State *state) {
  tf::Transform transform;
  transform.setOrigin( ca::point_cast<tf::Vector3>(state->as<planning_common::SE3TangentBundle::StateType>()->GetEigenForm().position));
  tf::Quaternion q;
  q.setRPY(state->as<planning_common::SE3TangentBundle::StateType>()->GetEigenForm().orientation.x(),
           state->as<planning_common::SE3TangentBundle::StateType>()->GetEigenForm().orientation.y(),
           state->as<planning_common::SE3TangentBundle::StateType>()->GetEigenForm().orientation.z());
  transform.setRotation(q);
  return transform;
}



}
}
}


