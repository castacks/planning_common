/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * bounds_state_validity_checker.cpp
 *
 *  Created on: Aug 24, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/state_validity/bounds_state_validity_checker.h"
#include "ompl/base/SpaceInformation.h"
namespace ca {
namespace planning_common {

bool BoundsStateValidityChecker::isValid(const ompl::base::State* state) const {
    return si_->satisfiesBounds(state);
}

}  // namespace planning_common
}  // namespace ca

