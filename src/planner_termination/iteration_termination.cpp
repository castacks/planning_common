/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * iteration_termination.cpp
 *
 *  Created on: Jun 15, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/planner_termination/iteration_termination.h"
#include "boost/bind.hpp"

namespace ob = ompl::base;

namespace ca {
namespace planning_common {

template<typename T, typename v>
static bool GreaterFunctional(T fn, v value) {
    return fn() > value;
}

template<typename T, typename v>
static bool LesserFunctional(T fn, v value) {
    return fn() < value;
}

ompl::base::PlannerTerminationCondition NumIterationsPlannerTerminationCondition(const GetIterationsFn &get_iterations_fn, unsigned int max_num_iterations) {
    return ob::PlannerTerminationCondition(boost::bind(&GreaterFunctional<GetIterationsFn, unsigned int>, get_iterations_fn, max_num_iterations));
}

ompl::base::PlannerTerminationCondition GradientConvergencePlannerTerminationCondition(const GetGradientNormFn &get_gradient_norm_fn, double min_gradient_norm) {
  return ob::PlannerTerminationCondition(boost::bind(&LesserFunctional<GetGradientNormFn, double>, get_gradient_norm_fn, min_gradient_norm));
}

ompl::base::PlannerTerminationCondition RelCostDecreasePlannerTerminationCondition(const GetRelCostDecreaseFn &get_rel_cost_dec_fn, double thr_cost_dec) {
  return ob::PlannerTerminationCondition(boost::bind(&LesserFunctional<GetRelCostDecreaseFn, double>, get_rel_cost_dec_fn, thr_cost_dec));
}

}  // namespace planning_common
}  // namespace ca


