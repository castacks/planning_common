/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * simple_grid_cost_map.cpp
 *
 *  Created on: Jun 8, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/data_structures/simple_grid_cost_map.h"
#include <iostream>     // cout, endl
#include <fstream>      // fstream
#include <vector>
#include <string>
#include <algorithm>    // copy
#include <iterator>     // ostream_operator
#include <ros/ros.h>
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>

namespace ca{
namespace planning_common{

bool SimpleGridCostMap::Initialize(const Eigen::Vector2d &origin, const Eigen::Vector2d &dimension, std::string filename) {
  has_initialized_ = false;
  origin_ = origin;
  dimension_ = dimension;

  std::ifstream in(filename.c_str());
  if (!in.is_open()) {
    ROS_ERROR_STREAM("Cannot open file");
    return false;
  }

  typedef boost::tokenizer< boost::escaped_list_separator<char> > Tokenizer;
  std::string separator1("");//dont let quoted arguments escape themselves
  std::string separator2(" ");//split on spaces
  std::string separator3("\"\'");//let it have quoted arguments  boost::escaped_list_separator<char> els(separator);
  boost::escaped_list_separator<char> els(separator1, separator2, separator3);
  std::vector< std::string > string_vector;
  std::string line;

  BOOST_ASSERT_MSG(getline(in,line), "Cant read line");
  {
    Tokenizer tok(line, els);
    string_vector.assign(tok.begin(),tok.end());
    BOOST_ASSERT_MSG(string_vector[0].compare("Dim:") == 0, "Expected Dim:");
    xdim_ = boost::lexical_cast<unsigned int>(string_vector[1]);
    ydim_ = boost::lexical_cast<unsigned int>(string_vector[2]);
  }

  BOOST_ASSERT_MSG(getline(in,line), "Cant read line");
  value_ = std::vector<double>(xdim_*ydim_);
  gradient_ = Vector2dVector(xdim_*ydim_);

  for (size_t x = 0; x < xdim_; x++) {
    BOOST_ASSERT_MSG(getline(in,line), "Cant read line");
    Tokenizer tok(line, els);
    string_vector.assign(tok.begin(),tok.end());
    BOOST_ASSERT_MSG(string_vector.size() == ydim_, "Line size not ydimension");
    for (size_t y = 0; y < ydim_; y++)
      value_[x*ydim_ + y] = boost::lexical_cast<double>(string_vector[y]);
  }

  for (size_t dim = 0; dim < 2; dim ++) {
    BOOST_ASSERT_MSG(getline(in,line), "Cant read line");
    for (size_t x = 0; x < xdim_; x++) {
      BOOST_ASSERT_MSG(getline(in,line), "Cant read line");
      Tokenizer tok(line, els);
      string_vector.assign(tok.begin(),tok.end());
      BOOST_ASSERT_MSG(string_vector.size() == ydim_, "Line size not ydimension");
      for (size_t y = 0; y < ydim_; y++)
        gradient_[x*ydim_ + y][dim] = boost::lexical_cast<double>(string_vector[y]);
    }
  }

  has_initialized_ = true;
  return true;
}

double SimpleGridCostMap::GetValue(const Eigen::VectorXd &query_gen){
  BOOST_ASSERT_MSG(query_gen.size() == 2, "Expect to be 2D");
  Eigen::Vector2d query(query_gen[0], query_gen[1]);
  BOOST_ASSERT_MSG(has_initialized_, "Expect to be initialized");
  if(!IsInGrid(query))
    return 0;
  return GetResolution()*value_[GetIndex(query)];
}

Eigen::VectorXd SimpleGridCostMap::GetGradient(const Eigen::VectorXd &query_gen) {
  BOOST_ASSERT_MSG(query_gen.size() == 2, "Expect to be 2D");
  Eigen::Vector2d query(query_gen[0], query_gen[1]);
  BOOST_ASSERT_MSG(has_initialized_, "Expect to be initialized");
  if(!IsInGrid(query))
    return Eigen::Vector2d::Zero();
  //std::cout<<"\n"<<query.transpose()<<" "<<GetX(query) <<" "<<GetY(query)<<"\n";
  return gradient_[GetIndex(query)]*GetResolution();
}

visualization_msgs::MarkerArray SimpleGridCostMap::GetMarkerArray() {
  visualization_msgs::MarkerArray marker_array;

  visualization_msgs::Marker marker;
  marker.header.frame_id = "world";
  marker.header.stamp = ros::Time::now();
  marker.ns = "gradient";
  marker.id = 0;
  marker.type = visualization_msgs::Marker::ARROW;
  marker.action = visualization_msgs::Marker::ADD;
  marker.pose.orientation.w = 1.0;
  marker.scale.x = 0.1*GetResolution();
  marker.scale.y = 0.15*GetResolution();
  marker.color.a = 1.0;
  marker.color.r = 0.0;
  marker.color.g = 1.0;
  marker.color.b = 0.0;

  for (size_t x = 0; x < xdim_; x++) {
    for (size_t y = 0; y < ydim_; y++) {
      marker.points.clear();
      marker.id++;
      geometry_msgs::Point ps;
      ROS_INFO_STREAM(" "<<GetXWorld(x)<<" "<<GetYWorld(y));
      ps.x = GetXWorld(x);
      ps.y = GetYWorld(y);
      marker.points.push_back(ps);
      ps.x += GetResolution()*gradient_[x*ydim_ + y][0];
      ps.y += GetResolution()*gradient_[x*ydim_ + y][1];
      marker.points.push_back(ps);
      marker_array.markers.push_back(marker);
    }
  }

  return marker_array;
}

bool SimpleGridCostMap::IsInGrid(const Eigen::Vector2d &query) {
  if (query[0] >= origin_[0] && query[1] >= origin_[1] && query[0] <= origin_[0] + dimension_[0] && query[1] <= origin_[1] + dimension_[1])
    return true;
  else
    return false;
}

unsigned int SimpleGridCostMap::GetIndex (const Eigen::Vector2d &query) {
  return GetX(query)*ydim_ + GetY(query);
}

}  // namepsace ca
}  // namespace planning_common
