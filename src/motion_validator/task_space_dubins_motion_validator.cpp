/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * task_space_dubins_motion_validation.cpp
 *
 *  Created on: Aug 24, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/motion_validator/task_space_dubins_motion_validator.h"
#include "Eigen/Dense"
#include "ompl/base/spaces/DubinsStateSpace.h"
#include "ompl/base/SpaceInformation.h"
#include "ompl/util/Exception.h"
#include <queue>
#include <boost/math/constants/constants.hpp>

namespace ca {
namespace planning_common {

namespace ob = ompl::base;

namespace {

const double twopi = 2. * boost::math::constants::pi<double>();
const double pi_2 = 0.5 * boost::math::constants::pi<double>();

const double DUBINS_EPS = 1e-6;
const double DUBINS_ZERO = -1e-9;

inline double mod2pi(double x) {
  if (x<0 && x>DUBINS_ZERO) return 0;
  return x - twopi * floor(x / twopi);
}

/**
 *
 * @param dx = (xgoal - x)/rho
 * @param dy = (ygoal - y)/rho
 * @param psi = heading at start
 * @return
 */
TaskSpaceDubinsMotionValidator::SingleDubinsPath DubinsLS(double dx, double dy, double psi) {
  Eigen::Vector2d v1;
  v1 << dx - cos(psi + pi_2), dy - sin(psi + pi_2);
  double psi1 = mod2pi(atan2(v1.y(),v1.x()) - psi);
  double d1 = v1.norm();
  if (d1*d1 - 1 > 0) {
    double l1 = sqrt(d1*d1 - 1);
    double alpha = atan(1/l1);
    return TaskSpaceDubinsMotionValidator::SingleDubinsPath(TaskSpaceDubinsMotionValidator::single_dubins_path_type_[0], mod2pi(psi1 + alpha), l1);
  }
  return TaskSpaceDubinsMotionValidator::SingleDubinsPath();
}

TaskSpaceDubinsMotionValidator::SingleDubinsPath DubinsRS(double dx, double dy, double psi) {
  Eigen::Vector2d v1;
  v1 << dx - cos(psi - pi_2), dy - sin(psi - pi_2);
  double psi1 = mod2pi(atan2(v1.y(),v1.x()) - psi);
  double d1 = v1.norm();
  if (d1*d1 - 1 > 0) {
    double l1 = sqrt(d1*d1 - 1);
    double alpha = atan(1/l1);
    return TaskSpaceDubinsMotionValidator::SingleDubinsPath(TaskSpaceDubinsMotionValidator::single_dubins_path_type_[1], mod2pi(twopi - psi1 + alpha), l1);
  }
  return TaskSpaceDubinsMotionValidator::SingleDubinsPath();
}

TaskSpaceDubinsMotionValidator::SingleDubinsPath SingleDubins(double dx, double dy, double psi) {
  if (sqrt(dx*dx + dy*dy) < DUBINS_EPS && fabs( sin(psi - atan2(dy,dx)) ) < DUBINS_EPS)
    return TaskSpaceDubinsMotionValidator::SingleDubinsPath(TaskSpaceDubinsMotionValidator::single_dubins_path_type_[0], 0, sqrt(dx*dx + dy*dy));

  TaskSpaceDubinsMotionValidator::SingleDubinsPath path(DubinsLS(dx, dy, psi)), tmp(DubinsRS(dx, dy, psi));
  double len, minLength = path.length();

  if ((len = tmp.length()) < minLength) {
    minLength = len;
    path = tmp;
  }

  return path;
}

} // namespace

const TaskSpaceDubinsMotionValidator::DubinsPathSegmentType TaskSpaceDubinsMotionValidator::single_dubins_path_type_[2][2] = {
    { DUBINS_LEFT, DUBINS_STRAIGHT},
    { DUBINS_RIGHT, DUBINS_STRAIGHT}
};


void TaskSpaceDubinsMotionValidator::DefaultSettings(void) {
  stateSpace_ = dynamic_cast<ob::DubinsStateSpace*>(si_->getStateSpace().get());
  if (!stateSpace_)
    throw ompl::Exception("No state space for motion validator");
}

bool TaskSpaceDubinsMotionValidator::CheckMotion(const ob::State *s1, const ob::State *s2, ob::State *new_state) const {
  /* assume motion starts in a valid configuration so s1 is valid */
  if (!si_->isValid(s2))
      return false;

  bool result = true, firstTime = true;
  SingleDubinsPath path;
  int nd = stateSpace_->validSegmentCount(s1, s2);

  /* initialize the queue of test positions */
  std::queue< std::pair<int, int> > pos;
  if (nd >= 2)
  {
      pos.push(std::make_pair(1, nd - 1));

      /* temporary storage for the checked state */
      ob::State *test = si_->allocState();

      /* repeatedly subdivide the path segment in the middle (and check the middle) */
      while (!pos.empty())
      {
          std::pair<int, int> x = pos.front();

          int mid = (x.first + x.second) / 2;
          Interpolate(s1, s2, (double)mid / (double)nd, firstTime, path, test);

          if (!si_->isValid(test))
          {
              result = false;
              break;
          }
          pos.pop();

          if (x.first < mid)
              pos.push(std::make_pair(x.first, mid - 1));
          if (x.second > mid)
              pos.push(std::make_pair(mid + 1, x.second));
      }

      si_->freeState(test);
  }

  if (result)
    Interpolate(s1, s2, 1.0, firstTime, path, new_state);

  return result;
}

void TaskSpaceDubinsMotionValidator::GetEndState(const ompl::base::State *s1, const ompl::base::State *s2, ompl::base::State *new_state) const {
  bool firstTime = true;
  SingleDubinsPath path;
  Interpolate(s1, s2, 1.0, firstTime, path, new_state);
}

double TaskSpaceDubinsMotionValidator::Distance(const ob::State *state1, const ob::State *state2) const {
  return stateSpace_->rho() * ComputeSingleDubinsPath(state1, state2).length();
}

TaskSpaceDubinsMotionValidator::SingleDubinsPath TaskSpaceDubinsMotionValidator::ComputeSingleDubinsPath(const ompl::base::State *state1, const ompl::base::State *state2) const {
  const ob::DubinsStateSpace::StateType *s1 = static_cast<const ob::DubinsStateSpace::StateType*>(state1);
  const ob::DubinsStateSpace::StateType *s2 = static_cast<const ob::DubinsStateSpace::StateType*>(state2);
  double rho  = stateSpace_->rho();
  double x1 = s1->getX(), y1 = s1->getY(), th1 = s1->getYaw();
  double x2 = s2->getX(), y2 = s2->getY();
  double dx = (x2 - x1)/rho, dy = (y2 - y1)/rho;
  return SingleDubins(dx, dy, th1);
}

void TaskSpaceDubinsMotionValidator::Interpolate(const ob::State *from, const ob::State *to, const double t,
                                                 bool& firstTime, SingleDubinsPath& path, ob::State *state) const {
  if (firstTime)
  {
    if (t<=0.)
    {
      if (from != state)
        stateSpace_->copyState(state, from);
      return;
    }

    path = ComputeSingleDubinsPath(from, to);

    firstTime = false;
  }
  Interpolate(from, path, t, state);
}

void TaskSpaceDubinsMotionValidator::Interpolate(const ob::State *from, const SingleDubinsPath& path, double t, ob::State *state) const {
  ob::DubinsStateSpace::StateType *s  = stateSpace_->allocState()->as<ob::DubinsStateSpace::StateType>();
  double seg = t * path.length(), phi, v;

  s->setXY(0., 0.);
  s->setYaw(from->as<ob::DubinsStateSpace::StateType>()->getYaw());

  for (unsigned int i=0; i<2 && seg>0; ++i)
  {
    v = std::min(seg, path.length_[i]);
    phi = s->getYaw();
    seg -= v;
    switch(path.type_[i])
    {
      case DUBINS_LEFT:
        s->setXY(s->getX() + sin(phi+v) - sin(phi), s->getY() - cos(phi+v) + cos(phi));
        s->setYaw(phi+v);
        break;
      case DUBINS_RIGHT:
        s->setXY(s->getX() - sin(phi-v) + sin(phi), s->getY() + cos(phi-v) - cos(phi));
        s->setYaw(phi-v);
        break;
      case DUBINS_STRAIGHT:
        s->setXY(s->getX() + v * cos(phi), s->getY() + v * sin(phi));
        break;
    }
  }

  state->as<ob::DubinsStateSpace::StateType>()->setX(s->getX() * stateSpace_->rho() + from->as<ob::DubinsStateSpace::StateType>()->getX());
  state->as<ob::DubinsStateSpace::StateType>()->setY(s->getY() * stateSpace_->rho() + from->as<ob::DubinsStateSpace::StateType>()->getY());
  stateSpace_->getSubspace(1)->enforceBounds(s->as<ob::SO2StateSpace::StateType>(1));
  state->as<ob::DubinsStateSpace::StateType>()->setYaw(s->getYaw());
  stateSpace_->freeState(s);
}

}  // namespace planning_common
}  // namespace ca


