/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * waypoint_smoothness_objective.cpp
 *
 *  Created on: Jun 6, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/objectives/waypoint_smoothness_objective.h"
#include "planning_common/paths/path_waypoint.h"
#include "planning_common/utils/workspace_utils.h"
#include <ros/ros.h>

namespace wu = ca::planning_common::workspace_utils;

namespace ca {
namespace planning_common{

ompl::base::Cost WaypointSmoothnessObjective::pathCost(const ompl::base::Path *path) const{
  // We are going to downcast since we are putting the whole logic here
  // Cant find a smart way to decouple
  BOOST_ASSERT_MSG(is_setup_, "WaypointSmoothnessObjective not set up");
  const PathWaypoint *path_wp = dynamic_cast<const PathWaypoint*>(path);
  if(path_wp) {
    Eigen::MatrixXd xi = path_wp->GetParameter();
    BOOST_ASSERT_MSG(IsMatrixConsistent(xi), "Path Waypoint dimensions dont match with A, b, c");
    Eigen::MatrixXd smoothness = (0.5 * xi.transpose() * A_ * xi + xi.transpose() * b_ + c_);
    if (threshold_residual_)
      smoothness *= GetThreshold(xi);
    return ompl::base::Cost( (xi.rows()+1)*smoothness.trace()); // Frobenius norm
  } else {
    ROS_ERROR_STREAM("Tried to cast to a PathWaypoint but failed. WaypointSmoothnessObjective can only work on PathWaypoint.");
    return ompl::base::Cost(0);
  }
}

Eigen::MatrixXd WaypointSmoothnessObjective::Gradient(const PathParametric *path) {
  // We are going to downcast since we are putting the whole logic here
  // Cant find a smart way to decouple
  BOOST_ASSERT_MSG(is_setup_, "WaypointSmoothnessObjective not set up");
  const PathWaypoint *path_wp = dynamic_cast<const PathWaypoint*>(path);
  if(path_wp) {
    Eigen::MatrixXd xi = path_wp->GetParameter();
    BOOST_ASSERT_MSG(IsMatrixConsistent(xi), "Path Waypoint dimensions dont match with A, b, c");
    Eigen::MatrixXd gradient = (xi.rows()+1)*(A_ * xi + b_);
    if (threshold_residual_)
      gradient *= GetThreshold(xi);
    return gradient;
  } else {
    ROS_ERROR_STREAM("Tried to cast to a PathWaypoint but failed. WaypointSmoothnessObjective can only work on PathWaypoint.");
    return Eigen::MatrixXd::Zero(0, 0);
  }
}

bool WaypointSmoothnessObjective::Setup (const PathWaypoint &path, const ompl::base::ScopedState<> &start, const ompl::base::ScopedState<> &end, const std::vector<double> &alpha) {
  if (alpha.size() == 0) {
    ROS_ERROR_STREAM("No alpha set up");
    return false;
  }

  /*
  namespace wu = workspace_utils;
  if(wu::HasSO3Rotation(si_)) {
    ROS_ERROR_STREAM("Waypoint smoothness doesnt work if space has SO3 because of issues with quaternion and state dimension func. Under developement");
    return false;
  }
  */

  Eigen::MatrixXd deriv_operator = DerivativeMatrix(path.NumberOfWaypoints());
  Eigen::MatrixXd deriv_matrix = Eigen::MatrixXd::Zero(deriv_operator.rows() + 2, deriv_operator.cols());

  deriv_matrix << Eigen::VectorXd::Zero(deriv_operator.cols()).transpose(),
      deriv_operator,
      Eigen::VectorXd::Zero(deriv_operator.cols()).transpose();
  deriv_matrix(0, 0) = 1;
  deriv_matrix(deriv_matrix.rows() - 1, deriv_matrix.cols() - 1) = -1;

  /*
  Eigen::VectorXd start_vec(start.getSpace()->getDimension()), end_vec(end.getSpace()->getDimension());
  for (size_t i = 0; i < start.getSpace()->getDimension(); i++) {
    start_vec(i) = start[i];
    end_vec(i) = end[i];
  }
*/
  Eigen::VectorXd start_vec = wu::GetTranslationVectorXd(si_, start.get());
  Eigen::VectorXd end_vec = wu::GetTranslationVectorXd(si_, end.get());


  Eigen::MatrixXd end_constraint_matrix(deriv_operator.rows() + 2, start_vec.rows());
  end_constraint_matrix << -start_vec.transpose(),
      Eigen::MatrixXd::Zero(deriv_operator.rows(), start_vec.rows()),
      end_vec.transpose();

  A_ = Eigen::MatrixXd::Zero(deriv_operator.cols(), deriv_operator.cols());
  b_ = Eigen::MatrixXd::Zero(deriv_operator.cols(), end_constraint_matrix.cols());
  c_ = Eigen::MatrixXd::Zero(end_constraint_matrix.cols(), end_constraint_matrix.cols());

  Eigen::MatrixXd padded_deriv_operator = Eigen::MatrixXd::Zero(deriv_matrix.rows(), deriv_matrix.rows());
  padded_deriv_operator << DerivativeMatrix(deriv_matrix.rows()),
      Eigen::VectorXd::Zero(deriv_matrix.rows()).transpose();

  for (size_t i = 0; i < alpha.size(); i++) {
    if (i > 0) {
      deriv_matrix = padded_deriv_operator * deriv_matrix;
      end_constraint_matrix = padded_deriv_operator * end_constraint_matrix;
    }
    A_ += alpha[i] * deriv_matrix.transpose() * deriv_matrix;
    b_ += alpha[i] * deriv_matrix.transpose() * end_constraint_matrix;
    c_ += alpha[i] * 0.5 * end_constraint_matrix.transpose() * end_constraint_matrix;


  }

  is_setup_ = true;
  return true;
}


Eigen::MatrixXd WaypointSmoothnessObjective::DerivativeMatrix(unsigned int dimension) const{
  // DONT CONSIDER THE END CONSTRAINT HERE!

  Eigen::MatrixXd deriv = Eigen::MatrixXd::Zero(dimension - 1, dimension);

  for (size_t i = 0; i < dimension - 1; i++) {
    deriv(i,i) = -1;
    deriv(i,i+1) = 1;
  }
  return deriv;
}

Eigen::MatrixXd WaypointSmoothnessObjective::GetThreshold (const Eigen::MatrixXd &xi) const  {
  Eigen::MatrixXd residual = (A_ * xi + b_);
  Eigen::VectorXd norm_res = residual.rowwise().norm();
  Eigen::VectorXd result = (norm_res.array() < threshold_).select(Eigen::VectorXd::Zero(norm_res.size()), Eigen::VectorXd::Ones(norm_res.size()));
  return result.asDiagonal();
}

}  // namespace planning_common
}  // namespace ca



