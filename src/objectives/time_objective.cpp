/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * time_objective.cpp
 *
 *  Created on: Jun 23, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/objectives/time_objective.h"
#include <ros/ros.h>
#include "planning_common/paths/path_waypoint.h"
#include "ompl/base/ScopedState.h"
#include "ompl/geometric/PathGeometric.h"
#include "planning_common/utils/workspace_utils.h"

namespace ob = ompl::base;
namespace og = ompl::geometric;
namespace wu = ca::planning_common::workspace_utils;
namespace ca {
namespace planning_common {

ob::Cost TimeObjective::pathCost(const ob::Path *path) const {
  const ob::State *start, *end;
  if (const PathWaypoint *path_wp = dynamic_cast<const PathWaypoint*>(path)) {
    if (path_wp->NumberOfWaypoints() > 1) {
      start = path_wp->GetWaypoint(0);
      end   = path_wp->GetWaypoint( static_cast <unsigned int> (path_wp->NumberOfWaypoints() - 1));
    } else {
      return ob::Cost(0);
    }
  } else if (const og::PathGeometric *path_geom = dynamic_cast<const og::PathGeometric*>(path)) {
    if (path_geom->getStateCount() > 1) {
      start = path_geom->getState(0);
      end   = path_geom->getState(static_cast <unsigned int> (path_geom->getStateCount() - 1));
    } else {
      return ob::Cost(0);
    }
  } else {
    OMPL_ERROR("Time Objective only applies to PathWaypoint or PathGeometric");
    return ob::Cost(ob::Cost::Infinity);
  }

  double start_time = wu::GetTime(si_, start);
  double end_time = wu::GetTime(si_, end);

  return ob::Cost(std::max(0.0, end_time - start_time));
}

ob::Cost TimeObjective::motionCost(const ob::State *start, const ob::State *end) const {
  ob::StateSpacePtr space(new ob::TimeStateSpace());
  ob::ScopedState<ob::TimeStateSpace> start_time(space), end_time(space);
  start_time << ob::ScopedState<>(si_->getStateSpace(), start);
  end_time << ob::ScopedState<>(si_->getStateSpace(), end);

  return ob::Cost(end_time->position - start_time->position);
}

}  // namespace planning_common
}  // namespace ca



