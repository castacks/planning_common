/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * differentiable_objective.cpp
 *
 *  Created on: Jun 14, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/objectives/differentiable_objective.h"
#include <ros/ros.h>
namespace ca {
namespace planning_common {

namespace ob = ompl::base;
bool MultiDifferentiableObjective::Parse(const ob::OptimizationObjectivePtr &optimization_objective) {
  objectives_.clear();
  if (boost::shared_ptr<ob::MultiOptimizationObjective> multi_obj = boost::dynamic_pointer_cast<ob::MultiOptimizationObjective>(optimization_objective)) {
    for (size_t i = 0; i < multi_obj->getObjectiveCount(); i++) {
      if (DifferentiableObjective::Ptr diff_obj = boost::dynamic_pointer_cast<DifferentiableObjective>(multi_obj->getObjective(i)))
        objectives_.push_back(DiffComponent( multi_obj->getObjectiveWeight(i), diff_obj ));
    }
    return true;
  } else if (DifferentiableObjective::Ptr diff_obj = boost::dynamic_pointer_cast<DifferentiableObjective>(optimization_objective)) {
    objectives_.push_back(DiffComponent( 1, diff_obj ));
    return true;
  } else {
    ROS_ERROR_STREAM("MultiDifferentiableObjective couldnt parse");
    return false;
  }
}

ob::Cost MultiDifferentiableObjective::pathCost(const ob::Path *path) {
  ob::Cost c = this->identityCost();
  for (std::vector<DiffComponent>::const_iterator comp = objectives_.begin();
       comp != objectives_.end();
       ++comp) {
      c.v += comp->first*(comp->second->pathCost(path).v);
  }
  return c;
}

Eigen::MatrixXd MultiDifferentiableObjective::Gradient(const PathParametric *path) {
  Eigen::MatrixXd gradient;
  for (std::vector<DiffComponent>::const_iterator comp = objectives_.begin();
       comp != objectives_.end();
       ++comp) {
    if (gradient.rows() == 0 && gradient.cols() == 0)
      gradient = comp->first * comp ->second->Gradient(path);
    else
      gradient += comp->first * comp ->second->Gradient(path);
  }
  return gradient;
}

}  // namespace planning_common
}  // namespace ca



