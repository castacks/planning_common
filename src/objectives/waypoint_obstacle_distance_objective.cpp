/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * waypoint_obstacle_distance_objective.cpp
 *
 *  Created on: Jun 8, 2014
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include <Eigen/Dense>
#include "planning_common/objectives/waypoint_obstacle_distance_objective.h"
#include "ompl/base/ScopedState.h"
#include "planning_common/utils/workspace_utils.h"

namespace wu = ca::planning_common::workspace_utils;
namespace caob = ca::ompl_base;

namespace ca {
namespace planning_common {

ompl::base::Cost WaypointObstacleDistanceObjective::pathCost(const ompl::base::Path *path) const{
  // We are going to downcast since we are putting the whole logic here
  // Cant find a smart way to decouple
  BOOST_ASSERT_MSG(is_setup_, "WaypointSmoothnessObjective not set up");
  const PathWaypoint *path_wp = dynamic_cast<const PathWaypoint*>(path);

  if (path_wp) {
    Eigen::MatrixXd workspace_path = GetWorkSpacePath(path_wp);
    Eigen::MatrixXd deriv_workspace = DerivativePath(workspace_path);
    double cost = 0;
    for (size_t i = 0; i < deriv_workspace.rows(); i++) {
      Eigen::Vector3d query = workspace_path.row(i).transpose();
      if (signed_distance_field_->IsValid(query)) {
        double val;
        if(!signed_distance_field_->GetValue(query, val))
          continue;
        cost += SquaredCost(val) * deriv_workspace.row(i).norm();
      }
    }
    cost /= deriv_workspace.rows();
    return ompl::base::Cost(cost);
  } else {
    ROS_ERROR_STREAM("Tried to cast to a PathWaypoint but failed. WaypointSmoothnessObjective can only work on PathWaypoint.");
    return ompl::base::Cost(0);
  }
}

Eigen::MatrixXd WaypointObstacleDistanceObjective::Gradient(const PathParametric *path) {
  // We are going to downcast since we are putting the whole logic here
  // Cant find a smart way to decouple
  BOOST_ASSERT_MSG(is_setup_, "WaypointSmoothnessObjective not set up");
  const PathWaypoint *path_wp = dynamic_cast<const PathWaypoint*>(path);
  if(path_wp) {
    Eigen::MatrixXd workspace_path = GetWorkSpacePath(path_wp);
    //std::cout<<"\nworkspace_path:\n"<<workspace_path<<"\n";
    Eigen::MatrixXd deriv_workspace = DerivativePath(workspace_path);
    //std::cout<<"\nderiv_workspace:\n"<<deriv_workspace<<"\n";
    Eigen::VectorXd deriv_workspace_mag = PathNorm(deriv_workspace);
    //std::cout<<"\nderiv_workspace_mag:\n"<<deriv_workspace_mag<<"\n";
    Eigen::MatrixXd deriv_workspace_normalized = NormalizePath(deriv_workspace);
    //std::cout<<"\nderiv_workspace_normalized:\n"<<deriv_workspace_normalized<<"\n";
    Eigen::MatrixXd double_deriv_workspace = DerivativePath(deriv_workspace);
    //std::cout<<"\ndouble_deriv_workspace:\n"<<double_deriv_workspace<<"\n";

    Eigen::MatrixXd configspace_gradient = Eigen::MatrixXd::Zero(workspace_path.rows(), wu::GetTranslationDimension(si_));
    for (size_t i = 0; i < double_deriv_workspace.rows(); i++) {
      Eigen::Vector3d query = workspace_path.row(i).transpose();
      if (!signed_distance_field_->IsValid(query))
        continue;
      double val;
      Eigen::Vector3d grad;
      if (!signed_distance_field_->GetValue(query, val))
        continue;
      signed_distance_field_->GetGradient(query, grad);

      Eigen::VectorXd cost_gradient = SquaredCostGradient(val, grad);
      Eigen::MatrixXd hausholder_proj = Eigen::MatrixXd::Identity(deriv_workspace.cols(), deriv_workspace.cols()) -
                                        deriv_workspace_normalized.row(i).transpose()*deriv_workspace_normalized.row(i);

      double cost = SquaredCost(val) * deriv_workspace.row(i).norm();

      Eigen::VectorXd workspace_gradient = hausholder_proj * ( cost_gradient * deriv_workspace_mag(i) - 0 *cost * double_deriv_workspace.row(i).transpose() / std::max(deriv_workspace_mag(i), std::numeric_limits<double>::epsilon()) );
      workspace_gradient[2] = 0;
      configspace_gradient.row(i) = (1.0/deriv_workspace.rows())*(workspace_gradient ).transpose();

/*
      ROS_INFO_STREAM("\ncomputation: "<<i
          <<"\ncoord"<<workspace_path.row(i).transpose()
          <<"\ncost\n"<<cost
          <<"\ndist\n"<<signed_distance_field_->GetValue(workspace_path.row(i).transpose())
          <<"\ndist_gradient\n"<<signed_distance_field_->GetGradient(workspace_path.row(i).transpose())
          <<"\ncost_gradient\n"<<cost_gradient
          <<"\nhausholder_proj\n"<<hausholder_proj
          <<"\nderiv workspace\n"<<deriv_workspace.row(i)
          <<"\nderiv workspace mag\n"<<deriv_workspace_mag.row(i)
          <<"\ndouble deriv \n"<<double_deriv_workspace.row(i)
          <<"\nworkspace_gradient\n"<<workspace_gradient
          <<"\nconfigspace_gradient\n"<<configspace_gradient.row(i));*/
    }

    //ROS_ERROR_STREAM("\nObstGrad: \n" << configspace_gradient);
    return configspace_gradient;
  } else {
    ROS_ERROR_STREAM("Tried to cast to a PathWaypoint but failed. WaypointSmoothnessObjective can only work on PathWaypoint.");
    return Eigen::MatrixXd::Zero(0,0);
  }
}


bool  WaypointObstacleDistanceObjective::Setup(const boost::shared_ptr<representation_interface::ObstacleGridRepresentationInterface> &signed_distance_field, double max_distance) {
  namespace wu = workspace_utils;
  if(wu::HasSO3Rotation(si_)) {
    ROS_ERROR_STREAM("Waypoint obstacle doesnt work if space has SO3 because of issues with quaternion and state dimension func. Under developement");
    return false;
  }
  signed_distance_field_ = signed_distance_field;
  max_distance_ = max_distance;
  is_setup_ = true;
  return true;
}

double WaypointObstacleDistanceObjective::SquaredCost(double signed_distance) const{
  double cost = 0;
  if (signed_distance < 0)
    cost = - signed_distance  + 0.5*max_distance_;
  else if (signed_distance < max_distance_)
    cost = (1/(2*max_distance_))*(signed_distance - max_distance_)*(signed_distance - max_distance_);
  else
    cost = 0;
  return cost;
}

Eigen::VectorXd WaypointObstacleDistanceObjective::SquaredCostGradient(double signed_distance, Eigen::VectorXd distance_gradient) const {
  Eigen::VectorXd gradient;
  if (signed_distance < 0)
    gradient = -1*distance_gradient;
  else if (signed_distance < max_distance_)
    gradient = (1/max_distance_)*(signed_distance - max_distance_)*distance_gradient;
  else
    gradient = Eigen::VectorXd::Zero(distance_gradient.rows());
  if (gradient.rows() == 3)
    gradient.cwiseProduct(premultiply_workspace_);
  return gradient;
}

Eigen::MatrixXd  WaypointObstacleDistanceObjective::GetWorkSpacePath(const PathWaypoint* path_wp) const {
  Eigen::MatrixXd workspace_path(path_wp->NumberOfWaypoints(), wu::GetTranslationDimension(si_));
  for (std::size_t i = 0 ; i < path_wp->NumberOfWaypoints() ; i++)
    workspace_path.row(i) = wu::GetTranslationVectorXd(si_, path_wp->GetWaypoint(i));

  /*
  Eigen::MatrixXd workspace_path_3d(path_wp->NumberOfWaypoints(), 3);
  if (workspace_path.rows() > 0 && workspace_path.cols() == 2)
    workspace_path_3d << workspace_path, Eigen::MatrixXd::Zero(path_wp->NumberOfWaypoints(), 1);
  else
    workspace_path_3d = workspace_path;
  */
  return workspace_path;
}

Eigen::MatrixXd WaypointObstacleDistanceObjective::DerivativePath(const Eigen::MatrixXd &path) const {
  // (x(k+1) - x(k))/delta_t
  // delta_t = 1 / num_waypoints;
  Eigen::MatrixXd output = Eigen::MatrixXd::Zero(path.rows()-1, path.cols());
  for (size_t i = 0; i < path.rows() - 1; i++)
    output.row(i) = path.row(i+1) - path.row(i);
  output *= path.rows();
  return output;
}

Eigen::MatrixXd WaypointObstacleDistanceObjective::NormalizePath(const Eigen::MatrixXd &path) const {
  // (x(k+1) - x(k))/delta_t
  // delta_t = 1 / num_waypoints;
  Eigen::MatrixXd output(path);
  for (size_t i = 0; i < path.rows(); i++) {
    if (path.row(i).norm() == 0)
      output.row(i) = Eigen::RowVectorXd::Zero(path.cols());
    else
      output.row(i).normalize();
  }

  return output;
}

Eigen::VectorXd WaypointObstacleDistanceObjective::PathNorm(const Eigen::MatrixXd &path) const {
  Eigen::VectorXd output(path.rows());
  for (size_t i = 0; i < path.rows(); i++)
    output[i] = path.row(i).norm();
  return output;
}

}  // namepsace planning_common
}  // namespace ca

