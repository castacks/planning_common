/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * planner_progress.cpp
 *
 *  Created on: Jan 7, 2015
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/planner_data/planner_progress.h"
#include "ros/ros.h"
#include <fstream>
namespace ob = ompl::base;

namespace ca {

void PlannerProgress::StartCollectingData(const ob::PlannerPtr &planner) {
  if (planner->getPlannerProgressProperties().size() > 0) {
    planner_progress_fields_.clear();
    for (auto it : planner->getPlannerProgressProperties())
      planner_progress_fields_.push_back(it.first);
    run_thread_ = true;
    thread_.reset(new boost::thread(boost::bind(&PlannerProgress::CollectProgressProperties, this,
                                                planner->getPlannerProgressProperties())));
  }
}

void PlannerProgress::StopCollectingData() {
  run_thread_ = false;
  if (thread_)
    thread_->join();
}

void PlannerProgress::PrintData(std::ostream &out) const {
  out << "time REAL\t";
  for (auto it : planner_progress_fields_)
    out << it<<"\t";
  out << "\n";

  for (auto snapshot : planner_progress_data_) {
    std::cout<<snapshot["time REAL"]<<"\t";
    for (auto it : planner_progress_fields_)
      out << snapshot[it] << "\t";
    out << "\n";
  }
}

void PlannerProgress::SaveData(std::string filename) const {
  std::ofstream  ofs(filename.c_str(), std::ofstream::out);
  ofs << "%time REAL,";
  for (auto it : planner_progress_fields_)
    ofs << it<<",";
  ofs << "\n";

  for (auto snapshot : planner_progress_data_) {
    ofs<<snapshot["time REAL"]<<",";
    for (auto it : planner_progress_fields_)
      ofs << snapshot[it] << ",";
    ofs << "\n";
  }

  ofs.close();
}

void PlannerProgress::CollectProgressProperties(const ob::Planner::PlannerProgressProperties& properties) {
  ros::Rate loop_rate(rate_);
  double time_latch = ros::Time::now().toSec();
  while(run_thread_) {
    std::string timeStamp = boost::lexical_cast<std::string>(ros::Time::now().toSec() - time_latch);
    std::map<std::string, std::string> data;
    data["time REAL"] = timeStamp;
    for (auto it : planner_progress_fields_) {
      try {
        data[it] = properties.at(it)();
      } catch (const std::out_of_range& e) {
        data[it] = "0";
        OMPL_WARN("Progress field mismatch");
      }
    }
    planner_progress_data_.push_back(data);
    loop_rate.sleep();
  }
}


}


