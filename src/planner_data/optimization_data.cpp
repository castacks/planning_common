/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * optimization_data.cpp
 *
 *  Created on: Jun 15, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/planner_data/optimization_data.h"

namespace ca {
namespace planning_common {

void OptimizationData::AddIterationInfo(unsigned int iteration_id, const ompl::base::Cost &cost, const PathParametric::Ptr &path, const PathParametric::Gradient &gradient, double step_size, bool restart) {
  num_iterations_ = std::max(num_iterations_, iteration_id+1);
  optimization_state_[iteration_id].cost = cost;
  optimization_state_[iteration_id].path = path;
  optimization_state_[iteration_id].gradient = gradient;
  optimization_state_[iteration_id].step_size = step_size;
  optimization_state_[iteration_id].restart = restart;
}

OptimizationData::OptimizationState OptimizationData::GetIterationInfo (unsigned int iteration_id) {
  return optimization_state_[iteration_id];
}

}  // namespace planning_common
}  // namespace ca


