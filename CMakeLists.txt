cmake_minimum_required(VERSION 2.8.3)
project(planning_common)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  cmake_modules
  roscpp
  tf_utils
  math_utils
  ompl
  ca_nav_msgs
  shapes
  representation_interface
)

find_package(Eigen REQUIRED)
include_directories(${Eigen_INCLUDE_DIRS})

#find_package(Qt4 REQUIRED)
#set(QT_USE_QTNETWORK TRUE)
#set(QT_DONT_USE_QTGUI TRUE)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
#include(${QT_USE_FILE})
#add_definitions(${QT_DEFINITIONS})
#include_directories(${CMAKE_BINARY_DIR})
#include_directories(${QT_INCLUDE_DIR})  

## System dependencies are found with CMake's conventions
# find_package(Boost REQUIRED COMPONENTS system)

## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

## To declare and build messages, services or actions from within this
## package, follow these steps:
## * Let MSG_DEP_SET be the set of packages whose message types you use in
##   your messages/services/actions (e.g. std_msgs, actionlib_msgs, ...).
## * In the file package.xml:
##   * add a build_depend and a run_depend tag for each package in MSG_DEP_SET
##   * If MSG_DEP_SET isn't empty the following dependencies might have been
##     pulled in transitively but can be declared for certainty nonetheless:
##     * add a build_depend tag for "message_generation"
##     * add a run_depend tag for "message_runtime"
## * In this file (CMakeLists.txt):
##   * add "message_generation" and every package in MSG_DEP_SET to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * add "message_runtime" and every package in MSG_DEP_SET to
##     catkin_package(CATKIN_DEPENDS ...)
##   * uncomment the add_*_files sections below as needed
##     and list every .msg/.srv/.action file to be processed
##   * uncomment the generate_messages entry below
##   * add every package in MSG_DEP_SET to generate_messages(DEPENDENCIES ...)

## Generate messages in the 'msg' folder
#add_message_files(
#   FILES
#   ProfilerEntry.msg
#)

## Generate services in the 'srv' folder
# add_service_files(
#   FILES
#   Service1.srv
#   Service2.srv
# )

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
#generate_messages(
#   DEPENDENCIES
#   std_msgs  # Or other packages containing msgs
#)

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if you package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
  INCLUDE_DIRS include
  LIBRARIES ${PROJECT_NAME}
  CATKIN_DEPENDS roscpp tf_utils ompl ca_nav_msgs math_utils shapes representation_interface
  #DEPENDS system_lib
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
  ${catkin_INCLUDE_DIRS}
  include
)

file(GLOB_RECURSE planning_SOURCES src/*.cpp src/*.c)
add_library(${PROJECT_NAME} ${planning_SOURCES})
add_dependencies(${PROJECT_NAME} ca_nav_msgs_generate_messages_cpp)
target_link_libraries(${PROJECT_NAME}
  ${catkin_LIBRARIES}
)


add_executable(${PROJECT_NAME}_example_simple_grid examples/example_simple_grid.cpp)
target_link_libraries(${PROJECT_NAME}_example_simple_grid
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
)

add_executable(${PROJECT_NAME}_example_path_waypoint examples/example_path_waypoint.cpp)
target_link_libraries(${PROJECT_NAME}_example_path_waypoint
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
)

add_executable(${PROJECT_NAME}_example_smoothness_objective examples/example_smoothness_objective.cpp)
target_link_libraries(${PROJECT_NAME}_example_smoothness_objective
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
)


#add_executable(${PROJECT_NAME}_example_waypoint_obstacle_objective examples/example_waypoint_obstacle_objective.cpp)
#target_link_libraries(${PROJECT_NAME}_example_waypoint_obstacle_objective
#  ${PROJECT_NAME}
#  ${catkin_LIBRARIES}
#)

add_executable(${PROJECT_NAME}_example_constraints_enforcement examples/example_constraints_enforcement.cpp)
target_link_libraries(${PROJECT_NAME}_example_constraints_enforcement
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
)

add_executable(${PROJECT_NAME}_example_workspace_checking examples/example_workspace_checking.cpp)
target_link_libraries(${PROJECT_NAME}_example_workspace_checking
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
)

add_executable(${PROJECT_NAME}_example_time_scaling examples/example_time_scaling.cpp)
target_link_libraries(${PROJECT_NAME}_example_time_scaling
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
)

add_executable(${PROJECT_NAME}_example_world_velocity examples/example_world_velocity.cpp)
target_link_libraries(${PROJECT_NAME}_example_world_velocity
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
)

add_executable(${PROJECT_NAME}_example_task_space_dubins examples/example_task_space_dubins.cpp)
target_link_libraries(${PROJECT_NAME}_example_task_space_dubins
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
)

add_executable(${PROJECT_NAME}_example_speed_warping examples/example_speed_warping.cpp)
target_link_libraries(${PROJECT_NAME}_example_speed_warping
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
)

add_executable(${PROJECT_NAME}_example_path_utils examples/example_path_utils.cpp)
target_link_libraries(${PROJECT_NAME}_example_path_utils
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
)


## Add cmake target dependencies of the executable/library
## as an example, message headers may need to be generated before nodes
# add_dependencies(ca_profiler_node ca_profiler_generate_messages_cpp)

## Specify libraries to link a library or executable target against
# target_link_libraries(ca_profiler_node
#   ${catkin_LIBRARIES}
# )

#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
# install(PROGRAMS
#   scripts/my_python_script
#   DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
# )

IF ("${ACS_INSTALL_TYPE}" STREQUAL "FULL")

# Mark executables and/or libraries for installation
 install(TARGETS ${PROJECT_NAME}
   ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
   LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
   RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
 )

# Mark cpp header files for installation
 install(DIRECTORY include/${PROJECT_NAME}/
   DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
   FILES_MATCHING PATTERN "*.h"
 )

## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

ENDIF()

#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
catkin_add_gtest(${PROJECT_NAME}-test_task_space test/task_space_test.cpp)
if(TARGET ${PROJECT_NAME}-test_task_space)
   target_link_libraries(${PROJECT_NAME}-test_task_space ${PROJECT_NAME} ${catkin_LIBRARIES})
endif()

catkin_add_gtest(${PROJECT_NAME}-test_state_utils test/state_utils_test.cpp)
if(TARGET ${PROJECT_NAME}-test_state_utils)
   target_link_libraries(${PROJECT_NAME}-test_state_utils ${PROJECT_NAME} ${catkin_LIBRARIES})
endif()

catkin_add_gtest(${PROJECT_NAME}-test_path_utils test/path_utils_test.cpp)
if(TARGET ${PROJECT_NAME}-test_path_utils)
   target_link_libraries(${PROJECT_NAME}-test_path_utils ${PROJECT_NAME} ${catkin_LIBRARIES})
endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)



