/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * path_utils_test.cpp
 *
 *  Created on: Sep 2, 2014
 *      Author: Sanjiban Choudhury
 */


#include <gtest/gtest.h>
#include <stdlib.h>
#include <boost/math/constants/constants.hpp>

#include "planning_common/utils/state_utils.h"
#include "planning_common/utils/path_utils.h"
#include "planning_common/states/se3_tangent_bundle.h"

TEST(StateAtIndex, StressTest) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace pu = ca::planning_common::path_utils;

  ob::StateSpacePtr space(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(-100000);
  bounds.setHigh(100000);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  pc::PathWaypoint path(si);
  for (int i = 0; i < 100; i++) {
    ob::ScopedState<pc::SE3TangentBundle> sample(space);
    sample->GetTime().position = i;
    sample->GetPose().setXYZ(i, i, i);
    path.AppendWaypoint(sample.get());
  }

  ob::ScopedState<pc::SE3TangentBundle> state(space);
  pu::StateAtIndex(path, 0, state.get());
  EXPECT_FLOAT_EQ(0.0, state->GetTime().position);
  pu::StateAtIndex(path, 99, state.get());
  EXPECT_FLOAT_EQ(99.0, state->GetTime().position);
  pu::StateAtIndex(path, 57.59, state.get());
  EXPECT_FLOAT_EQ(57.59, state->GetTime().position);

  path.Clear();
  for (int i = 0; i < 1; i++) {
    ob::ScopedState<pc::SE3TangentBundle> sample(space);
    sample->GetTime().position = i;
    sample->GetPose().setXYZ(i, i, i);
    path.AppendWaypoint(sample.get());
  }

  pu::StateAtIndex(path, 0, state.get());
  EXPECT_FLOAT_EQ(0.0, state->GetTime().position);
}

TEST(ProjectPosition, StressTest) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace pu = ca::planning_common::path_utils;

  ob::StateSpacePtr space(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(-100000);
  bounds.setHigh(100000);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  pc::PathWaypoint path(si);
  for (int i = 0; i < 100; i++) {
    ob::ScopedState<pc::SE3TangentBundle> sample(space);
    sample->GetTime().position = i;
    sample->GetPose().setXYZ(i, i, i);
    path.AppendWaypoint(sample.get());
  }

  EXPECT_FLOAT_EQ(0.0, pu::ProjectPosition(path, Eigen::Vector3d(-1, -1, -1), 0.0));
  EXPECT_FLOAT_EQ(1.0, pu::ProjectPosition(path, Eigen::Vector3d(-1, -1, -1), 1.0));
  EXPECT_FLOAT_EQ(99.0, pu::ProjectPosition(path, Eigen::Vector3d(-1, -1, -1), 99.0));

  EXPECT_FLOAT_EQ(99.0, pu::ProjectPosition(path, Eigen::Vector3d(100, 100, 100), 0.0));
  EXPECT_FLOAT_EQ(99.0, pu::ProjectPosition(path, Eigen::Vector3d(100, 100, 100), 19.0));
  EXPECT_FLOAT_EQ(99.0, pu::ProjectPosition(path, Eigen::Vector3d(100, 100, 100), 99.0));

  EXPECT_FLOAT_EQ(2.6, pu::ProjectPosition(path, Eigen::Vector3d(2.6, 2.6, 2.6), 0));
  EXPECT_FLOAT_EQ(2.6, pu::ProjectPosition(path, Eigen::Vector3d(2.6, 2.6, 2.6), 2.6));
  EXPECT_FLOAT_EQ(2.7, pu::ProjectPosition(path, Eigen::Vector3d(2.6, 2.6, 2.6), 2.7));
}

TEST(ProjectPosition, CornerCasesTest) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace pu = ca::planning_common::path_utils;

  ob::StateSpacePtr space(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(-100000);
  bounds.setHigh(100000);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  pc::PathWaypoint path(si);
  for (int i = 0; i < 10; i++) {
    ob::ScopedState<pc::SE3TangentBundle> sample(space);
    sample->GetTime().position = i;
    sample->GetPose().setXYZ(i, i, i);
    if (i == 5)
      path.AppendWaypoint(path.Back());
    else
      path.AppendWaypoint(sample.get());
  }

  EXPECT_FLOAT_EQ(7.5, pu::ProjectPosition(path, Eigen::Vector3d(7.5, 7.5, 7.5), 0.0));
}


TEST(SegmentFromIndex, StressTest) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace pu = ca::planning_common::path_utils;

  ob::StateSpacePtr space(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(-100000);
  bounds.setHigh(100000);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  pc::PathWaypoint path(si);
  for (int i = 0; i < 4; i++) {
    ob::ScopedState<pc::SE3TangentBundle> sample(space);
    sample->GetTime().position = i;
    sample->GetPose().setXYZ(i, i, i);
    path.AppendWaypoint(sample.get());
  }

  pc::PathWaypoint output(si);
  pu::SegmentFromIndex(path, 1.3, output);
  ASSERT_EQ(3, output.Size());
  EXPECT_FLOAT_EQ(1.3,   output.GetWaypoint(0)->as<pc::SE3TangentBundle::StateType>()->GetTime().position);

  output.Clear();
  pu::SegmentFromIndex(path, 1.7, output);
  ASSERT_EQ(2, output.Size());

  output.Clear();
  pu::SegmentFromIndex(path, 0, output);
  ASSERT_EQ(path.Size(), output.Size());

  output.Clear();
  pu::SegmentFromIndex(path, 3, output);
  ASSERT_EQ(1, output.Size());
}

TEST(SegmentToIndex, StressTest) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace pu = ca::planning_common::path_utils;

  ob::StateSpacePtr space(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(-100000);
  bounds.setHigh(100000);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  pc::PathWaypoint path(si);
  for (int i = 0; i < 4; i++) {
    ob::ScopedState<pc::SE3TangentBundle> sample(space);
    sample->GetTime().position = i;
    sample->GetPose().setXYZ(i, i, i);
    path.AppendWaypoint(sample.get());
  }

  pc::PathWaypoint output(si);
  pu::SegmentToIndex(path, 1.3, output);
  ASSERT_EQ(2, output.Size());
  EXPECT_FLOAT_EQ(0,   output.GetWaypoint(0)->as<pc::SE3TangentBundle::StateType>()->GetTime().position);
  EXPECT_FLOAT_EQ(1.3,   output.GetWaypoint(1)->as<pc::SE3TangentBundle::StateType>()->GetTime().position);

  output.Clear();
  pu::SegmentToIndex(path, 1.7, output);
  ASSERT_EQ(3, output.Size());
  EXPECT_FLOAT_EQ(0,   output.GetWaypoint(0)->as<pc::SE3TangentBundle::StateType>()->GetTime().position);
  EXPECT_FLOAT_EQ(1,   output.GetWaypoint(1)->as<pc::SE3TangentBundle::StateType>()->GetTime().position);
  EXPECT_FLOAT_EQ(1.7,   output.GetWaypoint(2)->as<pc::SE3TangentBundle::StateType>()->GetTime().position);

  output.Clear();
  pu::SegmentToIndex(path, 3, output);
  ASSERT_EQ(path.Size(), output.Size());

  output.Clear();
  pu::SegmentToIndex(path, 0, output);
  ASSERT_EQ(1, output.Size());
}

TEST(SegmentFromToIndex, StressTest) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace pu = ca::planning_common::path_utils;

  ob::StateSpacePtr space(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(-100000);
  bounds.setHigh(100000);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  pc::PathWaypoint path(si);
  for (int i = 0; i < 4; i++) {
    ob::ScopedState<pc::SE3TangentBundle> sample(space);
    sample->GetTime().position = i;
    sample->GetPose().setXYZ(i, i, i);
    path.AppendWaypoint(sample.get());
  }

  pc::PathWaypoint output(si);
  pu::SegmentFromToIndex(path, 1.3, 1.7, output);
  ASSERT_EQ(2, output.Size());
  EXPECT_FLOAT_EQ(1.3,   output.GetWaypoint(0)->as<pc::SE3TangentBundle::StateType>()->GetTime().position);
  EXPECT_FLOAT_EQ(1.7,   output.GetWaypoint(1)->as<pc::SE3TangentBundle::StateType>()->GetTime().position);

  output.Clear();
  pu::SegmentFromToIndex(path, 1.3, 1.3,output);
  ASSERT_EQ(1, output.Size());
  EXPECT_FLOAT_EQ(1.3,   output.GetWaypoint(0)->as<pc::SE3TangentBundle::StateType>()->GetTime().position);

  output.Clear();
  pu::SegmentFromToIndex(path, 1.1, 2.6, output);
  ASSERT_EQ(3, output.Size());
  EXPECT_FLOAT_EQ(1.1,   output.GetWaypoint(0)->as<pc::SE3TangentBundle::StateType>()->GetTime().position);
  EXPECT_FLOAT_EQ(2.0,   output.GetWaypoint(1)->as<pc::SE3TangentBundle::StateType>()->GetTime().position);
  EXPECT_FLOAT_EQ(2.6,   output.GetWaypoint(2)->as<pc::SE3TangentBundle::StateType>()->GetTime().position);

  output.Clear();
  pu::SegmentFromToIndex(path, 1.1, 2.2, output);
  ASSERT_EQ(2, output.Size());
  EXPECT_FLOAT_EQ(1.1,   output.GetWaypoint(0)->as<pc::SE3TangentBundle::StateType>()->GetTime().position);
  EXPECT_FLOAT_EQ(2.2,   output.GetWaypoint(1)->as<pc::SE3TangentBundle::StateType>()->GetTime().position);
}

TEST(SegmentToTime, MidpointSplit) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace pu = ca::planning_common::path_utils;

  ob::StateSpacePtr space(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(-100000);
  bounds.setHigh(100000);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  pc::PathWaypoint path(si);
  for (int i = 0; i < 100; i++) {
    ob::ScopedState<pc::SE3TangentBundle> sample(space);
    sample->GetTime().position = i;
    sample->GetPose().setXYZ(i, i, i);
    path.AppendWaypoint(sample.get());
  }

  pc::PathWaypoint output = pu::SegmentToTime(path, 30.3);
  ASSERT_EQ(32, output.Size());
  EXPECT_FLOAT_EQ(30.3,   output.GetWaypoint(31)->as<pc::SE3TangentBundle::StateType>()->GetPose().getX());
}

TEST(SegmentToTime, BeginSplit) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace pu = ca::planning_common::path_utils;

  ob::StateSpacePtr space(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(-100000);
  bounds.setHigh(100000);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  pc::PathWaypoint path(si);
  for (int i = 0; i < 100; i++) {
    ob::ScopedState<pc::SE3TangentBundle> sample(space);
    sample->GetTime().position = i;
    sample->GetPose().setXYZ(i, i, i);
    path.AppendWaypoint(sample.get());
  }

  pc::PathWaypoint output = pu::SegmentToTime(path, -1);
  ASSERT_EQ(0, output.Size());
}

TEST(SegmentToTime, EndSplit) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace pu = ca::planning_common::path_utils;

  ob::StateSpacePtr space(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(-100000);
  bounds.setHigh(100000);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  pc::PathWaypoint path(si);
  for (int i = 0; i < 100; i++) {
    ob::ScopedState<pc::SE3TangentBundle> sample(space);
    sample->GetTime().position = i;
    sample->GetPose().setXYZ(i, i, i);
    path.AppendWaypoint(sample.get());
  }

  pc::PathWaypoint output = pu::SegmentToTime(path, 120);
  ASSERT_EQ(100, output.Size());
}

TEST(SegmentToTime, EmptyTest) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace pu = ca::planning_common::path_utils;

  ob::StateSpacePtr space(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(-100000);
  bounds.setHigh(100000);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  pc::PathWaypoint path(si);

  pc::PathWaypoint output = pu::SegmentToTime(path, 120);
  ASSERT_EQ(0, output.Size());
}

TEST(StitchPathWaypointAtTime, CorrectStitch) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace pu = ca::planning_common::path_utils;

  ob::StateSpacePtr space(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(-100000);
  bounds.setHigh(100000);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  pc::PathWaypoint original(si), branch(si), merged(si);
  for (int i = 0; i < 100; i++) {
    ob::ScopedState<pc::SE3TangentBundle> sample(space);
    sample->GetTime().position = i;
    sample->GetPose().setXYZ(i, i, i);
    sample->GetPose().rotation().setIdentity();
    sample->GetTangentPose().SetXYZDot(0,0,0);
    sample->GetTangentPose().SetPhiThetaPsiDot(0,0,0);
    original.AppendWaypoint(sample.get());
  }

  for (int i = 0; i < 100; i++) {
    ob::ScopedState<pc::SE3TangentBundle> sample(space);
    sample->GetTime().position = 30.3 + i;
    sample->GetPose().setXYZ(30.3 - i, 30.3 - i, 30.3 -i);
    sample->GetPose().rotation().setIdentity();
    sample->GetTangentPose().SetXYZDot(0,0,0);
    sample->GetTangentPose().SetPhiThetaPsiDot(0,0,0);
    branch.AppendWaypoint(sample.get());
  }

  bool result = pu::StitchPathWaypointAtTime(original, branch, merged);
  ASSERT_EQ(true, result);
  ASSERT_EQ(131, merged.Size());
  EXPECT_FLOAT_EQ(0.0,   merged.GetWaypoint(0)->as<pc::SE3TangentBundle::StateType>()->GetPose().getX());
  EXPECT_FLOAT_EQ(30.3,   merged.GetWaypoint(31)->as<pc::SE3TangentBundle::StateType>()->GetPose().getX());
  EXPECT_FLOAT_EQ(-68.7,   merged.GetWaypoint(130)->as<pc::SE3TangentBundle::StateType>()->GetPose().getX());
}

TEST(RecomputeTime, simple1) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace pu = ca::planning_common::path_utils;

  ob::StateSpacePtr space(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(-100000);
  bounds.setHigh(100000);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  pc::PathWaypoint path(si);
  for (int i = 0; i < 100; i++) {
    ob::ScopedState<pc::SE3TangentBundle> sample(space);
    sample->GetPose().setXYZ(10*i, 0, 0);
    sample->GetTangentPose().SetXYZDot(10,0,0);
    path.AppendWaypoint(sample.get());
  }
  pu::RecomputeTime(100, path);
  ASSERT_FLOAT_EQ(199, path.Back()->as<pc::SE3TangentBundle::StateType>()->GetTime().position);
}

TEST(StateDistanceFromEndFromRef2D, straight1) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace pu = ca::planning_common::path_utils;

  ob::StateSpacePtr space(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(-100000);
  bounds.setHigh(100000);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  pc::PathWaypoint path(si);
  for (int i = 0; i < 101; i++) {
    ob::ScopedState<pc::SE3TangentBundle> sample(space);
    sample->GetTime().position = i;
    sample->GetPose().setXYZ(10*i, 0, 0);
    sample->GetTangentPose().SetXYZDot(10,0,0);
    path.AppendWaypoint(sample.get());
  }

  ob::ScopedState<pc::SE3TangentBundle> ref(space);
  ASSERT_TRUE(pu::StateDistanceFromEndFromRef2D(path, 45.0, Eigen::Vector3d(1000, 27, 0), ref.get()));
  EXPECT_FLOAT_EQ(96.4, ref->GetTime().position);
  EXPECT_FLOAT_EQ(964, ref->GetPose().getX());
}


int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}


