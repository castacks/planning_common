/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * task_space_test.cpp
 *
 *  Created on: Aug 24, 2014
 *      Author: Sanjiban Choudhury
 */

#include <gtest/gtest.h>
#include <stdlib.h>
#include <boost/math/constants/constants.hpp>

#include "planning_common/state_validity/bounds_state_validity_checker.h"
#include "ompl/base/ScopedState.h"

#include "planning_common/motion_validator/task_space_dubins_motion_validator.h"


TEST(TaskSpaceDubin, ReachEasyPointCorrectHeading) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;

  ob::StateSpacePtr space(new ob::DubinsStateSpace(2.0));
  ob::RealVectorBounds bounds(2);
  bounds.setLow(-100);
  bounds.setHigh(100);
  space->as<ob::DubinsStateSpace>()->setBounds(bounds);

  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setStateValidityChecker(ob::StateValidityCheckerPtr(new pc::BoundsStateValidityChecker(si)));
  si->setStateValidityCheckingResolution(0.001);
  si->setup();

  pc::TaskSpaceDubinsMotionValidator ts_dubins(si);

  ob::ScopedState<ob::SE2StateSpace> start(space), goal(space), goal_full(space);

  start->setXY(0, 0);
  start->setYaw(0);

  goal->setXY(0, 2.0 + 2*sqrt(2));

  ts_dubins.CheckMotion(start.get(), goal.get(), goal_full.get());
  // Check if a valid motion exists and get the new end point
  EXPECT_FLOAT_EQ(0.75*boost::math::constants::pi<double>(), goal_full->getYaw());
}

TEST(TaskSpaceDubin, ReachEasyPointCorrectDistance) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;

  ob::StateSpacePtr space(new ob::DubinsStateSpace(2.0));
  ob::RealVectorBounds bounds(2);
  bounds.setLow(-100);
  bounds.setHigh(100);
  space->as<ob::DubinsStateSpace>()->setBounds(bounds);

  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setStateValidityChecker(ob::StateValidityCheckerPtr(new pc::BoundsStateValidityChecker(si)));
  si->setStateValidityCheckingResolution(0.001);
  si->setup();

  pc::TaskSpaceDubinsMotionValidator ts_dubins(si);

  ob::ScopedState<ob::SE2StateSpace> start(space), goal(space);

  start->setXY(0, 0);
  start->setYaw(0);

  goal->setXY(0, 2.0 + 2*sqrt(2));


  // Check if a valid motion exists and get the new end point
  EXPECT_FLOAT_EQ(2 + 2*0.75*boost::math::constants::pi<double>(), ts_dubins.Distance(start.get(), goal.get()));
}

TEST(TaskSpaceDubin, ReachHardPointHeading) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;

  ob::StateSpacePtr space(new ob::DubinsStateSpace(1.0));
  ob::RealVectorBounds bounds(2);
  bounds.setLow(-100);
  bounds.setHigh(100);
  space->as<ob::DubinsStateSpace>()->setBounds(bounds);

  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setStateValidityChecker(ob::StateValidityCheckerPtr(new pc::BoundsStateValidityChecker(si)));
  si->setStateValidityCheckingResolution(0.001);
  si->setup();

  pc::TaskSpaceDubinsMotionValidator ts_dubins(si);

  ob::ScopedState<ob::SE2StateSpace> start(space), goal(space), goal_full(space);

  start->setXY(0, 0);
  start->setYaw(0);

  goal->setXY(0, -1+sqrt(2));

  ts_dubins.CheckMotion(start.get(), goal.get(), goal_full.get());
  // Check if a valid motion exists and get the new end point
  EXPECT_FLOAT_EQ(0.25*boost::math::constants::pi<double>(), goal_full->getYaw());
}

TEST(TaskSpaceDubin, ReachHardPointRadius) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;

  ob::StateSpacePtr space(new ob::DubinsStateSpace(1.0));
  ob::RealVectorBounds bounds(2);
  bounds.setLow(-100);
  bounds.setHigh(100);
  space->as<ob::DubinsStateSpace>()->setBounds(bounds);

  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setStateValidityChecker(ob::StateValidityCheckerPtr(new pc::BoundsStateValidityChecker(si)));
  si->setStateValidityCheckingResolution(0.001);
  si->setup();

  pc::TaskSpaceDubinsMotionValidator ts_dubins(si);

  ob::ScopedState<ob::SE2StateSpace> start(space), goal(space);

  start->setXY(0, 0);
  start->setYaw(0);

  goal->setXY(0, -1+sqrt(2));


  // Check if a valid motion exists and get the new end point
  EXPECT_FLOAT_EQ(1 + (2-0.25)*boost::math::constants::pi<double>(), ts_dubins.Distance(start.get(), goal.get()));
}

TEST(TaskSpaceDubin, TrivialZeroCase) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;

  ob::StateSpacePtr space(new ob::DubinsStateSpace(1.0));
  ob::RealVectorBounds bounds(2);
  bounds.setLow(-100);
  bounds.setHigh(100);
  space->as<ob::DubinsStateSpace>()->setBounds(bounds);

  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setStateValidityChecker(ob::StateValidityCheckerPtr(new pc::BoundsStateValidityChecker(si)));
  si->setStateValidityCheckingResolution(0.001);
  si->setup();

  pc::TaskSpaceDubinsMotionValidator ts_dubins(si);

  ob::ScopedState<ob::SE2StateSpace> start(space), goal(space);

  start->setXY(0, 0);
  start->setYaw(0);

  goal->setXY(0, 0);


  // Check if a valid motion exists and get the new end point
  EXPECT_FLOAT_EQ(0, ts_dubins.Distance(start.get(), goal.get()));
}

TEST(TaskSpaceDubin, RotationInvariance) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;

  ob::StateSpacePtr space(new ob::DubinsStateSpace(1.0));
  ob::RealVectorBounds bounds(2);
  bounds.setLow(-100);
  bounds.setHigh(100);
  space->as<ob::DubinsStateSpace>()->setBounds(bounds);

  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setStateValidityChecker(ob::StateValidityCheckerPtr(new pc::BoundsStateValidityChecker(si)));
  si->setStateValidityCheckingResolution(0.001);
  si->setup();

  pc::TaskSpaceDubinsMotionValidator ts_dubins(si);

  ob::ScopedState<ob::SE2StateSpace> start1(space), goal1(space), goal_full1(space), start2(space), goal2(space), goal_full2(space);

  start1->setXY(0, 0);
  start1->setYaw(0);
  goal1->setXY(0, 3);

  start2->setXY(0, 0);
  start2->setYaw(0.5*boost::math::constants::pi<double>());
  goal2->setXY(-3, 0);

  // Check if a valid motion exists and get the new end point
  ts_dubins.CheckMotion(start1.get(), goal1.get(), goal_full1.get());
  ts_dubins.CheckMotion(start2.get(), goal2.get(), goal_full2.get());

  double yaw_diff = goal_full2->getYaw() - goal_full1->getYaw();

  EXPECT_FLOAT_EQ(1, cos(0.5*boost::math::constants::pi<double>()-yaw_diff));
}

TEST(TaskSpaceDubin, DoubleDubinEquivalence) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;

  ob::StateSpacePtr space(new ob::DubinsStateSpace(3.0));
  ob::RealVectorBounds bounds(2);
  bounds.setLow(-100);
  bounds.setHigh(100);
  space->as<ob::DubinsStateSpace>()->setBounds(bounds);

  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setStateValidityChecker(ob::StateValidityCheckerPtr(new pc::BoundsStateValidityChecker(si)));
  si->setStateValidityCheckingResolution(0.001);
  si->setup();

  pc::TaskSpaceDubinsMotionValidator ts_dubins(si);

  ob::ScopedState<ob::SE2StateSpace> start(space), goal(space), goal_full(space);

  start->setXY(4, 5);
  start->setYaw(0.345);

  goal->setXY(-9, 15);

  ts_dubins.CheckMotion(start.get(), goal.get(), goal_full.get());
  // Check if a valid motion exists and get the new end point
  EXPECT_FLOAT_EQ(space->distance(start.get(), goal_full.get()), ts_dubins.Distance(start.get(), goal.get()));
}

int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}



