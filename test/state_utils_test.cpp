/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * state_utils_test.cpp
 *
 *  Created on: Aug 27, 2014
 *      Author: Sanjiban Choudhury
 */

#include <gtest/gtest.h>
#include <stdlib.h>
#include <boost/math/constants/constants.hpp>

#include "planning_common/utils/state_utils.h"
#include "planning_common/states/se3_tangent_bundle.h"
#include "ompl/base/spaces/DubinsStateSpace.h"

TEST(ConvertToSE3TangentBundle, InvarianceTest) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace su = ca::planning_common::state_utils;

  ob::StateSpacePtr space(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds(3);
  bounds.setLow(0);
  bounds.setHigh(1);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  ob::ScopedState<pc::SE3TangentBundle> sample(space);
  sample->GetTime().position = 3e8;
  sample->GetPose().setXYZ(1,2,3);
  sample->GetTangentPose().SetXYZDot(4,5,6);
  su::ConvertEulerToSO3 (0.5, 0, 0, &sample->GetPose().rotation());
  sample->GetTangentPose().SetPhiThetaPsiDot(0.1, 0.2, 0.3);

  ob::StateSpacePtr space2(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds2(3);
  bounds2.setLow(-100000);
  bounds2.setHigh(100000);
  space2->as<pc::SE3TangentBundle>()->SetBounds(bounds2, bounds2, bounds2);

  ob::ScopedState<pc::SE3TangentBundle> output(space2);
  su::ConvertAnyToSE3TangentBundle(sample.get(), si, output.get());

  EXPECT_FLOAT_EQ(sample->GetPose().getX(), output->GetPose().getX());
  EXPECT_FLOAT_EQ(sample->GetPose().getY(), output->GetPose().getY());
  EXPECT_FLOAT_EQ(sample->GetPose().getZ(), output->GetPose().getZ());

  EXPECT_FLOAT_EQ(sample->GetPose().rotation().x, output->GetPose().rotation().x);
  EXPECT_FLOAT_EQ(sample->GetPose().rotation().y, output->GetPose().rotation().y);
  EXPECT_FLOAT_EQ(sample->GetPose().rotation().z, output->GetPose().rotation().z);
  EXPECT_FLOAT_EQ(sample->GetPose().rotation().w, output->GetPose().rotation().w);

  EXPECT_FLOAT_EQ(sample->GetTangentPose().GetXDot(), output->GetTangentPose().GetXDot());
  EXPECT_FLOAT_EQ(sample->GetTangentPose().GetYDot(), output->GetTangentPose().GetYDot());
  EXPECT_FLOAT_EQ(sample->GetTangentPose().GetZDot(), output->GetTangentPose().GetZDot());

  EXPECT_FLOAT_EQ(sample->GetTangentPose().GetPhiDot(), output->GetTangentPose().GetPhiDot());
  EXPECT_FLOAT_EQ(sample->GetTangentPose().GetThetaDot(), output->GetTangentPose().GetThetaDot());
  EXPECT_FLOAT_EQ(sample->GetTangentPose().GetPsiDot(), output->GetTangentPose().GetPsiDot());

  EXPECT_FLOAT_EQ(sample->GetTime().position, output->GetTime().position);
}


TEST(ConvertToSE3TangentBundle, DubinTest) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace su = ca::planning_common::state_utils;

  ob::StateSpacePtr space(new ob::DubinsStateSpace(1.0));
  ob::RealVectorBounds bounds(2);
  bounds.setLow(0);
  bounds.setHigh(1);
  space->as<ob::DubinsStateSpace>()->setBounds(bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  ob::ScopedState<ob::DubinsStateSpace> sample(space);
  sample->setXY(1,2);
  sample->setYaw(1);

  ob::StateSpacePtr space2(new pc::SE3TangentBundle());
  ob::RealVectorBounds bounds2(3);
  bounds2.setLow(-100000);
  bounds2.setHigh(100000);
  space2->as<pc::SE3TangentBundle>()->SetBounds(bounds2, bounds2, bounds2);

  ob::ScopedState<pc::SE3TangentBundle> output(space2);
  su::ConvertAnyToSE3TangentBundle(sample.get(), si, output.get());

  EXPECT_FLOAT_EQ(1, output->GetPose().getX());
  EXPECT_FLOAT_EQ(2, output->GetPose().getY());
  EXPECT_FLOAT_EQ(0, output->GetPose().getZ());

  EXPECT_FLOAT_EQ(0, output->GetPose().rotation().x);
  EXPECT_FLOAT_EQ(0, output->GetPose().rotation().y);
  EXPECT_FLOAT_EQ(sin(1.0/2), output->GetPose().rotation().z);
  EXPECT_FLOAT_EQ(cos(1.0/2), output->GetPose().rotation().w);

  EXPECT_FLOAT_EQ(0, output->GetTangentPose().GetXDot());
  EXPECT_FLOAT_EQ(0, output->GetTangentPose().GetYDot());
  EXPECT_FLOAT_EQ(0, output->GetTangentPose().GetZDot());

  EXPECT_FLOAT_EQ(0, output->GetTangentPose().GetPhiDot());
  EXPECT_FLOAT_EQ(0, output->GetTangentPose().GetThetaDot());
  EXPECT_FLOAT_EQ(0, output->GetTangentPose().GetPsiDot());

  EXPECT_FLOAT_EQ(0, output->GetTime().position);
}

TEST(StateInterpolation, DubinTest) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace su = ca::planning_common::state_utils;

  ob::StateSpacePtr space(new ob::DubinsStateSpace(1.0));
  ob::RealVectorBounds bounds(2);
  bounds.setLow(0);
  bounds.setHigh(1);
  space->as<ob::DubinsStateSpace>()->setBounds(bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  ob::ScopedState<ob::DubinsStateSpace> sample1(space), sample2(space), sample3(space);
  sample1->setXY(0, 0);
  sample1->setYaw(0);

  sample2->setXY(1, 0);
  sample2->setYaw(1);

  si->getStateSpace()->InterpolateWithoutMotion(sample1.get(), sample2.get(), 0.5, sample3.get());

  EXPECT_FLOAT_EQ(0.5, sample3->getX());
  EXPECT_FLOAT_EQ(0, sample3->getY());
  EXPECT_FLOAT_EQ(0.5, sample3->getYaw());
}

TEST(StateInterpolation, DubinTest2) {
  namespace ob = ompl::base;
  namespace pc = ca::planning_common;
  namespace su = ca::planning_common::state_utils;

  ob::StateSpacePtr space(new ob::DubinsStateSpace(1.0));
  ob::RealVectorBounds bounds(2);
  bounds.setLow(0);
  bounds.setHigh(1);
  space->as<ob::DubinsStateSpace>()->setBounds(bounds);
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();

  ob::ScopedState<ob::DubinsStateSpace> sample1(space), sample2(space), sample3(space);
  sample1->setXY(0, 0);
  sample1->setYaw(-M_PI+1);

  sample2->setXY(1, 0);
  sample2->setYaw(M_PI-1);

  si->getStateSpace()->InterpolateWithoutMotion(sample1.get(), sample2.get(), 0.4, sample3.get());

  EXPECT_FLOAT_EQ(0.4, sample3->getX());
  EXPECT_FLOAT_EQ(0, sample3->getY());
  EXPECT_FLOAT_EQ(-M_PI+0.2, sample3->getYaw());
}


int main(int argc, char **argv){
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}





