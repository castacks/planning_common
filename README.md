# README #

Contains a bunch of common utilities for planning system. This builds on ompl - deriving its data structures. The major contribution of this package is defining the workspace - something ompl didnt define originally. This has significantly made abstraction easy.

This package should not contain any specific implementations - only things that are absolutely fundamental. This is important to keep this package light. See spin off packages such as curvature_constrained_state_spaces, common_cost_maps that specialize. If you have invented an awesome cost function or collision check, make a new package. Follow the common conventions. Add the package name to the metarepository list for code discovery. We will then definitely use your awesome function.

### How do I get setup? ###


```
#!bash

rosrun planning_common planning_common_example_path_waypoint
```


# Contact #
Author and Maintainer: Sanjiban Choudhury (sanjiban@cmu.edu)