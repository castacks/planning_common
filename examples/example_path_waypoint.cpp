/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_path_waypoint.cpp
 *
 *  Created on: Jun 5, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/paths/path_waypoint.h"
#include "ompl/base/State.h"
#include "ompl/base/ScopedState.h"
#include "ompl/base/spaces/SE2StateSpace.h"
#include "planning_common/states/se3_tangent_bundle.h"
#include "planning_common/utils/state_utils.h"

#include <ros/ros.h>

using namespace ca;

namespace pc = planning_common;
namespace ob = ompl::base;
namespace su = pc::state_utils;

int main(int argc, char **argv) {
  ros::init(argc, argv, "example_path_waypoint");
  ros::NodeHandle n("~");
/*
  // Lets define an SE2 space (x, y, theta).
  // Assumed that first space is workspace (x,y)
  ob::StateSpacePtr space(new ob::SE2StateSpace());
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  // Create start state and goal state
  ob::ScopedState<ob::SE2StateSpace> start(space), goal(space);

  start->setXY(0, 0);
  start->setYaw(0);

  goal->setXY(10, 10);
  goal->setYaw(M_PI);

  // Create the path
  ompl::base::ScopedState<> s1 = start;
  ompl::base::ScopedState<> s2 = goal;
  pc::PathWaypoint path(si, s1.get(), s2.get());

  // Lets set 10 waypoints
  path.InterpolateExclusive(10);

  // Print path 2 ways

  path.print(std::cout);

  std::cout << "\nLength is "<<path.length()<<"\n";

  // Print the jacobian -  we know there will be just the one for this kind of parameterization
  std::cout << "\nParameter \n"<<path.GetParameter()<<"\n";
*/

  ob::SpaceInformationPtr si = su::GetStandardSE3TangentBundle();
  ob::ScopedState<pc::SE3TangentBundle> temp(si);

  pc::PathWaypoint path(si);

  temp->GetPose().setXYZ(0,0,0);
  path.AppendWaypoint(temp.get());

  temp->GetPose().setXYZ(100,0,0);
  path.AppendWaypoint(temp.get());

//  temp->GetPose().setXYZ(100,100,0);
//  path.AppendWaypoint(temp.get());

//  path.Interpolate(40);

  std::cout << "Original path \n";
  path.PrintAsMatrix(std::cout);

  path.InterpWithDownSample(3);
  std::cout << "Dowsampled path \n";
  path.PrintAsMatrix(std::cout);
}


