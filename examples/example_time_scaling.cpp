/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_time_scaling.cpp
 *
 *  Created on: Jun 27, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/states/se3_tangent_bundle.h"
#include "planning_common/states/time_scalable_property.h"
#include "planning_common/utils/state_utils.h"
#include "ompl/base/SpaceInformation.h"
#include <iostream>

namespace ob = ompl::base;
namespace caob = ca::ompl_base;
namespace pc = ca::planning_common;
namespace su = pc::state_utils;

int main(int argc, char **argv) {

  ob::StateSpacePtr space(new pc::SE3TangentBundle());

  ob::RealVectorBounds bounds(3);
  bounds.setLow(0);
  bounds.setHigh(1);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);

  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));

  si->setup();
  si->workspace_information().PrintInformation(std::cout);

  // Lets create a state
  ob::ScopedState<pc::SE3TangentBundle> sample(space);
  sample->GetPose().setXYZ(1,2,3);
  sample->GetTangentPose().SetXYZDot(4,5,6);
  su::ConvertEulerToSO3 (0.5, 0, 0, &sample->GetPose().rotation());
  sample->GetTangentPose().SetPhiThetaPsiDot(0.1, 0.2, 0.3);
  std::cout<<"Old State: " <<sample<<std::endl;

  ob::State *state = sample.get();

  // Cast state to time scalabe?
  if (const pc::TimeScalable *space_time_scalable = dynamic_cast<const pc::TimeScalable*> (si->getStateSpace().get()) ) {
    std::cout<<"Can dynamically cast to time"<<std::endl;
    // Slow down by 0.5
    space_time_scalable->ScaleTime(sample.get(), 0.5);
    std::cout<<"New State: " <<sample<<std::endl;
  }

}


