/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_smoothness_objective.cpp
 *
 *  Created on: Jun 6, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/paths/path_waypoint.h"
#include "ompl/base/State.h"
#include "ompl/base/ScopedState.h"
#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "planning_common/objectives/waypoint_smoothness_objective.h"

#include <ros/ros.h>

using namespace ca;

namespace pc = planning_common;
namespace ob = ompl::base;

int main(int argc, char **argv) {
  ros::init(argc, argv, "example_smoothness_waypoint");
  ros::NodeHandle n("~");

  // Lets define an SE2 space (x, y, theta).
  // Assumed that first space is workspace (x,y)
  ob::StateSpacePtr space(new ob::RealVectorStateSpace(2));
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  // Create start state and goal state
  ob::ScopedState<> start(space), goal(space);

  start[0] = 0;
  start[1] = 0;

  goal[0] = 10;
  goal[1] = 10;

  // Create the path
  ompl::base::ScopedState<> s1 = start;
  ompl::base::ScopedState<> s2 = goal;
  pc::PathWaypoint path(si, s1.get(), s2.get());

  path.print(std::cout);
  // Lets set 10 waypoints
  path.InterpolateExclusive(20);
  path.print(std::cout);
  pc::WaypointSmoothnessObjective obj(si);
  std::vector<double> alpha;
  alpha.push_back(1);
  alpha.push_back(0.1);
  alpha.push_back(0.01);
  if (obj.Setup(path, start, goal, alpha))
    ROS_INFO_STREAM("Successfully setup smoothness objective");


  std::cout<<"\nA:\n"<<obj.A();
  std::cout<<"\nb:\n"<<obj.b();
  std::cout<<"\nc:\n"<<obj.c();
  std::cout<<"\nAinv:\n"<<obj.A().inverse()<<"\n";
}




