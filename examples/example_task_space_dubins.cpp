/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_single_dubins_check.cpp
 *
 *  Created on: Aug 24, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/motion_validator/task_space_dubins_motion_validator.h"
#include "planning_common/state_validity/bounds_state_validity_checker.h"
#include "ompl/base/ScopedState.h"

namespace pc = ca::planning_common;
namespace ob = ompl::base;

int main(int argc, char **argv) {

  // Set up a dubins space of radius 2
  ob::StateSpacePtr space(new ob::DubinsStateSpace(2.0));
  ob::RealVectorBounds bounds(2);
  bounds.setLow(-100);
  bounds.setHigh(100);
  space->as<ob::DubinsStateSpace>()->setBounds(bounds);

  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setStateValidityChecker(ob::StateValidityCheckerPtr(new pc::BoundsStateValidityChecker(si)));
  si->setStateValidityCheckingResolution(0.001);
  si->setup();

  // Create the task space motion validator
  pc::TaskSpaceDubinsMotionValidator ts_dubins(si);


  // Create a start and goal
  ob::ScopedState<ob::SE2StateSpace> start(space), goal(space), goal_full(space);

  start->setXY(0, 0);
  start->setYaw(0);

  goal->setXY(0.3, 0.1);

  // Check if a valid motion exists and get the new end point
  bool result = ts_dubins.CheckMotion(start.get(), goal.get(), goal_full.get());
  double distance = ts_dubins.Distance(start.get(), goal.get());

  // Print results
  std::cout<<" Goal reached? "<<result<<std::endl;
  std::cout<<goal_full<<std::endl;
  std::cout<<"Distance: "<<distance<<std::endl;

  // Lets try to verify that dubins would give the same answer
  double distance_full = space->distance(start.get(), goal_full.get());
  std::cout<<"Full dubins distance: "<<distance_full<<std::endl;

}




