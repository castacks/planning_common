/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_workspace_checking.cpp
 *
 *  Created on: Jun 25, 2014
 *      Author: Sanjiban Choudhury
 */

#include "planning_common/states/se3_tangent_bundle.h"
#include "planning_common/utils/workspace_utils.h"
#include "planning_common/utils/state_utils.h"
#include "ompl/base/SpaceInformation.h"
#include <iostream>

namespace ob = ompl::base;
namespace caob = ca::ompl_base;
namespace pc = ca::planning_common;
namespace wu = pc::workspace_utils;
namespace su = pc::state_utils;
int main(int argc, char **argv) {

  ob::StateSpacePtr space(new pc::SE3TangentBundle());

  ob::RealVectorBounds bounds(3);
  bounds.setLow(0);
  bounds.setHigh(1);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);

  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));

  si->setup();
  si->workspace_information().PrintInformation(std::cout);

  // Lets create a state
  ob::ScopedState<pc::SE3TangentBundle> sample(space);
  sample->GetPose().setXYZ(1,2,3);
  sample->GetTangentPose().SetXYZDot(4,5,6);
  su::ConvertEulerToSO3 (0.5, 0, 0, &sample->GetPose().rotation());
  sample->GetTangentPose().SetPhiThetaPsiDot(0.1, 0.2, 0.3);
  std::cout<<sample<<std::endl;

  // Lets see its sample workspace
  std::cout<<wu::GetTranslationVectorXd(si, sample.get()).transpose()<<std::endl;

  // Lets see its sample velocity space
  std::cout<<wu::GetTangentTranslationVectorXd(si, sample.get()).transpose()<<std::endl;

  // Rotation
  std::cout<<wu::GetRotationVector3d(si, sample.get()).transpose()<<std::endl;

  // Rotation rate
  std::cout<<wu::GetTangentRotationVector3d(si, sample.get()).transpose()<<std::endl;

  typedef std::map<std::string, ob::StateSpace::ValueLocation> value_map;

  for (value_map::const_iterator it = si->getStateSpace()->getValueLocationsByName().begin(); it != si->getStateSpace()->getValueLocationsByName().end(); ++it)
    std::cout << it->first << "value "<<it->second.index<<std::endl;

  for (std::size_t j = 0; j < si->getStateSpace()->getValueLocations().size(); j++)
    std::cout<<*si->getStateSpace()->getValueAddressAtLocation(sample.get(), si->getStateSpace()->getValueLocations()[j]) <<std::endl;

  for (std::size_t j = 0; j < si->getStateSpace()->getValueLocations().size(); j++)
    if (si->getStateSpace()->getValueLocations()[j].stateLocation.space->getName() == si->workspace_information().workspace_map().at(caob::WorkspaceTags::TRANSLATION))
      std::cout<<"Index is "<<j<<std::endl;


//  const std::string name = si->workspace_information().workspace_map().at(caob::WorkspaceTags::TRANSLATION);
//  const ob::StateSpace::ValueLocation loc =si->getStateSpace()->getValueLocationsByName().at(name);
//  const std::vector<ob::StateSpace::ValueLocation> valuemap = si->getStateSpace()->getValueLocations();
//  const std::vector<ob::StateSpace::ValueLocation>::iterator it = std::find(valuemap.begin(), valuemap.end(), loc);
//  loc.stateLocation.space->getName()
  //std::cout<<"  "<<std::find(si->getStateSpace()->getValueLocations().begin(), si->getStateSpace()->getValueLocations().end(), loc) - si->getStateSpace()->getValueLocations().begin() <<std::endl;

}


