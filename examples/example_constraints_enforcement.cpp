/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_constraints_enforcement.cpp
 *
 *  Created on: Jun 16, 2014
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "planning_common/paths/path_waypoint.h"
#include "ompl/base/spaces/DubinsStateSpace.h"
#include "ompl/base/ScopedState.h"

using namespace ca;
namespace ob = ompl::base;
namespace pc = planning_common;
int main(int argc, char **argv) {
  ros::init(argc, argv, "example_constraints_enforcement");
  ros::NodeHandle n("~");

  // Lets define an SE2 space (x, y, theta).
  // Assumed that first space is workspace (x,y)
  ob::StateSpacePtr space(new ob::DubinsStateSpace(0.2));
  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  // Create start state and goal state
  ob::ScopedState<ob::SE2StateSpace> start(space), goal(space);

  start->setXY(0, 0);
  start->setYaw(0);

  goal->setXY(1, 1);
  goal->setYaw(0);

  // Create the path
  ompl::base::ScopedState<> s1 = start;
  ompl::base::ScopedState<> s2 = goal;
  pc::PathWaypoint path(si, s1.get(), s2.get());

  // Lets set 10 waypoints
  path.InterpolateExclusive(100);
  std::cout<<"\n"<<path.GetParameter()<<"\n";
  if(path.EnforceDynamics(start.get(), goal.get(), 0.01))
    std::cout<<"\nModified path\n"<<path.GetParameter()<<"\n";
  else
    std::cout<<"\nCouldnt enforce dynamcis\n";
}


