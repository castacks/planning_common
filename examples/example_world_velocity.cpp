/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_world_velocity.cpp
 *
 *  Created on: Jul 21, 2014
 *      Author: Sanjiban Choudhury
 */


#include "planning_common/states/se3_tangent_bundle.h"
#include "planning_common/utils/workspace_utils.h"
#include "planning_common/utils/state_utils.h"
#include "ompl/base/SpaceInformation.h"
#include <iostream>

namespace ob = ompl::base;
namespace caob = ca::ompl_base;
namespace pc = ca::planning_common;
namespace wu = pc::workspace_utils;
namespace su = pc::state_utils;
int main(int argc, char **argv) {

  ob::StateSpacePtr space(new pc::SE3TangentBundle());

  ob::RealVectorBounds bounds(3);
  bounds.setLow(0);
  bounds.setHigh(1);
  space->as<pc::SE3TangentBundle>()->SetBounds(bounds, bounds, bounds);

  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));

  si->setup();
  si->workspace_information().PrintInformation(std::cout);

  // Lets create a state
  ob::ScopedState<pc::SE3TangentBundle> sample(space);
  sample->GetPose().setXYZ(1,2,3);   // x,y,z is 1,2,3
  sample->GetTangentPose().SetXYZDot(1,0,0); // body vel is 1, 0, 0
  su::ConvertEulerToSO3 (0, 0, M_PI/4.0, &sample->GetPose().rotation()); // heading is +45 deg
  sample->GetTangentPose().SetPhiThetaPsiDot(0, 0, 0);
  std::cout<<sample<<std::endl;


  // Lets see body velocity
  std::cout<<"body vel" <<wu::GetTangentTranslationVector3d(si, sample.get()).transpose()<<std::endl;

  // Rotation
  std::cout<<"rotation" <<wu::GetRotationVector3d(si, sample.get()).transpose()<<std::endl;

  // Lets see world velocity
  std::cout<<"world vel" <<wu::GetTangentTranslationVector3dWorld(si, sample.get()).transpose()<<std::endl;

}





