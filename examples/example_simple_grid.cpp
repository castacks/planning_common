/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_simple_grid.cpp
 *
 *  Created on: Jun 8, 2014
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "planning_common/data_structures/simple_grid_cost_map.h"

using namespace ca;
namespace pc = planning_common;

int main(int argc, char **argv) {
  ros::init(argc, argv, "example_simple_grid");
  ros::NodeHandle n("~");

  ros::Publisher pub_forest = n.advertise<visualization_msgs::MarkerArray>("gradients", 0);
  ros::Duration(1.0).sleep();

  std::string filename;
  if (!n.getParam("filename", filename)) {
    ROS_ERROR_STREAM("Couldnt find file!");
    return EXIT_FAILURE;
  }

  pc::SimpleGridCostMap simple_grid;
  ROS_INFO_STREAM(filename);
  simple_grid.Initialize(Eigen::Vector2d(-0.1, -0.1), Eigen::Vector2d(1.1, 1.1), filename);

  ROS_INFO_STREAM("Value: "<<simple_grid.GetValue(Eigen::Vector2d(0.1, 0.7))<<" Gradient "<<simple_grid.GetGradient(Eigen::Vector2d(0.1, 0.7)).transpose());

  pub_forest.publish(simple_grid.GetMarkerArray());
  ros::Duration(1.0).sleep();
}


