/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * example_waypoint_obstacle_objective.cpp
 *
 *  Created on: Jun 8, 2014
 *      Author: Sanjiban Choudhury
 */

#include <ros/ros.h>
#include "planning_common/data_structures/simple_grid_cost_map.h"

#include "planning_common/paths/path_waypoint.h"
#include "ompl/base/State.h"
#include "ompl/base/ScopedState.h"
#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "planning_common/objectives/waypoint_obstacle_distance_objective.h"
#include "planning_common/utils/visualization_utils.h"

using namespace ca;
namespace pc = planning_common;
namespace ob = ompl::base;
namespace vu = pc::visualization_utils;

int main(int argc, char **argv) {
  ros::init(argc, argv, "example_waypoint_obstacle_objective");
  ros::NodeHandle n("~");

  ros::Publisher pub_path = n.advertise<visualization_msgs::MarkerArray>("path_gradient", 0);
  ros::Duration(1.0).sleep();

  std::string filename;
  if (!n.getParam("filename", filename)) {
    ROS_ERROR_STREAM("Couldnt find file!");
    return EXIT_FAILURE;
  }

  pc::SimpleGridCostMap simple_grid;
  ROS_INFO_STREAM(filename);
  simple_grid.Initialize(Eigen::Vector2d(-0.1, -0.1), Eigen::Vector2d(1.1, 1.1), filename);
  pc::CostMap::Ptr signed_distance_field(new pc::SimpleGridCostMap(simple_grid));

  // Lets define an SE2 space (x, y, theta).
  // Assumed that first space is workspace (x,y)
  ob::StateSpacePtr space(new ob::RealVectorStateSpace(2));
  ob::RealVectorBounds bounds(2);
  bounds.setLow(0);
  bounds.setHigh(1);
  space->as<ob::RealVectorStateSpace>()->setBounds(bounds);

  ob::SpaceInformationPtr si(new ob::SpaceInformation(space));
  si->setup();
  // Create start state and goal state
  ob::ScopedState<> start(space), goal(space);

  start[0] = 0;
  start[1] = 0;

  goal[0] = 1;
  goal[1] = 1;

  // Create the path
  ompl::base::ScopedState<> s1 = start;
  ompl::base::ScopedState<> s2 = goal;
  pc::PathWaypoint path(si, s1.get(), s2.get());
  path.InterpolateExclusive(100);
  pc::PathParametric::Ptr path_ptr(new pc::PathWaypoint(path));

  pc::WaypointObstacleDistanceObjective obj(si);
  if (obj.Setup(signed_distance_field, 1.0))
    ROS_INFO_STREAM("Successfully setup smoothness objective");

  ROS_INFO_STREAM("Cost: "<<obj.pathCost(path_ptr.get()));

  Eigen::MatrixXd gradient = obj.Gradient(path_ptr.get());
  std::cout <<"\n"<< gradient <<"\n";

  pub_path.publish(vu::GetMarkerArrayGradient(gradient, path, 2, 0.01, 0, 1, 0, 1));
  ros::Duration(1.0).sleep();
}





