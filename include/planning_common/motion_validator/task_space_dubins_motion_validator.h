/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * task_space_dubins_motion_validator.h
 *
 *  Created on: Aug 24, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_MOTION_VALIDATION_TASK_SPACE_DUBINS_MOTION_VALIDATOR_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_MOTION_VALIDATION_TASK_SPACE_DUBINS_MOTION_VALIDATOR_H_

#include "planning_common/motion_validator/task_space_motion_validator.h"
#include "ompl/base/spaces/DubinsStateSpace.h"

namespace ca {
namespace planning_common {

class TaskSpaceDubinsMotionValidator : public TaskSpaceMotionValidator
{
 public:
  enum DubinsPathSegmentType { DUBINS_LEFT=0, DUBINS_STRAIGHT=1, DUBINS_RIGHT=2 };

  static const DubinsPathSegmentType single_dubins_path_type_[2][2];

  class SingleDubinsPath {
   public:
    SingleDubinsPath(const DubinsPathSegmentType* type = single_dubins_path_type_[0],
                     double t=0., double p=std::numeric_limits<double>::max())
   : type_(type) {
      length_[0] = t;
      length_[1] = p;
      assert(t >= 0.);
      assert(p >= 0.);
    }

    double length() const {
      return length_[0] + length_[1];
    }

    /** Path segment types */
    const DubinsPathSegmentType* type_;
    /** Path segment lengths */
    double length_[2];
  };


  TaskSpaceDubinsMotionValidator(ompl::base::SpaceInformation* si)
 : TaskSpaceMotionValidator(si) {
    DefaultSettings();
  }
  TaskSpaceDubinsMotionValidator(const ompl::base:: SpaceInformationPtr &si)
  : TaskSpaceMotionValidator(si) {
    DefaultSettings();
  }
  virtual ~ TaskSpaceDubinsMotionValidator(void)
  {
  }

  virtual bool CheckMotion(const ompl::base::State *s1, const ompl::base::State *s2, ompl::base::State *new_state) const;

  virtual void GetEndState(const ompl::base::State *s1, const ompl::base::State *s2, ompl::base::State *new_state) const;

  virtual double Distance(const ompl::base::State *state1, const ompl::base::State *state2) const;

  SingleDubinsPath ComputeSingleDubinsPath(const ompl::base::State *state1, const ompl::base::State *state2) const;

  void Interpolate(const ompl::base::State *from, const ompl::base::State *to, const double t, bool& firstTime, SingleDubinsPath& path, ompl::base::State *state) const;

  void Interpolate(const ompl::base::State *from, const SingleDubinsPath& path, double t, ompl::base::State *state) const;

 private:
  ompl::base::DubinsStateSpace *stateSpace_;
  void DefaultSettings(void);
};

typedef boost::shared_ptr<TaskSpaceDubinsMotionValidator> TaskSpaceDubinsMotionValidatorPtr;

}
}


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_MOTION_VALIDATION_TASK_SPACE_DUBINS_MOTION_VALIDATOR_H_ 
