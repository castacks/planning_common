/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * task_space_motion_validator.h
 *
 *  Created on: Aug 24, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_MOTION_VALIDATION_TASK_SPACE_MOTION_VALIDATOR_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_MOTION_VALIDATION_TASK_SPACE_MOTION_VALIDATOR_H_

#include "ompl/base/State.h"
#include "ompl/base/SpaceInformation.h"
#include <utility>

namespace ca {
namespace planning_common{


/** \brief Abstract definition for a class checking the
            validity of motions in TASK SPACE.
            Often planners need to
             */

/**
 * \brief Abstract class that checks motion for connections in task space.
 * Task space is sort of ambiguous and depends on the state space and application.
 * It is useful for example in RRT when we sample in task space and then try to extend the
 * connection using some local control law. Whatever the resultant state is, it is added to the tree
 * Refer to
 * Shkolnik, Alexander, and Russ Tedrake. "Path planning in 1000+ dimensions using a task-space Voronoi bias." Robotics and Automation, 2009. ICRA'09. IEEE International Conference on. IEEE, 2009.
 *
 */
class TaskSpaceMotionValidator {
 public:

  /** \brief Constructor */
  TaskSpaceMotionValidator(ompl::base::SpaceInformation* si)
 : si_(si) {}

  /** \brief Constructor */
  TaskSpaceMotionValidator(const ompl::base::SpaceInformationPtr &si)
  : si_(si.get()){}

  virtual ~TaskSpaceMotionValidator(void) {}

  /**
   * \brief Check motion between two states. Only use the task space component of s2 and ignore else. Resultant state is computed
   * @param s1 Start state (used fully)
   * @param s2 Goal state (used partially)
   * @param new_state Resultant full state
   * @return true if successful
   */
  virtual bool CheckMotion(const ompl::base::State *s1, const ompl::base::State *s2, ompl::base::State *new_state) const = 0;

  virtual void GetEndState(const ompl::base::State *s1, const ompl::base::State *s2, ompl::base::State *new_state) const = 0;

  virtual double Distance(const ompl::base::State *state1, const ompl::base::State *state2) const = 0;

 protected:

  /** \brief The instance of space information this state validity checker operates on */
  ompl::base::SpaceInformation    *si_;

};

typedef boost::shared_ptr<TaskSpaceMotionValidator> TaskSpaceMotionValidatorPtr;

}  // namespace planning_common
}  // namespace ca


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_MOTION_VALIDATION_TASK_SPACE_MOTION_VALIDATOR_H_ 
