/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * planner_progress.h
 *
 *  Created on: Jan 7, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_PLANNER_DATA_PLANNER_PROGRESS_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_PLANNER_DATA_PLANNER_PROGRESS_H_

#include "ompl/base/Planner.h"
#include "ompl/util/Time.h"
#include <boost/scoped_ptr.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/progress.hpp>
#include <boost/thread.hpp>
#include <iostream>

namespace ca {

class PlannerProgress {
 public:
  typedef std::vector<std::map<std::string, std::string> > PlannerProgressData;
  typedef std::vector<std::string> PlannerProgressFields;

  PlannerProgress()
  : planner_progress_fields_(),
    planner_progress_data_(),
    run_thread_(false),
    thread_(),
    rate_(100.0) {}
  ~PlannerProgress() {}

  void StartCollectingData(const ompl::base::PlannerPtr &planner);

  void StopCollectingData();

  void PrintData(std::ostream &out) const;

  void SaveData(std::string filename) const;

  void set_rate(double rate) {rate_ = rate;}
 private:

  void CollectProgressProperties(const ompl::base::Planner::PlannerProgressProperties& properties);

  PlannerProgressFields planner_progress_fields_;
  PlannerProgressData planner_progress_data_;
  bool run_thread_;
  boost::scoped_ptr<boost::thread> thread_;
  double rate_;
};

}

#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_PLANNER_DATA_PLANNER_PROGRESS_H_ 
