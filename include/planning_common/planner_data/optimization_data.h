/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * optimization_data.h
 *
 *  Created on: Jun 15, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_PLANNER_PARAMETERS_OPTIMIZATION_DATA_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_PLANNER_PARAMETERS_OPTIMIZATION_DATA_H_

#include "ompl/base/PlannerData.h"
#include "ompl/base/Cost.h"
#include "planning_common/paths/path_parametric.h"
#include <Eigen/Dense>

namespace ca {
namespace planning_common {

/** \brief A child class of planner data that represents
 * additional information an optimization planner is likely
 * to return
 */
class OptimizationData : public ompl::base::PlannerData {
 public:
  typedef boost::shared_ptr <OptimizationData> Ptr;
  struct OptimizationState {
    ompl::base::Cost cost;
    PathParametric::Ptr path;
    PathParametric::Gradient gradient;
    double step_size;
    bool restart;
    OptimizationState()
    : cost(),
      path(),
      gradient(),
      step_size(),
      restart(){}
  };

  OptimizationData(const ompl::base::SpaceInformationPtr &si)
 : ompl::base::PlannerData(si),
   num_iterations_(),
   optimization_state_() {}


  virtual ~OptimizationData() {};

  /**
   * \brief Add information that is computed every iteratipn
   * @param iteration_id_ The iteration number
   * @param cost Cost of the path in that iteration
   * @param path The parameters of the path for that iteration (more informative that discretized waypoints)
   * @param gradient The gradient of the parameterized path in that iteration
   * @param step_size The step size in that iteration
   * @param restart Whether the optimization had to restart
   */
  void AddIterationInfo (unsigned int iteration_id, const ompl::base::Cost &cost, const PathParametric::Ptr &path, const PathParametric::Gradient &gradient, double step_size, bool restart);

  /** \brief Get iteration information for the iteration_id */
  OptimizationState GetIterationInfo (unsigned int iteration_id);
  /** \brief Set the total iterations executed by the planner */
  unsigned int num_iterations() const {return num_iterations_;}

  /** \brief Get the number of iterations executed by the planner */
  void set_num_iterations(unsigned int num_iterations) {num_iterations_ = num_iterations;}

  typedef std::map < unsigned int, OptimizationState > OptimizationStateSet;
  const OptimizationStateSet& optimization_state() const {return optimization_state_;}
 protected:
  unsigned int num_iterations_;
  std::map < unsigned int, OptimizationState > optimization_state_;
};

}  // namespace planning_common
}  // namespace ca


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_PLANNER_PARAMETERS_OPTIMIZATION_DATA_H_ 
