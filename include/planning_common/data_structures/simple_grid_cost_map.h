/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * simple_grid_cost_map.h
 *
 *  Created on: Jun 13, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_DATA_STRUCTURES_SIMPLE_GRID_COST_MAP_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_DATA_STRUCTURES_SIMPLE_GRID_COST_MAP_H_

#include <vector>
#include <math.h>
#include <visualization_msgs/MarkerArray.h>
#include "planning_common/data_structures/cost_map.h"

namespace ca {
namespace planning_common {

/** \brief A naive example of a simple 2D grid cost map. Please \b DONOT use for any actual application,
 * only for examples.
 * A grid is read from a file. It represents values and gradients. This grid can be assumed to have
 * an arbitrary origin and dimension allowing the user to stretch the extents.
 */
class SimpleGridCostMap : public CostMap {
 public:
  typedef std::vector<Eigen::Vector2d, Eigen::aligned_allocator<Eigen::Vector2d> > Vector2dVector;

  SimpleGridCostMap()
 : xdim_(),
   ydim_(),
   value_(),
   gradient_(),
   origin_(),
   dimension_(),
   has_initialized_(false){}

  virtual ~SimpleGridCostMap(){}

  /** \brief Initialize grid
   * @param origin body world start of the grid
   * @param dimension the bounding box dimensions
   * @param filename filename of grid to load it
   * @return true if initialized
   */
  bool Initialize(const Eigen::Vector2d &origin, const Eigen::Vector2d &dimension, std::string filename);

  /** \brief Get value of grid
   * @param query Will expect this to be 2D
   * @return value of grid
   */
  virtual double GetValue(const Eigen::VectorXd &query);

  /** \brief Get gradient at query
   * @param query Will expect this to be 2D
   * @return gradient (2D)
   */
  virtual Eigen::VectorXd GetGradient(const Eigen::VectorXd &query);

  /** \brief Get marker array to visualize grid */
  visualization_msgs::MarkerArray GetMarkerArray();

 protected:
  /** \brief Is query in grid? */
  bool IsInGrid(const Eigen::Vector2d &query);
  /** \brief Get index in grid */
  unsigned int GetIndex (const Eigen::Vector2d &query);
  /** \brief Get x location in grid*/
  inline unsigned int GetX(const Eigen::Vector2d &query) {return round(((query[0] - origin_[0])/dimension_[0])*(xdim_-1));}
  /** \brief Get y location in grid*/
  inline unsigned int GetY(const Eigen::Vector2d &query) {return round(((query[1] - origin_[1])/dimension_[1])*(ydim_-1));}
  /** \brief Get x location in world*/
  inline double GetXWorld(unsigned int x) { return origin_[0] + x*dimension_[0]/(xdim_-1);}
  /** \brief Get y location in world*/
  inline double GetYWorld(unsigned int y) { return origin_[1] + y*dimension_[1]/(ydim_-1);}
  /** \brief Get resolution*/
  inline double GetResolution() { return sqrt((dimension_[0]/xdim_)*(dimension_[0]/xdim_) +  (dimension_[1]/ydim_)*(dimension_[1]/ydim_)); }
  /** \brief x dimension of cell*/
  unsigned int xdim_;
  /** \brief y dimension of cell*/
  unsigned int ydim_;
  /** \brief value vector of grid */
  std::vector<double> value_;
  /** \brief gradient vector of grid */
  Vector2dVector gradient_;
  /** \brief origin of grid */
  Eigen::Vector2d origin_;
  /** \brief dim of boundign box */
  Eigen::Vector2d dimension_;
  /** \brief has it initialzied */
  bool has_initialized_;
};

}  // namepsace ca
}  // namespace planning_common



#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_DATA_STRUCTURES_SIMPLE_GRID_COST_MAP_H_ 
