/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * cost_map.h
 *
 *  Created on: Jun 13, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_DATA_STRUCTURES_COST_MAP_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_DATA_STRUCTURES_COST_MAP_H_

#include <Eigen/Dense>
#include <boost/shared_ptr.hpp>
namespace ca {
namespace planning_common {

/**
 * \brief This is an abstract class that represents a map
 * containing cost information of some kind.
 * For example, it could be a voxelgrid of distances from
 * obstacle. It could be a map of feasible and infeasible
 * regions.
 * The requirement from a map is to get a value and get a
 * gradient.
 */
class CostMap {
 public:
  typedef boost::shared_ptr<CostMap> Ptr;
  CostMap() {};
  virtual ~CostMap() {};

  /** \brief Get value at query location. Currently the value is assumed to be a double.
   *
   * @param query Eigen column vector representing a general query
   * @return cost at location
   */
  virtual double GetValue(const Eigen::VectorXd &query) = 0;

  /** \brief Get graident at query location.
   *
   * @param query Eigen column vector representing a general query
   * @return Eigen column vector representing the gradient. Assume guaranteed that
   * dim(query) = dim(gradient)
   */
  virtual Eigen::VectorXd GetGradient(const Eigen::VectorXd &query) = 0;

  /**
   * Reluctantly added this because if it is discrete then the resolution must be known.
   * If it is not discrete - resolution should be 0 (which is set as the default value)
   */
  virtual double GetResolution() {return 0;}

  /**
   * \brief Is the query valid. If not, pay no attention to its gradient or value
   * @param query
   * @return
   */
  virtual bool IsValid(const Eigen::VectorXd &query) {return true;}

};

}  // namepsace ca
}  // namespace planning_common


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_DATA_STRUCTURES_COST_MAP_H_ 
