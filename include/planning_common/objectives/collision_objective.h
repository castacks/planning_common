/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * collision_objective.h
 *
 *  Created on: Sep 4, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_COLLISION_OBJECTIVE_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_COLLISION_OBJECTIVE_H_

#include "ompl/base/OptimizationObjective.h"
#include "planning_common/state_validity/state_validity_dt.h"

namespace ca {
namespace planning_common {

class CollisionObjective : public ompl::base::OptimizationObjective {
 public:
  CollisionObjective(const ompl::base::SpaceInformationPtr &si, CostMap::Ptr dist_to_obst, double robot_size)
 :ompl::base::OptimizationObjective(si),
  state_validity_(si, dist_to_obst, robot_size){
    description_ = "Collision";
  }

  virtual ~CollisionObjective() {}

  virtual ompl::base::Cost pathCost(const ompl::base::Path *path) const {
    if (const PathWaypoint *path_wp = dynamic_cast<const PathWaypoint*>(path)) {
      for (PathWaypoint::waypoints::const_iterator it = path_wp->Begin(); it != path_wp->End(); it++)
        if (!state_validity_.isValid(*it))
          return ompl::base::Cost(ompl::base::Cost::Infinity);

      return ompl::base::Cost(0);
    } else {
      OMPL_ERROR("Havent implemented yet");
      return ompl::base::Cost(ompl::base::Cost::Infinity);
    }
  }

  virtual ompl::base::Cost motionCost(const ompl::base::State *s1, const ompl::base::State *s2) const {
    OMPL_ERROR("Havent implemented yet");
    return ompl::base::Cost(ompl::base::Cost::Infinity);
  }
private:
  StateValidityCheckerDT state_validity_;
};

}  // namespace planning_common
}  // namespace ca


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_COLLISION_OBJECTIVE_H_ 
