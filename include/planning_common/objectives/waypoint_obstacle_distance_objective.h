/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * waypoint_obstacle_distance_objective.h
 *
 *  Created on: Jun 13, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_WAYPOINT_OBSTACLE_DISTANCE_OBJECTIVE_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_WAYPOINT_OBSTACLE_DISTANCE_OBJECTIVE_H_

#include "planning_common/objectives/differentiable_objective.h"
#include "planning_common/paths/path_parametric.h"
#include "planning_common/paths/path_waypoint.h"
#include "representation_interface/obstacle_grid_representation_interface.h"
#include "ompl/base/ScopedState.h"

namespace ca {
namespace planning_common {

/** \brief Cost function measuring the proximity to obstacles.
 * Locations outside obstacle are penalize proportional to
 * squared distance from the obstacle. Inside obstacle penalization
 * proportional to distance.
 * The structure of this cost function appears in the following:
 * @par External documentation
 * Zucker, M., Ratliff, N., Dragan, A. D., Pivtoraiko, M., Klingensmith, M., Dellin, C. M., Srinivasa, S. S.
 * (2013). CHOMP: Covariant hamiltonian optimization for motion planning. The International Journal of Robotics Research, 32(9-10), 1164-1193.
 */
class WaypointObstacleDistanceObjective : public DifferentiableObjective {
 public:
  /** \brief Shared ptr */
  typedef boost::shared_ptr<WaypointObstacleDistanceObjective> Ptr;
  WaypointObstacleDistanceObjective(const ompl::base::SpaceInformationPtr &si)
 : DifferentiableObjective(si),
   signed_distance_field_(),
   max_distance_(),
   is_setup_(false),
   premultiply_workspace_(Eigen::Vector3d(1,1,1)){}

  virtual ~WaypointObstacleDistanceObjective() {}

  /** \brief Cost of parametric path */
  virtual ompl::base::Cost pathCost(const ompl::base::Path *path) const;

  /** \brief Gradient of parametric path */
  virtual Eigen::MatrixXd Gradient(const PathParametric *path);

  /**
   * \brief Setup the cost function
   * @param signed_distance_field Costmap that represents the signed distance field
   * @param max_distance  distance to be considered beyond which no cost is applied
   * @return true if set up
   */
  bool Setup(const boost::shared_ptr<representation_interface::ObstacleGridRepresentationInterface> &signed_distance_field, double max_distance);

  void set_premultiply_workspace(const Eigen::Vector3d& premultiply_workspace) {premultiply_workspace_ = premultiply_workspace;}

 protected:
  /** \brief Squared cost*/
  double SquaredCost(double signed_distance) const;

  /** \brief Squared cost gradient*/
  Eigen::VectorXd SquaredCostGradient(double signed_distance, Eigen::VectorXd distance_gradient) const;

  /** \brief Get a matrix of Nxd where N is the waypoints, d is the workspace dim */
  Eigen::MatrixXd GetWorkSpacePath(const PathWaypoint *path_wp) const;

  /** \brief diff() of the matrix */
  Eigen::MatrixXd DerivativePath(const Eigen::MatrixXd &path) const;

  /** \brief Normalize matrix matrix./(normr) */
  Eigen::MatrixXd NormalizePath(const Eigen::MatrixXd &path) const;

  /** \brief Get norm along rows normr() */
  Eigen::VectorXd PathNorm(const Eigen::MatrixXd &path) const;

  /** \brief cost map containing signed distnace */
  boost::shared_ptr<representation_interface::ObstacleGridRepresentationInterface>   signed_distance_field_;

  /** \brief max distance we care about */
  double max_distance_;

  /** \brief is cost function set up */
  bool is_setup_;

  /** \brief needed if you need to make some X,Y,Z gradient 0**/
  Eigen::Vector3d premultiply_workspace_;
};

}  // namepsace planning_common
}  // namespace ca


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_WAYPOINT_OBSTACLE_DISTANCE_OBJECTIVE_H_ 
