/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * time_objective.h
 *
 *  Created on: Jun 23, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_TIME_OBJECTIVE_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_TIME_OBJECTIVE_H_

#include "ompl/base/OptimizationObjective.h"

namespace ca {
namespace planning_common {

class TimeObjective : public ompl::base::OptimizationObjective {
 public:
  typedef boost::shared_ptr<TimeObjective> Ptr;

  TimeObjective(const ompl::base::SpaceInformationPtr &si)
 :ompl::base::OptimizationObjective(si){
    description_ = "Time";
  }

  virtual ~TimeObjective() {}

  /** \brief The path cost gets the end time for the path. Assumes that the state contains a time subspace and
   *  \e Path is of type \e PathGeometric or of type \e PathWaypoint.*/
  virtual ompl::base::Cost pathCost(const ompl::base::Path *path) const;

  virtual ompl::base::Cost motionCost(const ompl::base::State *s1, const ompl::base::State *s2) const;

};

}  // namespace planning_common
}  // namespace ca


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_TIME_OBJECTIVE_H_ 
