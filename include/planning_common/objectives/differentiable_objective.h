/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * differentiable_objective.h
 *
 *  Created on: Jun 13, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_DIFFERENTIABLE_OBJECTIVE_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_DIFFERENTIABLE_OBJECTIVE_H_

#include <utility>
#include "ompl/base/OptimizationObjective.h"
#include "planning_common/paths/path_parametric.h"
#include "Eigen/Dense"


namespace ca {
namespace planning_common {

/** \brief This is an abstract class of objectives that have differentiable objectives
 * and can provide analytical gradients to parametric paths. This objective
 * commits to parametric path representation. This implies only planners committing
 * to parametric path representations can use this (otherwise the structure of the
 * gradient is ill-posed).
 */
class DifferentiableObjective : public ompl::base::OptimizationObjective {
 public:
  /** shared pointer to ca::planning_common::DifferentiableObjective */
  typedef boost::shared_ptr<DifferentiableObjective> Ptr;

  DifferentiableObjective(const ompl::base::SpaceInformationPtr &si):
    ompl::base::OptimizationObjective (si) {}

  virtual ~DifferentiableObjective() {}

  /** \brief Differential Objective deals with parametric cost and doesnt have to support motioncost   */
  virtual ompl::base::Cost motionCost(const ompl::base::State *s1, const ompl::base::State *s2) const {
    BOOST_ASSERT_MSG(true, "Motion Cost illegal for DifferentiableObjective");
    return ompl::base::Cost(0);
  }

  /** \brief Get gradient on the parameters of
   * oarametric path
   * @param path parametric path
   * @return the gradient of the same dimension as the
   * parameter matrix of the path
   */
  virtual Eigen::MatrixXd Gradient(const PathParametric *path) = 0;
};

class MultiDifferentiableObjective : public DifferentiableObjective {
 public:
  /** shared pointer to ca::planning_common::MultiDifferentiableObjective */
  typedef boost::shared_ptr<MultiDifferentiableObjective> Ptr;

  MultiDifferentiableObjective(const ompl::base::SpaceInformationPtr &si):
    DifferentiableObjective(si),
    objectives_(){}

  /** \brief Parse a optimization objective as much as it can */
  bool Parse(const ompl::base::OptimizationObjectivePtr &optimization_objective);

  /** \brief Get cost of a parametric path
   * @param path parametric path
   * @return cost
   */
  virtual ompl::base::Cost pathCost(const ompl::base::Path *path);

  /** \brief Get gradient on the parameters of
   * oarametric path
   * @param path parametric path
   * @return the gradient of the same dimension as the
   * parameter matrix of the path
   */
  virtual Eigen::MatrixXd Gradient(const PathParametric *path);
 protected:
  typedef std::pair<double, DifferentiableObjective::Ptr> DiffComponent;
  std::vector< DiffComponent > objectives_;
};

}  // namespace ca
}  // namespace planning_common


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_DIFFERENTIABLE_OBJECTIVE_H_ 
