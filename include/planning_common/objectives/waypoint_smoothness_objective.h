/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * waypoint_smoothness_objective.h
 *
 *  Created on: Jun 13, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_WAYPOINT_SMOOTHNESS_OBJECTIVE_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_WAYPOINT_SMOOTHNESS_OBJECTIVE_H_

#include "planning_common/objectives/differentiable_objective.h"
#include "planning_common/paths/path_parametric.h"
#include "planning_common/paths/path_waypoint.h"
#include "ompl/base/ScopedState.h"

namespace ca {
namespace planning_common {

/** \brief Computing the smoothness of waypoint parameterized paths.
 * Smoothness is a norm of derivatives of the path.
 * Open questions: Can this be generalized to any path? Notions such as
 * derivatives for aribitrary parameterized paths can be found but their
 * structure is hard to ascertain apriori.
 */
class WaypointSmoothnessObjective : public DifferentiableObjective {
 public:
  /** \brief Shared ptr for ca::planning_common::WaypointSmoothnessObjective */
  typedef boost::shared_ptr<WaypointSmoothnessObjective> Ptr;
  WaypointSmoothnessObjective(const ompl::base::SpaceInformationPtr &si)
 : DifferentiableObjective(si),
   A_(0, 0),
   b_(0, 0),
   c_(0, 0),
   is_setup_(false),
   threshold_residual_(false),
   threshold_(){}

  virtual ~WaypointSmoothnessObjective() {}


  /** \brief Cost of a parametric path
   * @param path Parametric path
   * @return cost
   */
  virtual ompl::base::Cost pathCost(const ompl::base::Path *path) const;

  /** \brief Gradient of a parametric path. The parameters of the path is a matrix,
   * hence the gradient is a matrix.
   * @param path Parametric path
   * @return gradient of the parameters
   */
  virtual Eigen::MatrixXd Gradient(const PathParametric *path);

  /** \brief Setup the cost function
   * @param path Parametric path
   * @param start Start of the trajectory
   * @param end End of the trajectory
   * @param alpha Weights corresponding D^i derivative terms
   * @return
   */
  bool Setup (const PathWaypoint &path, const ompl::base::ScopedState<> &start, const ompl::base::ScopedState<> &end, const std::vector<double> &alpha);

  /** \brief Smoothness quadratic term */
  Eigen::MatrixXd A() const {return A_;}

  /** \brief Smoothness linear term */
  Eigen::MatrixXd b() const {return b_;}

  /** \brief Smoothness constant term */
  Eigen::MatrixXd c() const {return c_;}

  void SetThreshold(double threshold) {
    threshold_residual_ = true;
    threshold_ = threshold;
  }

 protected:
  /** \brief Check if a test matrix has the same dimensions as the smoothing term */
  bool IsMatrixConsistent (const Eigen::MatrixXd &test) const{return test.rows() == A_.cols();}

  /** \brief Get the finite difference operator */
  Eigen::MatrixXd DerivativeMatrix(unsigned int dimension) const;

  /** \brief Smoothness quadratic term */
  Eigen::MatrixXd A_;

  /** \brief Smoothness linear term */
  Eigen::MatrixXd b_;

  /** \brief Smoothness constant term */
  Eigen::MatrixXd c_;

  /** \brief Has cost function been setup? */
  bool is_setup_;

  bool threshold_residual_;

  double threshold_;

  Eigen::MatrixXd GetThreshold (const Eigen::MatrixXd &xi) const;
};

}  // namespace planning_common
}  // namespace ca


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_WAYPOINT_SMOOTHNESS_OBJECTIVE_H_ 
