/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * state_validity_objective.h
 *
 *  Created on: Mar 21, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_STATE_VALIDITY_OBJECTIVE_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_STATE_VALIDITY_OBJECTIVE_H_

#include "ompl/base/OptimizationObjective.h"
#include "planning_common/paths/path_waypoint.h"
namespace ca {
namespace planning_common {

class StateValidityObjective : public ompl::base::OptimizationObjective {
 public:
  typedef boost::shared_ptr<StateValidityObjective> Ptr;

  StateValidityObjective(const ompl::base::SpaceInformationPtr &si)
 :ompl::base::OptimizationObjective(si){
    description_ = "Collision";
  }

  virtual ~StateValidityObjective() {}

  virtual ompl::base::Cost pathCost(const ompl::base::Path *path) const {
    if (const PathWaypoint *path_wp = dynamic_cast<const PathWaypoint*>(path)) {
      for (PathWaypoint::waypoints::const_iterator it = path_wp->CBegin(); it != path_wp->CEnd(); it++)
        if (!si_->isValid(*it))
          return ompl::base::Cost(ompl::base::Cost::Infinity);

      return ompl::base::Cost(0);
    } else {
      OMPL_ERROR("Havent implemented yet");
      return ompl::base::Cost(ompl::base::Cost::Infinity);
    }
  }

  virtual ompl::base::Cost motionCost(const ompl::base::State *s1, const ompl::base::State *s2) const {
    OMPL_ERROR("Havent implemented yet");
    return ompl::base::Cost(ompl::base::Cost::Infinity);
  }
};

}  // namespace planning_common
}  // namespace ca



#endif /* PLANNING_COMMON_INCLUDE_PLANNING_COMMON_OBJECTIVES_STATE_VALIDITY_OBJECTIVE_H_ */
