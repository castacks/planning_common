/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * complex_state_space_types.h
 *
 *  Created on: Jun 25, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATES_COMPLEX_STATE_SPACE_TYPES_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATES_COMPLEX_STATE_SPACE_TYPES_H_

#include "ompl/base/StateSpaceTypes.h"

namespace ca {
namespace planning_common {

enum StateSpaceType
{
  TANGENT_BUNDLE_SE2     =  ompl::base::STATE_SPACE_TYPE_COUNT + 1,

  TANGENT_BUNDLE_SE3,

  DUBINS_Z_STATE_SPACE,

  CLOTHOID_STATE_SPACE,

  CLOTHOID_Z_STATE_SPACE,

  XYZPSI_STATE_SPACE,

  COMPLEX_STATE_SPACE_TYPE_COUNT
};
}
}



#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATES_COMPLEX_STATE_SPACE_TYPES_H_ 
