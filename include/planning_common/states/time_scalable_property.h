/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * time_scalable_property.h
 *
 *  Created on: Jun 27, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATES_TIME_SCALABLE_PROPERTY_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATES_TIME_SCALABLE_PROPERTY_H_

#include "ompl/base/State.h"

namespace ca {
namespace planning_common {

class TimeScalable {
 public:
  TimeScalable() {};
  virtual ~TimeScalable() {};

  /** \brief Modify derivatives of state according to scaling factor
   * \dot{x} *= scale
   * \ddot{x} *= scale^2
   * @param state State to be scaled
   * @param scale Scaling factor
   */
  virtual void ScaleTime(ompl::base::State *state, double scale) const = 0;
};

}
}


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATES_TIME_SCALABLE_PROPERTY_H_ 
