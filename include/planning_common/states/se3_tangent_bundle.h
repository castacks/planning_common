/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * se3_tangent_bundle.h
 *
 *  Created on: Jun 24, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATES_SE3_TANGENT_BUNDLE_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATES_SE3_TANGENT_BUNDLE_H_


#include "ompl/base/StateSpace.h"
#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "ompl/base/spaces/SE3StateSpace.h"
#include "ompl/base/spaces/se3_tangent_space.h"
#include "ompl/base/spaces/TimeStateSpace.h"
#include "planning_common/states/complex_state_space_types.h"
#include "planning_common/states/time_scalable_property.h"
#include <Eigen/Dense>

namespace ca {
namespace planning_common {

class SE3TangentBundle : public ompl::base::CompoundStateSpace,  public TimeScalable {
 public:

  struct EigenForm {
    double time;
    Eigen::Vector3d position;
    Eigen::Vector3d velocity;
    Eigen::Vector3d orientation;
    Eigen::Vector3d rot_rates;
    EigenForm()
    : time(0),
      position(0,0,0),
      velocity(0,0,0),
      orientation(0,0,0),
      rot_rates(0,0,0){}
  };

  class StateType : public ompl::base::CompoundStateSpace::StateType {
   public:
    StateType(void) : ompl::base::CompoundStateSpace::StateType() {}

    const ompl::base::TimeStateSpace::StateType& GetTime() const {
      return *as<ompl::base::TimeStateSpace::StateType>(0);
    }

    const ompl::base::SE3StateSpace::StateType& GetPose() const {
      return *as<ompl::base::SE3StateSpace::StateType>(1);
    }

    const ompl_base::SE3TangentSpace::StateType& GetTangentPose() const {
      return *as<ompl_base::SE3TangentSpace::StateType>(2);
    }

    ompl::base::TimeStateSpace::StateType& GetTime() {
      return *as<ompl::base::TimeStateSpace::StateType>(0);
    }

    ompl::base::SE3StateSpace::StateType& GetPose() {
      return *as<ompl::base::SE3StateSpace::StateType>(1);
    }

    ompl_base::SE3TangentSpace::StateType& GetTangentPose() {
      return *as<ompl_base::SE3TangentSpace::StateType>(2);
    }

    EigenForm GetEigenForm() const;

    Eigen::Vector3d GetVelocity() const;

    void SetEigenForm(const EigenForm &eig);

    void ScaleTime (double scale) {
      // Invalidate time since the caller has to ensure that he sets the time offset correctly
      as<ompl::base::TimeStateSpace::StateType>(0)->position = 0;
      as<ompl_base::SE3TangentSpace::StateType>(2)->SetXDot( as<ompl_base::SE3TangentSpace::StateType>(2)->GetXDot() * scale );
      as<ompl_base::SE3TangentSpace::StateType>(2)->SetYDot( as<ompl_base::SE3TangentSpace::StateType>(2)->GetYDot() * scale );
      as<ompl_base::SE3TangentSpace::StateType>(2)->SetZDot( as<ompl_base::SE3TangentSpace::StateType>(2)->GetZDot() * scale );
      as<ompl_base::SE3TangentSpace::StateType>(2)->SetPhiDot( as<ompl_base::SE3TangentSpace::StateType>(2)->GetPhiDot() * scale );
      as<ompl_base::SE3TangentSpace::StateType>(2)->SetThetaDot( as<ompl_base::SE3TangentSpace::StateType>(2)->GetThetaDot() * scale );
      as<ompl_base::SE3TangentSpace::StateType>(2)->SetPsiDot( as<ompl_base::SE3TangentSpace::StateType>(2)->GetPsiDot() * scale );
    }
  };


  SE3TangentBundle(void) : CompoundStateSpace() {
    setName("SE3TangentBundle" + getName());
    type_ = TANGENT_BUNDLE_SE3;

    ompl::base::StateSpacePtr space;

    space = ompl::base::StateSpacePtr(new ompl::base::TimeStateSpace());
    space->as<ompl::base::TimeStateSpace>()->AssignWorkspaceTag(ompl_base::WorkspaceTags::TIME);
    addSubspace(space, 1.0);

    space = ompl::base::StateSpacePtr(new ompl::base::SE3StateSpace());
    space->as<ompl::base::SE3StateSpace>()->AssignWorkspaceTagSubspace(ompl_base::WorkspaceTags::TRANSLATION, ompl_base::WorkspaceTags::ROTATION);
    addSubspace(space, 1.0);

    space = ompl::base::StateSpacePtr(new ompl_base::SE3TangentSpace());
    space->as<ompl_base::SE3TangentSpace>()->AssignWorkspaceTagSubspace(ompl_base::WorkspaceTags::TANGENT_TRANSLATION, ompl_base::WorkspaceTags::TANGENT_ROTATION);
    addSubspace(space, 1.0);

    lock();
  }

  void SetBounds(const ompl::base::RealVectorBounds &position_bounds, const ompl::base::RealVectorBounds &vel_bounds, const ompl::base::RealVectorBounds &rot_rate_bounds) {
      as<ompl::base::SE3StateSpace>(1)->setBounds(position_bounds);
      as<ompl_base::SE3TangentSpace>(2)->SetBounds(vel_bounds, rot_rate_bounds);
  }

  virtual ~SE3TangentBundle(void) {}

  void ScaleTime (ompl::base::State *state, double scale) const{
    StateType *s = static_cast<StateType*>(state);
    s->ScaleTime(scale);
  }

  virtual double distance(const ompl::base::State *state1, const ompl::base::State *state2) const {
    const StateType *s1 = static_cast<const StateType*>(state1);
    const StateType *s2 = static_cast<const StateType*>(state2);
    return sqrt((s1->GetPose().getX() - s2->GetPose().getX())*(s1->GetPose().getX() - s2->GetPose().getX()) +
        (s1->GetPose().getY() - s2->GetPose().getY())*(s1->GetPose().getY() - s2->GetPose().getY()) +
        (s1->GetPose().getZ() - s2->GetPose().getZ())*(s1->GetPose().getZ() - s2->GetPose().getZ()));
  }


};

}  // namespace planning_common
}  // namespace ca


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATES_SE3_TANGENT_BUNDLE_H_ 
