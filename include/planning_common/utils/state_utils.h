/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * state_utils.h
 *
 *  Created on: Jun 25, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_UTILS_STATE_UTILS_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_UTILS_STATE_UTILS_H_


#include "ompl/base/spaces/SO3StateSpace.h"
#include "ompl/base/ScopedState.h"
#include "planning_common/states/se3_tangent_bundle.h"
#include "nav_msgs/Odometry.h"
#include "tf/tf.h"
#include <Eigen/Dense>


namespace ca {
namespace planning_common {
namespace state_utils {

ompl::base::ScopedState<ompl::base::SO3StateSpace> ConvertEulerToSO3 (double roll, double pitch, double yaw);

void ConvertEulerToSO3 (double roll, double pitch, double yaw, ompl::base::SO3StateSpace::StateType *state);

Eigen::Vector3d ConvertSO3toEuler (const  ompl::base::SO3StateSpace::StateType *state);

/**
 * \brief Shamelessy converts any state to se3 tangent bundle. Puts 0 if some workspace is missing.
 */
void ConvertAnyToSE3TangentBundle (const ompl::base::State* input_state, const ompl::base::SpaceInformationPtr &si, SE3TangentBundle::StateType *se3tb_state);

/**
 * \brief Converts odometry to SE3 Tangent bundle
 */
void ConvertOdomToSE3TangentBundle(const nav_msgs::Odometry &odom, SE3TangentBundle::StateType *se3tb_state);

void ConvertSE3TangentBundleToOdom(const SE3TangentBundle::StateType *se3tb_state, nav_msgs::Odometry &odom);

/**
 * \brief Are two space information same?
 */
bool AreSpaceInformationSame(const ompl::base::SpaceInformationPtr &si1, const ompl::base::SpaceInformationPtr &si2);



/**
 * \brief Get standard se3 tangent bundle si.
 * Practically speaking most of the aerial vehicle state operates in this space with no defined bounds. bounds are
 * applicable for the planner. But for the state space that exists in the outer layers, the bounds are irrelevant.
 * Think of this as the ompl equivalent of nav_msgs::Odometry.
 */
ompl::base::SpaceInformationPtr GetStandardSE3TangentBundle();

tf::Transform GetTFfromSE3(const ompl::base::State *state);

}
}
}


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_UTILS_STATE_UTILS_H_ 
