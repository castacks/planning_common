/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * workspace_utils.h
 *
 *  Created on: Jun 25, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_UTILS_WORKSPACE_UTILS_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_UTILS_WORKSPACE_UTILS_H_

#include "ompl/base/workspace_information.h"
#include "ompl/base/SpaceInformation.h"
#include "ompl/base/ScopedState.h"
#include "ompl/base/spaces/RealVectorStateSpace.h"
#include "ompl/base/spaces/SO2StateSpace.h"
#include "ompl/base/spaces/SO3StateSpace.h"
#include "ompl/base/spaces/TimeStateSpace.h"
#include <Eigen/Dense>

namespace ca {
namespace planning_common {
namespace workspace_utils {

/*
 * Important:
 * Derivatives are expected to be w.r.t. body frame because all code makes that assumption.
 * */

bool HasTranslation(const ompl::base::SpaceInformationPtr& si);
bool HasTangentTranslation(const ompl::base::SpaceInformationPtr& si);
bool HasRotation(const ompl::base::SpaceInformationPtr& si);
bool HasTangentRotation(const ompl::base::SpaceInformationPtr& si);
bool HasTime(const ompl::base::SpaceInformationPtr& si);
bool HasSO3Rotation(const ompl::base::SpaceInformationPtr& si);


const ompl::base::RealVectorStateSpace::StateType* GetTranslationState(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state, const ompl::base::StateSpace* &space);
ompl::base::RealVectorStateSpace::StateType* GetTranslationState(const ompl::base::SpaceInformationPtr& si, ompl::base::State *state);
Eigen::VectorXd GetTranslationVectorXd(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state);
Eigen::Vector2d GetTranslationVector2d(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state);
Eigen::Vector3d GetTranslationVector3d(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state);
unsigned int GetTranslationDimension(const ompl::base::SpaceInformationPtr& si);


const ompl::base::RealVectorStateSpace::StateType* GetTangentTranslationState(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state, const ompl::base::StateSpace* &space);
ompl::base::RealVectorStateSpace::StateType* GetTangentTranslationState(const ompl::base::SpaceInformationPtr& si, ompl::base::State *state);
Eigen::VectorXd GetTangentTranslationVectorXd(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state);
Eigen::Vector2d GetTangentTranslationVector2d(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state);
Eigen::Vector3d GetTangentTranslationVector3d(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state);

Eigen::Vector3d GetTangentTranslationVector3dWorld(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state);

ompl::base::RealVectorStateSpace::StateType* GetTangentTranslationState(const ompl::base::SpaceInformationPtr& si, ompl::base::State *state, const ompl::base::StateSpace* &space);
ompl::base::RealVectorStateSpace::StateType* GetTangentTranslationState(const ompl::base::SpaceInformationPtr& si, ompl::base::State *state);



const ompl::base::SO2StateSpace::StateType* GetRotationStateSO2(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state);
double GetRotationYaw(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state);
const ompl::base::SO3StateSpace::StateType* GetRotationStateSO3(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state);
ompl::base::SO3StateSpace::StateType* GetRotationStateSO3(const ompl::base::SpaceInformationPtr& si, ompl::base::State *state);
Eigen::Vector3d GetRotationVector3d(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state);
void SetRotationVector3d(const ompl::base::SpaceInformationPtr& si, const Eigen::Vector3d &rot_vec, ompl::base::State *state);

const ompl::base::RealVectorStateSpace::StateType* GetTangentRotationState(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state, const ompl::base::StateSpace* &space);
ompl::base::RealVectorStateSpace::StateType* GetTangentRotationState(const ompl::base::SpaceInformationPtr& si, ompl::base::State *state, const ompl::base::StateSpace* &space);
ompl::base::RealVectorStateSpace::StateType* GetTangentRotationState(const ompl::base::SpaceInformationPtr& si, ompl::base::State *state);


double GetTangentRotationYaw(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state);
Eigen::Vector3d GetTangentRotationVector3d(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state);

double GetTime(const ompl::base::SpaceInformationPtr& si, const ompl::base::State *state);

ompl::base::TimeStateSpace::StateType* GetTimeState(const ompl::base::SpaceInformationPtr& si, ompl::base::State *state);

}
}
}


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_UTILS_WORKSPACE_UTILS_H_ 
