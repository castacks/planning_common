/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * visualization_utils.h
 *
 *  Created on: Jun 26, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_UTILS_VISUALIZATION_UTILS_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_UTILS_VISUALIZATION_UTILS_H_

#include "planning_common/utils/workspace_utils.h"
#include "ompl/base/SpaceInformation.h"
#include "ompl/geometric/PathGeometric.h"
#include "ompl/base/PlannerData.h"
#include "planning_common/paths/path_waypoint.h"
#include "ca_nav_msgs/WorkspaceTrajectory.h"

namespace ca {
namespace planning_common {
namespace visualization_utils {

visualization_msgs::Marker GetMarker(boost::function< const ompl::base::State* (unsigned int)> GetStateFn,
                                     boost::function< std::size_t (void)> GetNumState,
                                     const ompl::base::SpaceInformationPtr& si,
                                     double scale=1, double r=0, double g=1, double b=0, double a=1);

visualization_msgs::Marker GetMarker(const ompl::geometric::PathGeometric& path, double scale=1, double r=0, double g=1, double b=0, double a=1);

visualization_msgs::Marker GetMarker(const PathWaypoint& path, double scale=1, double r=0, double g=1, double b=0, double a=1);

visualization_msgs::Marker GetMarker(const std::vector<ca::planning_common::PathWaypoint >& path_set, double scale=1, double r=0, double g=1, double b=0, double a=1);

visualization_msgs::Marker GetMarkerLineList(const PathWaypoint& path, double scale=1, double r=0, double g=1, double b=0, double a=1);

visualization_msgs::Marker GetMarker(const ca_nav_msgs::WorkspaceTrajectory &traj, double scale=1, double r=0, double g=1, double b=0, double a=1);

visualization_msgs::MarkerArray GetMarkerArrayGradient(const Eigen::MatrixXd &gradient, const PathWaypoint& path, double gradientscale=1, double scale=1, double r=0, double g=1, double b=0, double a=1);

visualization_msgs::Marker GetGraph(const ompl::base::PlannerData &data, unsigned int resolution = 100, double scale=1, double r=0, double g=0,  double b=1, double a=1);

visualization_msgs::Marker GetMarker(const nav_msgs::Odometry &odom, double scale=1, double r=0, double g=1, double b=0, double a=1);

visualization_msgs::Marker VisualizeSpeedGrid(const PathWaypoint &path, double grid_res_ms=10, double ms2m=1, double num_levels=5, double scale=1.5);

}
}
}


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_UTILS_VISUALIZATION_UTILS_H_ 
