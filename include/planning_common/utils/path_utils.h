/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * path_utils.h
 *
 *  Created on: Jun 23, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_UTILS_PATH_UTILS_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_UTILS_PATH_UTILS_H_

#include "ompl/base/State.h"
#include "planning_common/paths/path_waypoint.h"
#include "ompl/geometric/PathGeometric.h"
#include "ca_nav_msgs/WorkspaceTrajectory.h"

namespace ca {
namespace planning_common {
namespace path_utils {

//bool StateDistanceFromEnd(const PathWaypoint &path, double distance, ompl::base::State *sample);

double GetWorkspaceTranslationLength (const PathWaypoint &path);

PathWaypoint ConvertPathToSE3TangentBundlePath(const ompl::geometric::PathGeometric& original_path);

PathWaypoint ConvertPathToSE3TangentBundlePath(const PathWaypoint& original_path);


PathWaypoint ConvertPathToSE3TangentBundlePath(boost::function< const ompl::base::State* (unsigned int)> GetStateFn,
                                     boost::function< std::size_t (void)> GetNumState,
                                     const ompl::base::SpaceInformationPtr& si_original);

PathWaypoint ConvertWorkspaceTrajectoryMsgToPathWaypoint(const ca_nav_msgs::WorkspaceTrajectory &msg);

ca_nav_msgs::WorkspaceTrajectory ConvertPathWaypointToWorkspaceTrajectoryMsg(const PathWaypoint &path);

/**
 * \\brief Index operations
 */

bool IsIndexValid(const PathWaypoint &path, double index);

void StateAtIndex(const PathWaypoint &path, double index, ompl::base::State* state);

double ProjectPosition(const PathWaypoint &path, const Eigen::Vector3d &position, double start_index);


/**
 * \brief Cut chaat snip trim :P
 */
void SegmentFromIndex(const PathWaypoint &original, double index, PathWaypoint &modified);

void SegmentToIndex(const PathWaypoint &original, double index, PathWaypoint &modified);

void SegmentFromToIndex(const PathWaypoint &original, double from_index, double to_index, PathWaypoint &modified);


void StateAtTime(const PathWaypoint &path, double time, ompl::base::State* state);

PathWaypoint SegmentToTime(const PathWaypoint &original, double time);

PathWaypoint SegmentFromTime(const PathWaypoint &original, double time);

bool StitchPathWaypointAtTime(const PathWaypoint &original, const PathWaypoint &branch, PathWaypoint &merged, double threshold = 1e-3);

void RecomputeTime(double start_time, PathWaypoint &path);

/**
 * \brief All  functions below are specific to SE3TangentBundle
 */

bool StateDistanceFromEndFromRef2D(const PathWaypoint &path, double distance, const Eigen::Vector3d &ref, ompl::base::State *sample);

void SplitPathAtHorizon(const PathWaypoint &original, PathWaypoint &first, PathWaypoint &second, double horizon);

bool WarpSpeedProfile(const PathWaypoint &original_path, PathWaypoint &new_path);

bool SpeedProfileCopyBackwards(const PathWaypoint &original_path, PathWaypoint &new_path);

double ForwardPropagateIndexByDistance(const PathWaypoint &path, double index, double propagate_distance);
/**
 * \brief Very dangerous function that fixes the time, position and heading for SE3TangentBundle
 */
void ShiftPathToStart(const ompl::base::State* start_state, PathWaypoint &path);

void MakeHeadingTangent(PathWaypoint &path);
void PrintGlideSlope(const PathWaypoint &path);

void PrintXY(const PathWaypoint &path);

}  // namespace path_utils
}  // namespace planning_common
}  // namespace ca



#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_UTILS_PATH_UTILS_H_ 
