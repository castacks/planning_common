/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * iteration_termination.h
 *
 *  Created on: Jun 15, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_PLANNER_PARAMETERS_ITERATION_TERMINATION_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_PLANNER_PARAMETERS_ITERATION_TERMINATION_H_

#include "ompl/base/PlannerTerminationCondition.h"

namespace ca {
namespace planning_common {
/** \brief A boost function representing the number of iterations executed call from a planner */
typedef boost::function<unsigned int(void)> GetIterationsFn;

/** \brief A boost function representing the largest gradiemt */
typedef boost::function<double(void)> GetGradientNormFn;

/** \brief A boost function representing the largest gradiemt */
typedef boost::function<double(void)> GetRelCostDecreaseFn;

/** \brief Termination condition if max number of iterations exceeded */
ompl::base::PlannerTerminationCondition NumIterationsPlannerTerminationCondition(const GetIterationsFn &get_iterations_fn, unsigned int max_num_iterations);

/** \brief Termination condition if gradient norm below a value */
ompl::base::PlannerTerminationCondition GradientConvergencePlannerTerminationCondition(const GetGradientNormFn &get_gradient_norm_fn, double min_gradient_norm);

ompl::base::PlannerTerminationCondition RelCostDecreasePlannerTerminationCondition(const GetRelCostDecreaseFn &get_rel_cost_dec_fn, double thr_cost_dec);


}  // namespace planning_common
}  // namespace ca


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_PLANNER_PARAMETERS_ITERATION_TERMINATION_H_ 
