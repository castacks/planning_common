/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * state_validity_dt.h
 *
 *  Created on: Aug 23, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_COLLISION_CHECK_STATE_VALIDITY_DT_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_COLLISION_CHECK_STATE_VALIDITY_DT_H_

#include <ompl/base/SpaceInformation.h>
#include "planning_common/data_structures/cost_map.h"
#include "planning_common/utils/workspace_utils.h"


namespace ca {
namespace planning_common {

class StateValidityCheckerDT : public ompl::base::StateValidityChecker {
 public:
  StateValidityCheckerDT(const ompl::base::SpaceInformationPtr& si, CostMap::Ptr dist_to_obst, double robot_size)
 : ompl::base::StateValidityChecker(si),
   si_ptr_(si),
   dist_to_obst_(dist_to_obst),
   robot_size_(robot_size){}

  // Returns whether the given state's position overlaps the
  // circular obstacle
  bool isValid(const ompl::base::State* state) const {
    Eigen::VectorXd pos = workspace_utils::GetTranslationVectorXd(si_ptr_, state);
    Eigen::Vector3d query = pos.rows() == 3 ? Eigen::Vector3d(pos[0], pos[1], pos[2]) : Eigen::Vector3d(pos[0], pos[1], 0);
    /*ROS_ERROR_STREAM("query "<<query.transpose());
    ROS_ERROR_STREAM("sat bounds "<<si_->satisfiesBounds(state));
    ROS_ERROR_STREAM("valid "<<dist_to_obst_->IsValid(query));
    ROS_ERROR_STREAM("dist "<<dist_to_obst_->GetValue(query));
*/

    return si_->satisfiesBounds(state) && !(dist_to_obst_->IsValid(query) && dist_to_obst_->GetValue(query) < robot_size_);
  }

 private:
  ompl::base::SpaceInformationPtr si_ptr_; // No choice but to do this now (because base class has raw pointer which messes up workspace utils call)
  CostMap::Ptr dist_to_obst_;
  double robot_size_;
};


}
}


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_COLLISION_CHECK_STATE_VALIDITY_DT_H_ 
