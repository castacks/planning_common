/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2014 Sanjiban Choudhury
 * bounds_state_validity_checker.h
 *
 *  Created on: Aug 24, 2014
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATE_VALIDITY_BOUNDS_STATE_VALIDITY_CHECKER_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATE_VALIDITY_BOUNDS_STATE_VALIDITY_CHECKER_H_

#include "ompl/base/StateValidityChecker.h"

namespace ca {
namespace planning_common {

/** \brief Bounds Validity Checker */
class BoundsStateValidityChecker : public ompl::base::StateValidityChecker {
 public:

  /** \brief Constructor */
  BoundsStateValidityChecker(ompl::base::SpaceInformation* si)
 : ompl::base::StateValidityChecker(si){}

  /** \brief Constructor */
  BoundsStateValidityChecker(const ompl::base::SpaceInformationPtr &si)
  : ompl::base::StateValidityChecker(si){}

  /** \brief If satisfies bounds, return true */
  virtual bool isValid(const ompl::base::State* state) const;

};

}  // namespace planning_common
}  // namespace ca


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATE_VALIDITY_BOUNDS_STATE_VALIDITY_CHECKER_H_ 
