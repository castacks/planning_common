/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * shapes_state_validity.h
 *
 *  Created on: Feb 27, 2015
 *      Author: Sanjiban Choudhury
 */

#ifndef PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATE_VALIDITY_SHAPES_STATE_VALIDITY_H_
#define PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATE_VALIDITY_SHAPES_STATE_VALIDITY_H_

#include <ompl/base/SpaceInformation.h>
#include "planning_common/utils/workspace_utils.h"
#include "shapes/shapes.h"

namespace ca {
namespace planning_common {

class ShapesStateValidityChecker : public ompl::base::StateValidityChecker {
 public:
  ShapesStateValidityChecker(const ompl::base::SpaceInformationPtr& si, const ShapePtr &valid_region, const ShapeSet &invalid_region)
 : ompl::base::StateValidityChecker(si),
   si_ptr_(si),
   valid_region_(valid_region),
   invalid_region_(invalid_region){}

  bool isValid(const ompl::base::State* state) const {
    Eigen::VectorXd pos = workspace_utils::GetTranslationVectorXd(si_ptr_, state);
    return si_->satisfiesBounds(state) && valid_region_->InShape(pos) && !invalid_region_.InShapeSet(pos);
  }

 private:
  ompl::base::SpaceInformationPtr si_ptr_; // No choice but to do this now (because base class has raw pointer which messes up workspace utils call)
  ShapePtr valid_region_;
  ShapeSet invalid_region_;
};


}
}


#endif  // PLANNING_COMMON_INCLUDE_PLANNING_COMMON_STATE_VALIDITY_SHAPES_STATE_VALIDITY_H_ 
